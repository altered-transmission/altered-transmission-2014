"GameMenu"
{
	"1"
	{
		"label" "#AT_GameMenu_ResumeGame"
		"command" "ResumeGame"
		"InGameOrder" "10"
		"OnlyInGame" "1"
	}
	"3"	
	{
		"label" "#AT_GameMenu_NewGame"
		"command" "engine map AT_01"
		"InGameOrder" "40"
		"notmulti" "1"
	}
	"4"	
	{
		"label" "#AT_GameMenu_SceneSelection"
		"command" "OpenNewGameDialog"
		"InGameOrder" "40"
		"notmulti" "1"
	}
	"5"
	{
		"label" "#AT_GameMenu_LoadGame"
		"command" "OpenLoadGameDialog"
		"InGameOrder" "30"
		"notmulti" "1"
	}
	"6"
	{
		"label" "#AT_GameMenu_SaveGame"
		"command" "OpenSaveGameDialog"
		"InGameOrder" "20"
		"notmulti" "1"
		"OnlyInGame" "1"
	}
	"7"
	{
		"label" "#AT_GameMenu_Options"
		"command" "OpenOptionsDialog"
		"InGameOrder" "80"
	}
	//"8"
	//{
	//	"label" "#AT_GameMenu_Bonus"
	//	"command" "OpenOptionsDialog"
	//	"InGameOrder" "80"
	//}
	"9"
	{
		"label" "#AT_GameMenu_Quit"
		"command" "Quit"
		"InGameOrder" "90"
	}
}

