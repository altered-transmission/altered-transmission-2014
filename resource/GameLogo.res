Resource/GameLogo.res
{
	GameLogo
	{
		ControlName	EditablePanel
		fieldName	GameLogo
		xpos		0
		ypos		0
		zpos		50
		wide		1024
		tall		128
		autoResize	1
		pinCorner	0
		visible		1
		enabled		1
		offsetX		-20
		offsetY		-15
	}
 
	Logo
	{
		ControlName	ImagePanel
		fieldName	Logo
		xpos		0
		ypos		0
		zpos		50
		wide		1024
		tall		128
		visible		1
		enabled		1
		image		"AT_Logo"
		scaleImage	1		
	}
}