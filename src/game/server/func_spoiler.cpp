//========= Copyright � 2007-2008, Maxsi.dk, All rights reserved. ============//
//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: When needed, display a hint if the player is stuck.
//			Should undermine the stupid need of walkthroughs... :D
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "engine/IEngineSound.h"
#include "baseentity.h"
#include "entityoutput.h"
#include "recipientfilter.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CFunc_Spoiler : public CPointEntity
{
public:
	DECLARE_CLASS( CFunc_Spoiler, CPointEntity );

	void	Spawn( void );
	void	Precache( void );

private:
	void InputShowHudHint( inputdata_t &inputdata );
	void InputHideHudHint( inputdata_t &inputdata );
	void InputSetHint( inputdata_t &inputdata );
	
	char	CurrentHint[255];

	string_t m_iszMessage;
	DECLARE_DATADESC();
};

LINK_ENTITY_TO_CLASS( func_spoiler, CFunc_Spoiler );

BEGIN_DATADESC( CFunc_Spoiler )

	DEFINE_KEYFIELD( m_iszMessage, FIELD_STRING, "message" ),
	DEFINE_INPUTFUNC( FIELD_VOID, "ShowHudHint", InputShowHudHint ),
	DEFINE_INPUTFUNC( FIELD_VOID, "HideHudHint", InputHideHudHint ),
	DEFINE_INPUTFUNC( FIELD_VOID, "SetHint", InputSetHint ),

END_DATADESC()



//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CFunc_Spoiler::Spawn( void )
{
	Precache();

	strcpy_s(CurrentHint,sizeof(CurrentHint),STRING(m_iszMessage));

	SetSolid( SOLID_NONE );
	SetMoveType( MOVETYPE_NONE );
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CFunc_Spoiler::Precache( void )
{
}

//-----------------------------------------------------------------------------
// Purpose: Change the desired hint
//-----------------------------------------------------------------------------
void CFunc_Spoiler::InputSetHint( inputdata_t &inputdata )
{
	Msg("Func_Spoiler: Changing Hint from \"%s\" to ",m_iszMessage);
	//m_iszMessage = MAKE_STRING(inputdata.value.String());
	
	inputdata.value.Convert(FIELD_STRING);

	Msg("\"%s\" = ",inputdata.value.String());

	inputdata.value.Convert(FIELD_INTEGER);

	Msg("\"%s\" = ",inputdata.value.Int());


	strcpy_s(CurrentHint,sizeof(CurrentHint),inputdata.value.String());

	Msg("\"%s\" = ",CurrentHint);

	m_iszMessage = MAKE_STRING(CurrentHint);

	Msg("\"%s\"\n",m_iszMessage);

	// Was that just it? Damn that was easy.. :D
	// Edit oh noes.. it wasn't D:
}

//-----------------------------------------------------------------------------
// Purpose: Input handler for showing the message and/or playing the sound.
//-----------------------------------------------------------------------------
void CFunc_Spoiler::InputShowHudHint( inputdata_t &inputdata )
{
	CBaseEntity *pPlayer = NULL;

	if ( inputdata.pActivator && inputdata.pActivator->IsPlayer() )
	{
		pPlayer = inputdata.pActivator;
	}
	else
	{
		pPlayer = UTIL_GetLocalPlayer();
	}

	if ( pPlayer )
	{
		if ( !pPlayer || !pPlayer->IsNetClient() )
			return;

		CSingleUserRecipientFilter user( (CBasePlayer *)pPlayer );
		user.MakeReliable();
		UserMessageBegin( user, "HintText" );
			WRITE_BYTE( 1 );	// one message
			WRITE_STRING( STRING(m_iszMessage) );
		MessageEnd();
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void CFunc_Spoiler::InputHideHudHint( inputdata_t &inputdata )
{
	CBaseEntity *pPlayer = NULL;

	if ( inputdata.pActivator && inputdata.pActivator->IsPlayer() )
	{
		pPlayer = inputdata.pActivator;
	}
	else
	{
		pPlayer = UTIL_GetLocalPlayer();
	}

	if ( pPlayer )
	{
		if ( !pPlayer || !pPlayer->IsNetClient() )
			return;

		CSingleUserRecipientFilter user( (CBasePlayer *)pPlayer );
		user.MakeReliable();
		UserMessageBegin( user, "HintText" );
		WRITE_BYTE( 1 );	// one message
		WRITE_STRING( STRING(NULL_STRING) );
		MessageEnd();
	}
}
