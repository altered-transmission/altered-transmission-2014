//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//========= Copyright � 2007-2008, Maxsi.dk, All rights reserved. ============//
// item_ar2ball_supply.cpp
// Purpose: A better gameplay wise way to supply players with infinite amounts of AR2 balls,
// and a way to make them more stabile. ;D
//
// $NoKeywords: $
//
//=============================================================================//


#include "cbase.h"
#include "gamerules.h"
#include "player.h"
#include "engine/IEngineSound.h"
#include "in_buttons.h"
#include "func_recharger.h"
#include "gamerules.h"
#include "ammodef.h"
#include "weapon_ar2.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

extern ConVar	sk_weapon_ar2_alt_fire_next_decrease;

ConVar	g_debug_ar2ball_supply( "g_debug_ar2ball_supply", "0" );

#define SF_CITADEL_RECHARGER	0x2000
#define SF_KLEINER_RECHARGER	0x4000 // Gives only 25 health




//NEW
class CAR2BallSupply : public CBaseAnimating
{
public:
	DECLARE_CLASS( CAR2BallSupply, CBaseAnimating );

	void Spawn( );
	bool CreateVPhysics();
	int DrawDebugTextOverlays(void);
	void Off(void);
	void Recharge(void);
	void SlowRecharge(void);

	bool KeyValue( const char *szKeyName, const char *szValue );
	void Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	virtual int	ObjectCaps( void ) { return (BaseClass::ObjectCaps() | m_iCaps ); }

	void SetInitialCharge( void );

private:
	void InputRecharge( inputdata_t &inputdata );
	void InputSetCharge( inputdata_t &inputdata );
	float MaxJuice() const;
	void UpdateJuice( int newJuice );
	void Precache( void );

	DECLARE_DATADESC();

	float	m_flNextCharge; 
	int		m_iReactivate ; // DeathMatch Delay until reactvated
	int		m_iJuice;
	int		m_iOn;			// 0 = off, 1 = startup, 2 = going
	float   m_flSoundTime;
	
	int		m_nState;
	int		m_iCaps;
	int		m_iMaxJuice;
	
	COutputFloat m_OutRemainingCharge;
	COutputEvent m_OnHalfEmpty;
	COutputEvent m_OnEmpty;
	COutputEvent m_OnFull;
	COutputEvent m_OnPlayerUse;

	virtual void StudioFrameAdvance ( void );
	float m_flJuice;
};

BEGIN_DATADESC( CAR2BallSupply )

	DEFINE_FIELD( m_flNextCharge, FIELD_TIME ),
	DEFINE_FIELD( m_iReactivate, FIELD_INTEGER),
	DEFINE_FIELD( m_iJuice, FIELD_INTEGER),
	DEFINE_FIELD( m_iOn, FIELD_INTEGER),
	DEFINE_FIELD( m_flSoundTime, FIELD_TIME ),
	DEFINE_FIELD( m_nState, FIELD_INTEGER ),
	DEFINE_FIELD( m_iCaps, FIELD_INTEGER ),
	DEFINE_FIELD( m_iMaxJuice, FIELD_INTEGER ),

	// Function Pointers
	DEFINE_FUNCTION( Off ),
	DEFINE_FUNCTION( Recharge ),
	DEFINE_FUNCTION( SlowRecharge ),

	DEFINE_OUTPUT(m_OutRemainingCharge, "OutRemainingCharge"),
	DEFINE_OUTPUT(m_OnHalfEmpty, "OnHalfEmpty" ),
	DEFINE_OUTPUT(m_OnEmpty, "OnEmpty" ),
	DEFINE_OUTPUT(m_OnFull, "OnFull" ),
	DEFINE_OUTPUT(m_OnPlayerUse, "OnPlayerUse" ),
	DEFINE_FIELD( m_flJuice, FIELD_FLOAT ),

	DEFINE_INPUTFUNC( FIELD_VOID, "Recharge", InputRecharge ),
	DEFINE_INPUTFUNC( FIELD_INTEGER, "SetCharge", InputSetCharge ),
	
END_DATADESC()


LINK_ENTITY_TO_CLASS( item_ar2ballsupply, CAR2BallSupply);

#define HEALTH_CHARGER_MODEL_NAME "models/props_combine/ar20_charger001.mdl"
#define CHARGE_RATE 0.05f
#define CHARGES_PER_SECOND 1 / CHARGE_RATE
#define CITADEL_CHARGES_PER_SECOND 10 / CHARGE_RATE
#define CALLS_PER_SECOND 7.0f * CHARGES_PER_SECOND


bool CAR2BallSupply::KeyValue( const char *szKeyName, const char *szValue )
{
	if (	FStrEq(szKeyName, "style") ||
				FStrEq(szKeyName, "height") ||
				FStrEq(szKeyName, "value1") ||
				FStrEq(szKeyName, "value2") ||
				FStrEq(szKeyName, "value3"))
	{
	}
	else if (FStrEq(szKeyName, "dmdelay"))
	{
		m_iReactivate = atoi(szValue);
	}
	else
	{
		return BaseClass::KeyValue( szKeyName, szValue );
	}

	return true;
}

void CAR2BallSupply::Precache( void )
{
	PrecacheModel( HEALTH_CHARGER_MODEL_NAME );

	PrecacheScriptSound( "SuitRecharge.Deny" );
	PrecacheScriptSound( "SuitRecharge.Start" );
	PrecacheScriptSound( "SuitRecharge.ChargingLoop" );

}

void CAR2BallSupply::SetInitialCharge( void )
{
	m_iMaxJuice = 100;
	/*if ( HasSpawnFlags( SF_KLEINER_RECHARGER ) )
	{
		// The charger in Kleiner's lab.
		m_iMaxJuice =  25.0f;
		return;
	}

	if ( HasSpawnFlags( SF_CITADEL_RECHARGER ) )
	{
		m_iMaxJuice =  sk_suitcharger_citadel.GetFloat();
		return;
	}

	m_iMaxJuice =  sk_suitcharger.GetFloat();*/
}

void CAR2BallSupply::Spawn()
{
	Precache( );

	SetMoveType( MOVETYPE_NONE );
	SetSolid( SOLID_VPHYSICS );
	CreateVPhysics();

	SetModel( HEALTH_CHARGER_MODEL_NAME );
	AddEffects( EF_NOSHADOW );

	ResetSequence( LookupSequence( "idle" ) );

	SetInitialCharge();

	UpdateJuice( MaxJuice() );

	m_nState = 0;		
	m_iCaps	= FCAP_CONTINUOUS_USE;

	CreateVPhysics();

	m_flJuice = m_iJuice;

	m_iReactivate = 0;

	SetCycle( 1.0f - ( m_flJuice / MaxJuice() ) );
}

bool CAR2BallSupply::CreateVPhysics()
{
	VPhysicsInitStatic();
	return true;
}

int CAR2BallSupply::DrawDebugTextOverlays(void) 
{
	int text_offset = BaseClass::DrawDebugTextOverlays();

	if (m_debugOverlays & OVERLAY_TEXT_BIT) 
	{
		char tempstr[512];
		Q_snprintf(tempstr,sizeof(tempstr),"Charge left: %i", m_iJuice );
		EntityText(text_offset,tempstr,0);
		text_offset++;
	}
	return text_offset;
}

void CAR2BallSupply::StudioFrameAdvance( void )
{
	m_flPlaybackRate = 0;

	float flMaxJuice = MaxJuice() + 0.1f;
	float flNewJuice = 1.0f - (float)( m_flJuice / flMaxJuice );

	SetCycle( flNewJuice );
//	Msg( "Cycle: %f - Juice: %d - m_flJuice :%f - Interval: %f\n", (float)GetCycle(), (int)m_iJuice, (float)m_flJuice, GetAnimTimeInterval() );

	if ( !m_flPrevAnimTime )
	{
		m_flPrevAnimTime = gpGlobals->curtime;
	}

	// Latch prev
	m_flPrevAnimTime = m_flAnimTime;
	// Set current
	m_flAnimTime = gpGlobals->curtime;
}



//-----------------------------------------------------------------------------
// Max juice for recharger
//-----------------------------------------------------------------------------
float CAR2BallSupply::MaxJuice()	const
{
	return m_iMaxJuice;
}


//-----------------------------------------------------------------------------
// Purpose: 
// Input  : newJuice - 
//-----------------------------------------------------------------------------
void CAR2BallSupply::UpdateJuice( int newJuice )
{
	bool reduced = newJuice < m_iJuice;
	if ( reduced )
	{
		// Fire 1/2 way output and/or empyt output
		int oneHalfJuice = (int)(MaxJuice() * 0.5f);
		if ( newJuice <= oneHalfJuice && m_iJuice > oneHalfJuice )
		{
			m_OnHalfEmpty.FireOutput( this, this );
		}

		if ( newJuice <= 0 )
		{
			m_OnEmpty.FireOutput( this, this );
		}
	}
	else if ( newJuice != m_iJuice &&
		newJuice == (int)MaxJuice() )
	{
		m_OnFull.FireOutput( this, this );
	}
	m_iJuice = newJuice;
}

void CAR2BallSupply::InputRecharge( inputdata_t &inputdata )
{
	Recharge();
}

void CAR2BallSupply::InputSetCharge( inputdata_t &inputdata )
{
	ResetSequence( LookupSequence( "idle" ) );

	int iJuice = inputdata.value.Int();

	m_flJuice = m_iMaxJuice = m_iJuice = iJuice;
	StudioFrameAdvance();
}

void CAR2BallSupply::Use( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{ 
	bool GavePlayerExtraAmmo = false;
	int AR2Index = GetAmmoDef()->Index( "AR2" );

	CBaseCombatWeapon *player_ar2;
	// if it's not a player, ignore
	if ( !pActivator || !pActivator->IsPlayer() )
		return;

	CBasePlayer *pPlayer = static_cast<CBasePlayer *>(pActivator);

	// Reset to a state of continuous use.
	m_iCaps = FCAP_CONTINUOUS_USE;

	if ( m_iOn )
	{
		float flCharges = CHARGES_PER_SECOND;
		float flCalls = CALLS_PER_SECOND;

		if ( HasSpawnFlags( SF_CITADEL_RECHARGER ) )
			 flCharges = CITADEL_CHARGES_PER_SECOND;

		m_flJuice -= flCharges / flCalls * 5;		
		StudioFrameAdvance();
	}
	if ( pPlayer )
	{
		player_ar2 = pPlayer->Weapon_OwnsThisType("weapon_ar2");
		if ( player_ar2 )
		{
			// If the player doesn't have ammo on the Ar2, he cannot switch to it, so give him the ammo! :D
			if ( pPlayer->GetAmmoCount(AR2Index) < 1 && !player_ar2->HasAmmo())
			{
				GavePlayerExtraAmmo = true;
				pPlayer->GiveAmmo( 1, AR2Index );
			}
			if ( pPlayer->Weapon_CanSwitchTo(player_ar2) )
			{
				pPlayer->Weapon_Switch(player_ar2);
			}
			else
			{
				if (m_flSoundTime <= gpGlobals->curtime)
				{
					if ( g_debug_ar2ball_supply.GetInt() == 1 )
					{
						Msg("Couldn't access AR2!\n");
					}
					m_flSoundTime = gpGlobals->curtime + 0.62;
					EmitSound( "SuitRecharge.Deny" );
				}
				return;
			}
		}

	}
	else
	{
		return;
	}
	// Only usable if you have the HEV suit on
	if ( !pPlayer->IsSuitEquipped() )
	{
		if (m_flSoundTime <= gpGlobals->curtime)
		{
			m_flSoundTime = gpGlobals->curtime + 0.62;
			if ( g_debug_ar2ball_supply.GetInt() == 1 )
			{
				Msg("Player got no HEV suit\n");
			}
			EmitSound( "SuitRecharge.Deny" );
		}
		return;
	}

	// if there is no juice left, turn it off
	if ( m_iJuice <= 0 )
	{
		// Start our deny animation over again
		// ResetSequence( LookupSequence( "emptyclick" ) );
		
		m_nState = 1;
		
		// Recharge in a moment

		SetNextThink( gpGlobals->curtime + CHARGE_RATE );
		SetThink( &CAR2BallSupply::SlowRecharge );

		// Play a deny sound
		if ( m_flSoundTime <= gpGlobals->curtime )
		{
			m_flSoundTime = gpGlobals->curtime + 0.62;
			if ( g_debug_ar2ball_supply.GetInt() == 1 )
			{
				Msg("No juice.\n");
			}
			EmitSound( "SuitRecharge.Deny" );
		}

		return;
	}

	// Get our maximum armor value
	int nMaxArmor = 100;

	int nIncrementArmor = 5;

	// The citadel charger gives more per charge and also gives health
	if ( HasSpawnFlags(	SF_CITADEL_RECHARGER ) )
	{
		nIncrementArmor = 1;
		
#ifdef HL2MP
		nIncrementArmor = 2;
#endif

		// Also give health for the citadel version.

	}

	/*// If we're over our limit, debounce our keys
	if ( pPlayer->ArmorValue() >= nMaxArmor)
	{
		// Citadel charger must also be at max health
		if ( !HasSpawnFlags(SF_CITADEL_RECHARGER) || ( HasSpawnFlags( SF_CITADEL_RECHARGER ) && pActivator->GetHealth() >= pActivator->GetMaxHealth() ) )
		{
			// Make the user re-use me to get started drawing health.
			pPlayer->m_afButtonPressed &= ~IN_USE;
			m_iCaps = FCAP_IMPULSE_USE;
			if ( g_debug_ar2ball_supply.GetInt() == 2 )
			{
				Msg("Full armor.\n");
			}
		}
	}*/

	// This is bumped out if used within the time period
	SetNextThink( gpGlobals->curtime + CHARGE_RATE );
	SetThink( &CAR2BallSupply::SlowRecharge );

	// Time to recharge yet?
	if ( m_flNextCharge >= gpGlobals->curtime )
		return;

	// Play the on sound or the looping charging sound
	if ( !m_iOn )
	{
		m_iOn++;
		EmitSound( "SuitRecharge.Start" );
		m_flSoundTime = 0.56 + gpGlobals->curtime;

		m_OnPlayerUse.FireOutput( pActivator, this );
	}

	if ((m_iOn == 1) && (m_flSoundTime <= gpGlobals->curtime))
	{
		m_iOn++;
		CPASAttenuationFilter filter( this, "SuitRecharge.ChargingLoop" );
		filter.MakeReliable();
		EmitSound( filter, entindex(), "SuitRecharge.ChargingLoop" );
	}

	// Look up the ammo index for the Ar2 ball and give some ammo. ;-)
	int AR2BallIndex = GetAmmoDef()->Index( "AR2AltFire" );
	int AR2BallAmmoAmount = 5;
	pPlayer->GiveAmmo( AR2BallAmmoAmount, AR2BallIndex );
	if ( GavePlayerExtraAmmo)
	{
		GavePlayerExtraAmmo = true;
		pPlayer->RemoveAmmo(1, AR2Index );
	}
	// Prevent a decrease in the near future, otherwise we wont be able to fill it fully up which sucks for the player
	// New System: Shared console command:
	sk_weapon_ar2_alt_fire_next_decrease.SetValue((float)gpGlobals->curtime + (float)2.0);

	if ( pPlayer )
	{
		player_ar2 = pPlayer->Weapon_OwnsThisType("weapon_ar2");
		if ( player_ar2 )
		{
			CWeaponAR2 *the_ar2 = static_cast<CWeaponAR2 *>(player_ar2);
			if ( the_ar2 )
			{
				the_ar2->RemoveAr2Ammo();
			}
			else
			{
				DevMsg("Couldn't convert to Ar2!\n");
			}
		}
		else
		{
			DevMsg("Couldn't find base Ar2!\n");
		}
	}
	else
	{
		DevMsg("AR2BALL_SUPPLY: No pPlayer?!\n");
	}

	/*if ( pPlayer )
	{
		player_ar2 = pPlayer->Weapon_OwnsThisType("weapon_ar2");
		if ( player_ar2 )
		{
			CWeaponAR2 *the_ar2 = static_cast<CWeaponAR2 *>(player_ar2);
			if ( the_ar2 )
			{
				DevMsg("Delayed Removal from: %d to",the_ar2->m_NextAr2AltDecrease);
				the_ar2->m_NextAr2AltDecrease = gpGlobals->curtime + 1.5;
				DevMsg("%d \n",the_ar2->m_NextAr2AltDecrease);
			}
			else
			{
				DevMsg("Couldn't convert to Ar2!\n");
			}
		}
		else
		{
			DevMsg("Couldn't find base Ar2!\n");
		}
	}
	else
	{
		DevMsg("AR2BALL_SUPPLY: No pPlayer?!\n");
	}*/
	
	UpdateJuice( m_iJuice - AR2BallAmmoAmount );

	// Altered Transmission Edit:
	// Why isn't the float version getting updated here? It might run nonsync with the integer otherwise?
	m_flJuice = m_iJuice;

	// Give armor if we need it
	if ( pPlayer->ArmorValue() < nMaxArmor )
	{
		pPlayer->IncrementArmorValue( nIncrementArmor, nMaxArmor );
	}
	if ( pActivator->GetHealth() < pActivator->GetMaxHealth())
	{
		pActivator->TakeHealth( 5, DMG_GENERIC );
	}
	// Send the output.
	float flRemaining = m_iJuice / MaxJuice();
	
	m_OutRemainingCharge.Set(flRemaining, pActivator, this);

	// govern the rate of charge
	m_flNextCharge = gpGlobals->curtime + 0.1;
}

void CAR2BallSupply::Recharge(void)
{
	EmitSound( "SuitRecharge.Start" );
	ResetSequence( LookupSequence( "idle" ) );

	UpdateJuice( MaxJuice() );

	m_nState = 0;		
	m_flJuice = m_iJuice;
	m_iReactivate = 0;
	StudioFrameAdvance();

	SetThink( &CAR2BallSupply::SUB_DoNothing );
}

void CAR2BallSupply::SlowRecharge(void)
{
	if ( g_debug_ar2ball_supply.GetInt() == 3 ) Msg("[SlowRecharge: ");
	
	m_nState = 0;
	if (m_iOn > 0)
	{
		if ( g_debug_ar2ball_supply.GetInt() == 3 ) Msg("Stopsound ResetSeq");
		StopSound( "SuitRecharge.ChargingLoop" );
		ResetSequence( LookupSequence( "idle" ) );

		m_iOn = 0;
	}
	if ( g_debug_ar2ball_supply.GetInt() == 3 ) Msg("(%d / %d) ",m_iJuice,m_iMaxJuice);
	if ( m_iJuice < m_iMaxJuice )
	{
		if ( g_debug_ar2ball_supply.GetInt() == 3 ) Msg("< ");
		m_iJuice++;

		UpdateJuice( m_iJuice );
		m_flJuice = m_iJuice;

		StudioFrameAdvance();

		SetNextThink( gpGlobals->curtime + 0.05 );
		SetThink( &CAR2BallSupply::SlowRecharge );
	}
	else
	{
		if ( g_debug_ar2ball_supply.GetInt() == 3 ) Msg("Off ");
		Off();
		SetThink( &CAR2BallSupply::SUB_DoNothing );
	}
	if ( g_debug_ar2ball_supply.GetInt() == 3 ) Msg("]\n");
}

void CAR2BallSupply::Off(void)
{
	// Stop looping sound.
	if (m_iOn > 1)
	{
		StopSound( "SuitRecharge.ChargingLoop" );
	}
	
	if ( m_nState == 1 )
	{
		SetCycle( 1.0f );
	}

	m_iOn = 0;
	m_flJuice = m_iJuice;

	// This buggies out the SlowRecharge by overiding it, it'll automaticly recharge, so this is useless,
	// outcommented.

	//if ( m_iReactivate == 0 )
	//{
	//	if ((!m_iJuice) && g_pGameRules->FlHEVChargerRechargeTime() > 0 )
	//	{
	//		if ( HasSpawnFlags( SF_CITADEL_RECHARGER ) )
	//		{
	//			m_iReactivate = g_pGameRules->FlHEVChargerRechargeTime() * 2;
	//		}
	//		else
	//		{
	//			m_iReactivate = g_pGameRules->FlHEVChargerRechargeTime();
	//		}
	//		SetNextThink( gpGlobals->curtime + m_iReactivate );
	//		SetThink(&CAR2BallSupply::Recharge);
	//	}
	//	else
	//	{
	//		SetThink( NULL );
	//	}
	//}
}
