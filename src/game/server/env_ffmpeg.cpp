// Nate Nichols, InfoLab Northwestern University, August 2006.
#include "cbase.h"

#ifdef ENABLE_FFMPEG_IN_SOURCE

// server

#ifndef ffmpeg_MATERIAL_H
#define ffmpeg_MATERIAL_H

//This is the server side of the entity.  The corresponding client entity is C_ffmpegMaterial.
class CEnv_ffmpeg : public CBaseEntity
{
public:
	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();
	DECLARE_CLASS( CEnv_ffmpeg, CBaseEntity );
	CEnv_ffmpeg();
	void Spawn();
	void Play(inputdata_t &input);
	void Play();
	void Pause(inputdata_t &input);
	void SetMovie(inputdata_t &input);
	void AdvanceFrame(inputdata_t &input);

private:
	CNetworkVar(int, m_iPlay);
	CNetworkVar(bool, m_bLoop);
	CNetworkVar(string_t, m_iszTextureName);
	CNetworkVar(string_t, m_iszMovieName);
	CNetworkVar(float, m_fBegun);	
	CNetworkVar(float, m_fFrameRateHack);	
};

LINK_ENTITY_TO_CLASS( env_ffmpeg, CEnv_ffmpeg );

BEGIN_DATADESC(CEnv_ffmpeg)
	DEFINE_FIELD( m_iPlay, FIELD_INTEGER ),
	DEFINE_FIELD( m_bLoop, FIELD_BOOLEAN ),
	DEFINE_FIELD( m_fBegun, FIELD_FLOAT ),
	DEFINE_KEYFIELD( m_fFrameRateHack, FIELD_FLOAT, "FrameRate" ),
	DEFINE_KEYFIELD( m_iszTextureName, FIELD_STRING, "TextureName" ),
	DEFINE_KEYFIELD( m_iszMovieName, FIELD_STRING, "MovieName" ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Play", Play ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Pause", Pause ),
	DEFINE_INPUTFUNC( FIELD_STRING, "SetMovie", SetMovie ), 
END_DATADESC()

IMPLEMENT_SERVERCLASS_ST( CEnv_ffmpeg, DT_Env_ffmpeg )
	SendPropInt( SENDINFO( m_iPlay ), 1, SPROP_UNSIGNED ),
	SendPropInt( SENDINFO( m_bLoop ), 1, SPROP_UNSIGNED ),
	SendPropStringT( SENDINFO(m_iszTextureName)),
	SendPropStringT( SENDINFO(m_iszMovieName)),
	SendPropFloat( SENDINFO(m_fBegun)),
END_SEND_TABLE()


#endif //ffmpeg_MATERIAL_H

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define SF_ffmpeg_MATERIAL_BEGIN_PLAYING		0x00000001
#define SF_ffmpeg_MATERIAL_LOOP					0x00000002

CEnv_ffmpeg::CEnv_ffmpeg()
{
	m_iPlay				=	0;
	m_fFrameRateHack	=	0.0;
}

void CEnv_ffmpeg::Spawn()
{
	SetMoveType(MOVETYPE_NONE);
	SetSolid(SOLID_NONE);
	UTIL_SetSize(this, -Vector(2, 2, 2), Vector(2, 2, 2));
	AddEFlags(EFL_FORCE_CHECK_TRANSMIT);
	m_bLoop = HasSpawnFlags(SF_ffmpeg_MATERIAL_LOOP);
	if (HasSpawnFlags(SF_ffmpeg_MATERIAL_BEGIN_PLAYING))
	{
		Play();
	}
}

void CEnv_ffmpeg::Play(inputdata_t &input)
{
	Play();
}

void CEnv_ffmpeg::Play()
{
	m_fBegun	=	gpGlobals->curtime;
	m_iPlay++;
}

void CEnv_ffmpeg::Pause(inputdata_t &input)
{
	m_iPlay--;
}

void CEnv_ffmpeg::SetMovie(inputdata_t &input)
{
	m_iszMovieName = input.value.StringID();
}

#endif
