// Nate Nichols, InfoLab Northwestern University, August 2006.
#include "cbase.h"

// server

#ifndef AVI_MATERIAL_H
#define AVI_MATERIAL_H

//This is the server side of the entity.  The corresponding client entity is C_AVIMaterial.
class CEnv_Bink : public CBaseEntity
{
public:
	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();
	DECLARE_CLASS( CEnv_Bink, CBaseEntity );
	CEnv_Bink();
	void Spawn();
	void Play(inputdata_t &input);
	void Play();
	void Pause(inputdata_t &input);
	void SetMovie(inputdata_t &input);
	void AdvanceFrame(inputdata_t &input);

private:
	CNetworkVar(int, m_iPlay);
	CNetworkVar(bool, m_bLoop);
	CNetworkVar(int, m_iAdvanceFrame);
	CNetworkVar(string_t, m_iszTextureName);
	CNetworkVar(string_t, m_iszMovieName);
	CNetworkVar(float, m_fBegun);	
};

LINK_ENTITY_TO_CLASS( env_bink, CEnv_Bink );

BEGIN_DATADESC(CEnv_Bink)
	DEFINE_FIELD( m_iPlay, FIELD_INTEGER ),
	DEFINE_FIELD( m_bLoop, FIELD_BOOLEAN ),
	DEFINE_FIELD( m_iAdvanceFrame, FIELD_INTEGER ),
	DEFINE_FIELD( m_fBegun, FIELD_FLOAT ),
	DEFINE_KEYFIELD( m_iszTextureName, FIELD_STRING, "TextureName" ),
	DEFINE_KEYFIELD( m_iszMovieName, FIELD_STRING, "MovieName" ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Play", Play ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Pause", Pause ),
	DEFINE_INPUTFUNC( FIELD_VOID, "AdvanceFrame", AdvanceFrame ),
	DEFINE_INPUTFUNC( FIELD_STRING, "SetMovie", SetMovie ), 
END_DATADESC()

IMPLEMENT_SERVERCLASS_ST( CEnv_Bink, DT_Env_Bink )
	SendPropInt( SENDINFO( m_iPlay ), 1, SPROP_UNSIGNED ),
	SendPropInt( SENDINFO( m_bLoop ), 1, SPROP_UNSIGNED ),
	SendPropInt( SENDINFO( m_iAdvanceFrame ), 1, SPROP_UNSIGNED ),
	SendPropStringT( SENDINFO(m_iszTextureName)),
	SendPropStringT( SENDINFO(m_iszMovieName)),
	SendPropFloat( SENDINFO(m_fBegun)),
END_SEND_TABLE()


#endif //AVI_MATERIAL_H

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define SF_AVI_MATERIAL_BEGIN_PLAYING		0x00000001
#define SF_AVI_MATERIAL_LOOP				0x00000002

CEnv_Bink::CEnv_Bink()
{
	m_iPlay = 0;
	m_iAdvanceFrame = 0;
}

void CEnv_Bink::Spawn()
{
	SetMoveType(MOVETYPE_NONE);
	SetSolid(SOLID_NONE);
	UTIL_SetSize(this, -Vector(2, 2, 2), Vector(2, 2, 2));
	AddEFlags(EFL_FORCE_CHECK_TRANSMIT);
	m_bLoop = HasSpawnFlags(SF_AVI_MATERIAL_LOOP);
	if (HasSpawnFlags(SF_AVI_MATERIAL_BEGIN_PLAYING))
	{
		Play();
	}
}

void CEnv_Bink::Play(inputdata_t &input)
{
	Play();
}

void CEnv_Bink::Play()
{
	m_fBegun	=	gpGlobals->curtime;
	m_iPlay++;
}

void CEnv_Bink::Pause(inputdata_t &input)
{
	m_iPlay--;
}

void CEnv_Bink::SetMovie(inputdata_t &input)
{
	m_iszMovieName = input.value.StringID();
}

void CEnv_Bink::AdvanceFrame(inputdata_t &input)
{
	//C_AVIMaterial keeps track of the last m_iAdvanceFrame, so if it changes, 
	//C_AVIMaterial knows to advance one frame.
	m_iAdvanceFrame++;
}