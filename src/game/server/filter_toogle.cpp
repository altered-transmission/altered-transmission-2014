//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//========= Copyright � 2007, Maxsi.dk, All rights reserved. ============//
// filter_toogle.cpp
// Purpose: You can disable a filter by deleting it, you enable a filter by, eh,
// Purpose: ^^
//
// $NoKeywords: $
//
//=============================================================================//


#include "cbase.h"
#include "filters.h"

// ###################################################################
//	> CFilterToggle
// ###################################################################
class CFilterToggle : public CBaseFilter
{
	DECLARE_CLASS( CFilterToggle, CBaseFilter );
	DECLARE_DATADESC();

public:
	string_t m_iFilterName;
	bool	m_enabled;

	bool PassesFilterImpl( CBaseEntity *pCaller, CBaseEntity *pEntity )
	{
		return m_enabled;
	}
	void InputEnable( inputdata_t &inputdata )
	{
		m_enabled=true;
	}
	void InputDisable( inputdata_t &inputdata )
	{
		m_enabled=false;
	}

};

LINK_ENTITY_TO_CLASS( filter_toogle, CFilterToggle );

BEGIN_DATADESC( CFilterToggle )

	// Keyfields
	DEFINE_KEYFIELD( m_enabled,	FIELD_BOOLEAN,	"Enabled" ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Enable", InputEnable ),
	DEFINE_INPUTFUNC( FIELD_VOID, "Disable", InputDisable ),

END_DATADESC()