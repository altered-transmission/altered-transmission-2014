// LibAVCodecDecoder.cpp
#include "cbase.h"
#include "igamesystem.h"
#include "LibAVCodecDecoder.h"

#ifdef ALTERED_TRANSMISSION

CLibAVCodecSystem g_LibAVCodecSystem;
CLibAVCodecSystem* g_pLibAVCodecSystem = &g_LibAVCodecSystem;

//=============================================================================
// LibAVCodec System
//=============================================================================

CLibAVCodecSystem::CLibAVCodecSystem() :
	BaseClass( "CLibAVCodecSystem" ),
	m_IsPaused( true)
{
	
}

CLibAVCodecSystem::~CLibAVCodecSystem()
{
	
}

void CLibAVCodecSystem__LogCallback(void* P, int Level, const char* Format, va_list List)
{
	char Buffer[512];

	// TODO: This is a non-standard call that might cause buffer overruns!
	_vsnprintf(Buffer, sizeof(Buffer)-1, Format, List);

	Warning("FFmpeg: %s", Buffer);
}


bool CLibAVCodecSystem::Init()
{
	// Initialize the FFmpeg backend.
	av_register_all();
	av_log_set_level(AV_LOG_WARNING);

	av_log_set_callback(CLibAVCodecSystem__LogCallback);

	// Create a worker thread that decodes AV data.
	m_ThreadHandle = CreateSimpleThread(LibAVCodecDecoderThread, &m_ThreadData);

	if ( m_ThreadHandle == NULL )
	{
		Warning("Could not create libavcodec decoder thread!\n");
		return false;
	}

	return true;
}

void CLibAVCodecSystem::Shutdown()
{
	m_ThreadData.m_ShouldQuit = true;
	m_ThreadData.m_MessageEvent.Set();
	ThreadJoin(m_ThreadHandle);
}


bool CLibAVCodecSystem::AddPlayer(ILibAVCodecPlayer* New)
{
	m_ThreadData.m_MessageLock.Lock();
	
	New->Next = m_ThreadData.m_MessagedPlayer;
	New->Prev = NULL;
	m_ThreadData.m_MessagedPlayer = New;

	if ( m_ThreadData.m_MessagedPlayerLast == NULL ) { m_ThreadData.m_MessagedPlayerLast = New; }

	m_ThreadData.m_MessageLock.Unlock();

	m_ThreadData.m_MessageEvent.Set();

	return true;
}

// Gets called each frame
void CLibAVCodecSystem::Update(float frametime)
{
	if ( frametime == 0.0f ) { if ( !m_IsPaused ) { OnGamePaused(); } return; }
	if ( m_IsPaused ) { OnGameUnpaused(); }
}

void CLibAVCodecSystem::OnGamePaused()
{
	if ( m_IsPaused ) { return; }

	m_ThreadData.m_MessageLock.Lock();

	m_ThreadData.m_SignalGamePaused = true;
	m_ThreadData.m_SignalGameUnpaused = false;

	m_ThreadData.m_MessageLock.Unlock();

	m_IsPaused = true;
}

void CLibAVCodecSystem::OnGameUnpaused()
{
	if ( !m_IsPaused ) { return; }

	m_ThreadData.m_MessageLock.Lock();

	m_ThreadData.m_SignalGamePaused = true;
	m_ThreadData.m_SignalGameUnpaused = false;

	m_ThreadData.m_MessageLock.Unlock();

	m_IsPaused = false;
}

//=============================================================================
// LibAVCodec Decoder Thread
//=============================================================================

unsigned int LibAVCodecDecoderThread(void* pParam)
{
	return ((CLibAVCodecThread*) pParam)->Run();
}


CLibAVCodecThread::CLibAVCodecThread()
{
	m_ShouldQuit = false;
	m_MessagedPlayer = NULL;
	m_MessagedPlayerLast = NULL;
	m_First = NULL;
}

CLibAVCodecThread::~CLibAVCodecThread()
{
}

unsigned int CLibAVCodecThread::Run()
{
	while ( !m_ShouldQuit )
	{
		m_MessageEvent.Wait( (m_First) ? 0.05f : TT_INFINITE );

		// TODO: This might cause a memory leak!
		if ( m_ShouldQuit ) { break; }

		bool HasAnythingToDo = true;

		// Keep looping as long as just one player thinks it has plenty to do.
		while ( HasAnythingToDo )
		{
			HasAnythingToDo = false;

			m_MessageLock.Lock();

			// Add new players to our system.
			if ( m_MessagedPlayer != NULL )
			{
				m_MessagedPlayerLast->Next = m_First;
				if ( m_First ) { m_First->Prev = m_MessagedPlayerLast; }
				m_First = m_MessagedPlayer;
				m_MessagedPlayer = NULL;
				m_MessagedPlayerLast = NULL;
			}
			
			// These two must not be both true, since that 
			// means we are dealing with a race condition.
			bool SignalPaused = m_SignalGamePaused;
			bool SignalUnpaused = m_SignalGameUnpaused;

			m_MessageLock.Unlock();
			
			// Signal to the players whether the game is paused, or not.
			if ( SignalPaused ) { for ( ILibAVCodecPlayer* TMP = m_First; TMP != NULL; TMP = TMP->Next ) { TMP->OnGamePaused(); } }
			if ( SignalUnpaused ) { for ( ILibAVCodecPlayer* TMP = m_First; TMP != NULL; TMP = TMP->Next ) { TMP->OnGameUnpaused(); } }

			// Alright, perform an iteration on every player.
			for ( ILibAVCodecPlayer* TMP = m_First; TMP != NULL; TMP = TMP->Next )
			{
				int Result = TMP->DoIteration();

				if ( Result == -1 )
				{
					// The Player requested to be shut down, so drop it from our linked list.
					if ( TMP->Prev ) { TMP->Prev->Next = TMP->Next; }
					if ( TMP->Next ) { TMP->Next->Prev = TMP->Prev; }
					TMP->Next = NULL;
					TMP->Prev = NULL;

					// TODO: Possible memory leak here!
				}

				if ( Result == 1 )
				{
					// The player did some work and thinks it has more, but gives us a chance
					// to let other players do some work too.
					HasAnythingToDo = true;
				}
			}
		}
	}

	return 0;
}

//=============================================================================
// LibAVCodec Video Frame
//=============================================================================

CLibAVCodecVideoFrame::CLibAVCodecVideoFrame(int Width, int Height)
{
	// Create a RGB image. I doubt we need other color systems.
	Img = new uint8_t[3 * Width * Height];
	ImgWidth = Width;
	ImgHeight = Height;
}

CLibAVCodecVideoFrame::~CLibAVCodecVideoFrame()
{
	delete[] Img;
}

//=============================================================================
// LibAVCodec Player
//=============================================================================

CLibAVCodecPlayer::CLibAVCodecPlayer()
{
	m_FilePath = NULL;
	m_NewFilePath = NULL;

	m_FormatContext = NULL;
	m_AudioCodecContext = NULL;
	m_VideoCodecContext = NULL;

	m_AudioStreamId = -1;
	m_VideoStreamId = -1;

	m_AudioDest = NULL;
	m_VideoDest = NULL;

	m_HasPacket = false;

	m_AudioPacketData = NULL;
	m_AudioPacketDataUsed = 0;
	m_AudioPacketDataSize = 0;

	// TODO: What if we don't need audio?
	m_AudioReady = new int16_t[2 * AVCODEC_MAX_AUDIO_FRAME_SIZE / sizeof(int16_t)];
	m_AudioReadySize = 2 * AVCODEC_MAX_AUDIO_FRAME_SIZE;
	m_AudioReadyUsed = 0;
}

CLibAVCodecPlayer::~CLibAVCodecPlayer()
{
	if ( m_FilePath ) { delete[] m_FilePath; }
	if ( m_NewFilePath ) { delete[] m_NewFilePath; }
	if ( m_AudioReady ) { delete[] m_AudioReady; }
}

bool CLibAVCodecPlayer::LoadFile(const char* FilePath)
{
	if ( FilePath == NULL ) { return false; }
	
	char* FilePathCopy = new char[Q_strlen(FilePath) + 1];
	Q_strcpy(FilePathCopy, FilePath);

	m_Lock.Lock();

	if ( m_NewFilePath != NULL ) { delete[] m_NewFilePath; }
	m_NewFilePath = FilePathCopy;

	m_Lock.Unlock();

	return true;
}

void CLibAVCodecPlayer::Seek(float Position)
{
	// TODO: Seek to Position! This is a pretty important lacking feature!
}

void CLibAVCodecPlayer::SetAudioDest(ILibAVCodecAudioDest* Dest)
{
	m_AudioDest = Dest;
}

void CLibAVCodecPlayer::SetVideoDest(ILibAVCodecVideoDest* Dest)
{
	m_VideoDest = Dest;
}

int CLibAVCodecPlayer::DoIteration()
{
	// Return values:
	// < 0 means Player has nothing to do and should be removed.
	// 0 means Player is currently all good and should be called again soon.
	// 1 means the Player did some work and possibly have more if called again.

	if ( m_FormatContext == NULL )
	{
		if ( !DoLoadFile() ) { return -1; }
	}

	if ( m_AudioStreamId == -1 && m_VideoStreamId == -1 )
	{
		if ( !DoFindStreams() || !DoFindCodecs() ) { DoCloseFile(); return -1; }
	}

	int Result = DoDecoding();

	if ( Result < 0 ) { DoCloseFile(); return Result; }

	return Result;
}


void CLibAVCodecPlayer::OnGamePaused()
{
	// TODO: Not used yet.
}

void CLibAVCodecPlayer::OnGameUnpaused()
{
	// TODO: Not used yet.
}

bool CLibAVCodecPlayer::DoLoadFile()
{
	if ( m_FilePath != NULL ) { delete[] m_FilePath; m_FilePath = NULL; }

	m_Lock.Lock();

	if ( m_NewFilePath == NULL ) { m_Lock.Unlock(); return false; }

	m_FilePath = m_NewFilePath;

	m_NewFilePath = NULL;

	// TODO: Originally the lock was after the open call, which is inefficient --
	// I think it is harmless to do it before, but I don't have time to make sure.
	m_Lock.Unlock();

	int OpenResult = av_open_input_file(&m_FormatContext, m_FilePath, NULL, 0, NULL);

	if ( OpenResult == 0 )
	 {
		// After opening, we must search for the stream information because not all
		// formats will have it in stream headers (eg. system MPEG streams)
		if ( av_find_stream_info(m_FormatContext) < 0 )
		{
			Warning("CLibAVCodecPlayer: could not find stream information for %s\n", m_FilePath);
			av_close_input_file(m_FormatContext); m_FormatContext = NULL;
			return false;
		}

		return true;
	}

	Warning("CLibAVCodecPlayer: could not open %s\n", m_FilePath);

	return false;
}

void CLibAVCodecPlayer::DoCloseFile()
{
	if ( m_HasPacket ) { av_free_packet(&m_Packet); }

	if ( m_AudioCodecContext ) { avcodec_close(m_AudioCodecContext); m_AudioCodecContext = NULL; }
	if ( m_VideoCodecContext ) { avcodec_close(m_VideoCodecContext); m_VideoCodecContext = NULL; }

	av_close_input_file(m_FormatContext); m_FormatContext = NULL;

	m_AudioStreamId = -1;
	m_VideoStreamId = -1;

	m_AudioReadySize = 0;
	m_AudioReadyUsed = 0;

	if ( m_FilePath != NULL ) { delete[] m_FilePath; m_FilePath = NULL; }
}

bool CLibAVCodecPlayer::DoFindStreams()
{
	for ( unsigned int I = 0; I < m_FormatContext->nb_streams; I++ )
	{
		if ( m_AudioDest && m_AudioStreamId == -1 && m_FormatContext->streams[I]->codec->codec_type == CODEC_TYPE_AUDIO )
		{
			m_AudioStreamId = I;
		}

		if ( m_VideoDest && m_VideoStreamId == -1 && m_FormatContext->streams[I]->codec->codec_type == CODEC_TYPE_VIDEO )
		{
			m_VideoStreamId = I;
		}
	}

	// TODO: We might want to display the video regardless if no sound was found!

	if ( m_AudioDest && m_AudioStreamId == -1 )
	{
		Warning("CLibAVCodecPlayer: could not find audio stream in %s\n", m_FilePath);
		return false;
	}

	if ( m_VideoDest && m_VideoStreamId == -1)
	{
		Warning("CLibAVCodecPlayer: could not find video stream in %s\n", m_FilePath);
		return false;
	}

	return true;
}

bool CLibAVCodecPlayer::DoFindCodecs()
{
	if ( m_AudioStreamId != -1 )
	{
		m_AudioCodecContext = m_FormatContext->streams[m_AudioStreamId]->codec;
		AVCodec* AudioCodec = avcodec_find_decoder(m_AudioCodecContext->codec_id);

		if( AudioCodec == NULL )
		{
			Warning("CLibAVCodecPlayer: could not find codec for audio stream in %s\n", m_FilePath);
			m_AudioCodecContext = NULL;
			return false;
		}

		if( avcodec_open(m_AudioCodecContext, AudioCodec) < 0 )
		{
			Warning("CLibAVCodecPlayer: could not open codec for audio stream in %s\n", m_FilePath);
			m_AudioCodecContext = NULL;
			return false;
		}

		// Ask our audio dest to prepare for this stream format, or fail if the dest doesn't support it.
		// Currently, ffmpeg always decodes audio (even 8-bit PCM) to 16-bit PCM.
		if ( !m_AudioDest->OnAudioInformation(m_AudioCodecContext->sample_rate, m_AudioCodecContext->channels, 16) )
		{
			Warning("CLibAVCodecPlayer: audio dest did not support rate = %i, channels = %i, bits = %i for audio stream in %s\n", m_VideoCodecContext->sample_rate, m_VideoCodecContext->channels, 16, m_FilePath);
			return false;
		}
	}

	if ( m_VideoStreamId != -1 )
	{
		m_VideoCodecContext = m_FormatContext->streams[m_VideoStreamId]->codec;
		AVCodec* VideoCodec = avcodec_find_decoder(m_VideoCodecContext->codec_id);

		if( VideoCodec == NULL )
		{
			Warning("CLibAVCodecPlayer: could not find codec for video stream in %s\n", m_FilePath);
			m_VideoCodecContext = NULL;
			return false;
		}

		if( avcodec_open(m_VideoCodecContext, VideoCodec) < 0 )
		{
			Warning("CLibAVCodecPlayer: could not open codec for video stream in %s\n", m_FilePath);
			m_VideoCodecContext = NULL;
			return false;
		}

		// Ask our video dest to prepare for this stream format, or fail if the dest doesn't support it.
		if ( !m_VideoDest->OnVideoInformation(m_VideoCodecContext->width, m_VideoCodecContext->height) )
		{
			Warning("CLibAVCodecPlayer: video dest did not support resolution %ix%i for video stream in %s\n", m_VideoCodecContext->width, m_VideoCodecContext->height, m_FilePath);
			return false;
		}
	}

	return true;
}

int CLibAVCodecPlayer::DoDecoding()
{	
	// Check if we have nothing to do, and if so, simply return 0, which means we will be called again soon.
	//if ( ( m_VideoDest && (AvailableVideoFrame = m_VideoDest->AvailableVideoFrame()) == NULL || !m_VideoDest ) && 
	//	 ( m_AudioDest && (AvailableSoundBuffer = m_AudioDest->AvailableAudioBufferSpace()) == 0 || !m_AudioDest ) ) { return 0; }

	if ( !m_HasPacket )
	{
		// Fetch a data packet if we don't have one pending already.
		if ( av_read_frame(m_FormatContext, &m_Packet) < 0 )
		{
			// TODO: How do we tell if this was an IO error or simply the end of file?
			DevWarning("CLibAVCodecPlayer: don't know whether error was an IO error or an EOF condition for %s\n", m_FilePath);
			return -1;
		}

		m_HasPacket = true;
	}

	int Result = 1;

	if ( m_AudioDest && m_AudioStreamId == m_Packet.stream_index )
	{
		// Attempt to decode this packet of audio and return 0 if not ready yet.
		Result = DoAudioDecoding();
		if ( Result == 0 ) { return 0; }
	}
	else if ( m_VideoDest && m_VideoStreamId == m_Packet.stream_index )
	{
		// Attempt to decode this packet of video and return 0 if not ready yet.
		Result = DoVideoDecoding();
		if ( Result == 0 ) { return 0; }
	}

	// Free the packet, now that we don't use it any longer.
	av_free_packet(&m_Packet);
	m_HasPacket = false;

	// We did some work, which means that we can likely do more immediately!
	// But m_First, let's give other Players a chance to also do some work. 
	return Result;
}

int CLibAVCodecPlayer::DoAudioDecoding()
{
	// Simply precache how large a buffer is.
	size_t AudioRequestedSize = m_AudioDest->AvailableAudioBufferSpace();

	// See if we know how large a buffer we need to provide, or try again later.
	if ( AudioRequestedSize == 0 ) { return 0; }

	// Don't decode any more data, if we already have enough.
	if ( m_AudioReadyUsed < AudioRequestedSize )
	{
		// Alright, let's decode some data.

		// Prepare a destination buffer that has the required padding!
		if ( m_AudioPacketData == NULL )
		{
			m_AudioPacketData = new uint8_t[m_Packet.size + FF_INPUT_BUFFER_PADDING_SIZE];
			m_AudioPacketDataSize = m_Packet.size;
			m_AudioPacketDataUsed = 0;
		}
		else if ( m_AudioPacketDataSize < m_AudioPacketDataUsed + m_Packet.size )
		{
			// We don't have room for the new data in our buffer. Simply allocate a new
			// bigger, better and uncut one.
			uint8_t* NewAudioPacketData = new uint8_t[m_AudioPacketDataSize + m_Packet.size + FF_INPUT_BUFFER_PADDING_SIZE];
			memcpy(NewAudioPacketData, m_AudioPacketData, m_AudioPacketDataUsed);
			m_AudioPacketDataSize += m_Packet.size;
			delete[] m_AudioPacketData;
			m_AudioPacketData = NewAudioPacketData;
		}
		
		// Copy the packet data into our destination buffer and set the padding to zero.
		memcpy(m_AudioPacketData + m_AudioPacketDataUsed, m_Packet.data, m_Packet.size);
		memset(m_AudioPacketData + m_AudioPacketDataUsed + m_Packet.size, 0, FF_INPUT_BUFFER_PADDING_SIZE);
		m_AudioPacketDataUsed += m_Packet.size;

		// Time to attempt the actual decoding, or try again later if decoding wasn't possible yet!
		
		int BytesAvailable = m_AudioReadySize - m_AudioReadyUsed;
		int DecodedSize = avcodec_decode_audio2(m_AudioCodecContext, (int16_t*) ( ((uint8_t*)m_AudioReady) + m_AudioReadyUsed ), &BytesAvailable, m_AudioPacketData, (int) m_AudioPacketDataUsed);

		if ( DecodedSize < 0 )
		{
			Warning("CLibAVCodecPlayer: error %i decoding packets for audio stream in %s\n", DecodedSize, m_FilePath);
			return -1;
		}

		if ( DecodedSize == 0 )
		{
			// Alright, we don't have enough packet data yet. Simply request more.
			return 1;
		}

		// We don't need the read bytes any longer, so move the unread data in the packet buffer to the front.
		if ( (size_t) DecodedSize < m_AudioPacketDataUsed ) { memmove(m_AudioPacketData, m_AudioPacketData + DecodedSize, m_AudioPacketDataUsed - DecodedSize); }
		m_AudioPacketDataUsed -= DecodedSize;

		// We decoded BytesAvailable bytes of audio.
		m_AudioReadyUsed += BytesAvailable;
	}

	// Check if we finally have enough data for our source.
	if ( AudioRequestedSize <= m_AudioReadyUsed )
	{
		// Alright let's feed our audio dest for now, and let's return later and
		// feed it some more (there could already be plenty of data in m_AudioReady!).
		if ( !m_AudioDest->OnAudioData(m_AudioReady, AudioRequestedSize) )
		{
			// Something went wrong when forwarding the sound, so let's shut down.
			return -1;
		}

		// Remove the forwarded data from the m_AudioReady buffer, and move the rest to the start of the buffer.
		if ( AudioRequestedSize < m_AudioReadyUsed ) { memmove(m_AudioReady, m_AudioReady + AudioRequestedSize / sizeof(int16_t), m_AudioReadyUsed - AudioRequestedSize); }
		m_AudioReadyUsed -= AudioRequestedSize;
	}

	return 1;
}

int CLibAVCodecPlayer::DoVideoDecoding()
{
	// TODO: We clearly need to decode video data here and forward it to the video destination!
	return 1;
}

#endif
