
// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was taken from the SDK wiki page on procedural materials.


#include "cbase.h"

#ifdef ENABLE_FFMPEG_IN_SOURCE

extern "C"
{
	#include <avcodec.h>
	#include <avformat.h>
	#include <swscale.h>
	#include <opt.h>
}

#include "env_ffmpeg.h"
#include "ffmpeg_proxy.h"
#include "ffmpegMaterialRegen.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// Declare some useful global variables.
CffmpegMaterialProxy** g_pffmpegProxies = NULL;
size_t g_iffmpegProxiesSize = 0;
size_t g_iNumProxies = 0;

void AllocateAtLeastNffmpegProxies(size_t N)
{
	if ( g_pffmpegProxies == NULL )
	{
		g_pffmpegProxies = new CffmpegMaterialProxy*[16];
		g_iffmpegProxiesSize = 16;
	}
	else
	{
		N = ( N > g_iffmpegProxiesSize * 2 ) ? N : g_iffmpegProxiesSize * 2;
		CffmpegMaterialProxy** New = new CffmpegMaterialProxy*[N];
		memcpy(New, g_pffmpegProxies, sizeof(CffmpegMaterialProxy*) * g_iNumProxies);
		g_pffmpegProxies = New;
		g_iffmpegProxiesSize = N;		
	}
}

void AddffmpegProxy(CffmpegMaterialProxy* Proxy)
{
	AllocateAtLeastNffmpegProxies(g_iNumProxies + 1);
	g_pffmpegProxies[g_iNumProxies] = Proxy;
	g_iNumProxies++;
}

void RemoveffmpegProxy(CffmpegMaterialProxy* Proxy)
{
	if ( g_iNumProxies == 0 ) { return; }

	size_t Id = SIZE_MAX;

	for ( size_t I = 0; I < g_iNumProxies; I++ ) { if ( Proxy == g_pffmpegProxies[I] ) { Id = I; break; } }

	if ( Id == SIZE_MAX ) { return; }

	if ( g_iNumProxies == 1 )
	{
		delete[] g_pffmpegProxies;
		g_pffmpegProxies = NULL;
		g_iNumProxies = 0;
	}
	else
	{
		g_iNumProxies--;
		g_pffmpegProxies[Id] = g_pffmpegProxies[g_iNumProxies];
	}
}


CffmpegMaterialProxy* LookupffmpegByTextureName(const char* textureName)
{
	for ( size_t I = 0; I < g_iNumProxies; I++ )
	{
		if ( Q_stricmp(g_pffmpegProxies[I]->m_pTexture->GetName(), textureName) == 0 ) { return g_pffmpegProxies[I]; }
	}

	Warning("LookupffmpegByTextureName couldn't find a ffmpeg proxy for material '%s'\n", textureName);
	
	return NULL;
}

IMaterial* CffmpegMaterialProxy::GetMaterial()
{
	return m_pMaterial;
}

CffmpegMaterialProxy::CffmpegMaterialProxy()
{
	m_pTextureVar	=	NULL;
	m_pTexture		=	NULL;
	m_pTextureRegen	=	NULL;
	m_VideoEntity	=	NULL;

	AddffmpegProxy(this);
}

CffmpegMaterialProxy::~CffmpegMaterialProxy()
{
	if (m_pTexture)
	{
		m_pTexture->SetTextureRegenerator( NULL );
	}

	delete m_pTextureRegen;

	RemoveffmpegProxy(this);
}


bool CffmpegMaterialProxy::Init( IMaterial *pMaterial, KeyValues *pKeyValues )
{
	bool found;

	m_pMaterial		=	pMaterial;
	
	m_pTextureVar	=	pMaterial->FindVar("$basetexture", &found, false);  // Get a reference to our base texture variable

	if( !found )
	{
		m_pTextureVar = NULL;
		return false;
	}

	m_pTexture = m_pTextureVar->GetTextureValue();  // Now grab a ref to the actual texture
	
	if (m_pTexture != NULL)
	{
#ifdef _DEBUG
		//Msg("%s has image format %d\n",m_pTexture->GetName(),m_pTexture->GetImageFormat());
#endif
		m_pTextureRegen = new CffmpegTextureRegen();
		m_pTextureRegen->m_VideoEntity	=	m_VideoEntity;
		// Convert the image to RGBA8888 - That should make it easier to copy
		// RGB data into the buffer.
		m_pTexture->SetTextureRegenerator(m_pTextureRegen); // And here we attach it to the texture.
	}

	return true;
}

//-----------------------------------------------------------------------------
// Called when the texture is bound...
//-----------------------------------------------------------------------------
void CffmpegMaterialProxy::OnBind( void *pEntity )
{
	if( !m_pTexture )  // Make sure we have a texture
		return;

	if( !m_pTextureVar->IsTexture() )  // Make sure it is a texture
		return;

	m_pTexture->Download(); // Force the regenerator to redraw
}

void CffmpegMaterialProxy::Release()
{ 
	delete this; 
}

#endif