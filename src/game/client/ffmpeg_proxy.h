// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was taken from the SDK wiki page on procedural materials.
#ifndef FFPEG_PROXY_H
#define FFPEG_PROXY_H

#include "ProxyEntity.h"
#include "materialsystem/IMaterialVar.h"
#include "materialsystem/ITexture.h"
#include "bitmap/TGALoader.h"
#include "view.h"
#include "datacache/idatacache.h"
#include "materialsystem/IMaterial.h"
#include "materialsystem/IMaterialProxy.h"
#include "ffmpegMaterialRegen.h"

#define MAX_VIDEOS 32

class CffmpegMaterialProxy : public IMaterialProxy
{
public:
	CffmpegMaterialProxy();
	~CffmpegMaterialProxy();
	bool Init( IMaterial *pMaterial, KeyValues *pKeyValues );
	//bool SetMovie(const char* filename);
	void OnBind(void *pC_BaseEntity );
	void Release();
	virtual IMaterial*	GetMaterial();
	//void Play() {m_pTextureRegen->Play();}
	//void Pause() {m_pTextureRegen->Pause();}
	//void Stop() {m_pTextureRegen->Stop();}
	//void AdvanceFrame() {m_pTextureRegen->AdvanceFrame();}
	//oid SetLooping(bool bLoop) {m_pTextureRegen->SetLooping(bLoop);}
	static CffmpegMaterialProxy** MakeNullArray();

	IMaterialVar		*m_pTextureVar;   // The material variable
	ITexture			*m_pTexture;      // The texture
	C_Env_ffmpeg		*m_VideoEntity;   // Entity handling the video
	IMaterial			*m_pMaterial;
	CffmpegTextureRegen	*m_pTextureRegen; // The regenerator

};

CffmpegMaterialProxy* LookupffmpegByTextureName(const char* textureName);
void AllocateAtLeastNffmpegProxies(size_t N);
void AddffmpegProxy(CffmpegMaterialProxy* Proxy);
void RemoveffmpegProxy(CffmpegMaterialProxy* Proxy);

EXPOSE_INTERFACE( CffmpegMaterialProxy, IMaterialProxy, "ffmpegRenderer" IMATERIAL_PROXY_INTERFACE_VERSION );

#endif //ffmpeg_MATERIAL_PROXY_H