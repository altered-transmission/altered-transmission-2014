// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was taken from the SDK wiki page on procedural materials.


#include "cbase.h"
// OBSOLETE OLD STUFF
#if 0
#include "AVIMaterialProxy.h"
// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


//#pragma warning (disable:4355)

CAviMaterialProxy** CAviMaterialProxy::MakeNullArray()
{
	CAviMaterialProxy** ret = new CAviMaterialProxy*[MAX_AVIS];
	for (int i = 0; i < MAX_AVIS; i++)
	{
		ret[i] = NULL;
	}
	return ret;
}
//This static junk is an array of all the proxies.
//It is used by C_AVIMaterial to connect to the appropriate proxy.  I'm sure there is a better way to do this.
CAviMaterialProxy** CAviMaterialProxy::g_pAviProxies = MakeNullArray();
int CAviMaterialProxy::g_iNextProxy = 0;


CAviMaterialProxy::CAviMaterialProxy()
{
	m_pTextureVar = NULL;
	m_pTexture = NULL;
	m_pTextureRegen = NULL;

	g_pAviProxies[g_iNextProxy++] = this;
}

//#pragma warning (default:4355)

CAviMaterialProxy::~CAviMaterialProxy()
{
	if (m_pTexture)
		m_pTexture->SetTextureRegenerator( NULL );

	delete m_pTextureRegen;
	g_iNextProxy--;
	if (g_iNextProxy == 0)
	{//I was the last Proxy, delete the static array too.
		delete[] g_pAviProxies;
	}
}

CAviMaterialProxy* CAviMaterialProxy::LookupByTextureName(const char* textureName)
{
	// TODO: Make this comparison case independent
	for (int i = 0; i < MAX_AVIS; i++)
	{
		if (g_pAviProxies[i])
		{
			if (!strcmp(g_pAviProxies[i]->m_pTexture->GetName(), textureName))
				return g_pAviProxies[i];
		}
	}
	Msg("THESE ARE NOT THE TEXTURES YOU'RE LOOKING FOR!!!!!!!!!!!");
	return NULL;
}


bool CAviMaterialProxy::Init( IMaterial *pMaterial, KeyValues *pKeyValues )
{
	bool found;
	
	m_pTextureVar = pMaterial->FindVar("$basetexture", &found, false);  // Get a reference to our base texture variable
	if( !found )
	{
		m_pTextureVar = NULL;
		return false;
	}

	m_pTexture = m_pTextureVar->GetTextureValue();  // Now grab a ref to the actual texture
	if (m_pTexture != NULL)
	{
		m_pTextureRegen = new CAviTextureRegen();
		m_pTexture->SetTextureRegenerator(m_pTextureRegen); // And here we attach it to the texture.
	}

	return true;
}

bool CAviMaterialProxy::SetMovie(const char* filename)
{
	if (m_pTextureRegen)
		return m_pTextureRegen->SetMovie(filename);
	return false;
}

//-----------------------------------------------------------------------------
// Called when the texture is bound...
//-----------------------------------------------------------------------------
void CAviMaterialProxy::OnBind( void *pEntity )
{
if( !m_pTexture )  // Make sure we have a texture
		return;

	if(!m_pTextureVar->IsTexture())  // Make sure it is a texture
		return;

	m_pTexture->Download(); // Force the regenerator to redraw
}

void CAviMaterialProxy::Release()
{ 
	delete this; 
}

IMaterial* CAviMaterialProxy::GetMaterial()
{ 
	return 0;
}
#endif 