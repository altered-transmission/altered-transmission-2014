// Nate Nichols, InfoLab Northwestern University, August 2006.
#ifndef C_AVI_MATERIAL_H
#define C_AVI_MATERIAL_H

#include "AVIMaterialProxy.h"

//This is the client side of the entity.  The corresponding server entity is C_AVIMaterial.
class C_AVIMaterial : public C_BaseEntity
{
public:
	DECLARE_CLIENTCLASS();
	DECLARE_CLASS( C_AVIMaterial, C_BaseEntity );
	C_AVIMaterial();
	void OnDataChanged(DataUpdateType_t updateType);

private:
	int m_iPlay;
	int m_iLastPlay;
	bool m_bLoop;
	int m_iAdvanceFrame;
	int m_iLastAdvanceFrame;
	CAviMaterialProxy* m_pProxy;
	char m_iszTextureName[256];	//These arrays may leak, but I don't think so.
	char m_iszMovieName[256];
	char m_iszLastMovieName[256];
};


IMPLEMENT_CLIENTCLASS_DT(C_AVIMaterial, DT_AVIMaterial, CAVIMaterial)
	RecvPropInt(RECVINFO(m_iPlay)),
	RecvPropInt(RECVINFO(m_bLoop)),
	RecvPropInt(RECVINFO(m_iAdvanceFrame)),
	RecvPropString(RECVINFO(m_iszTextureName)),
	RecvPropString(RECVINFO(m_iszMovieName)),
END_RECV_TABLE()




#endif //C_AVI_MATERIAL_H