#include "cbase.h"
#if 0
class CSunShaftEffect : public IScreenSpaceEffect
{
public:
	CSunShaftEffect( void ) { };
 
	virtual void Init( void );
	virtual void Shutdown( void );
	virtual void SetParameters( KeyValues *params );
	virtual void Enable( bool bEnable ) { m_bEnabled = bEnable; }
	virtual bool IsEnabled( ) { return m_bEnabled; }
 
	virtual bool ShaftsRendering( void );

	virtual void Render( int x, int y, int w, int h );
 
private:
	bool				m_bEnabled;

	//bool				bSunVisible;
	bool				bMapHasSun;
	bool				bFadeToZero;
	bool				bSunIsBehind;

	Vector				sunPos;

	float				fDensity;
	float				fDecay;
	float				fWeight;
	float				fExposure;
	float				fBlur;
	float				fBlurAddition;
	float				fBlurAdditionLerpTo;
	float				fAlpha;
	float				fAlphaLerpTo;

	CTextureReference	m_SunShaftBlend;
	CTextureReference	m_SunShaftBlend_Blur;

	CMaterialReference	m_SunShaft;
	CMaterialReference	m_SunDebug;
	CMaterialReference	m_SunShaft_BlurX;
	CMaterialReference	m_SunShaft_BlurY;

	CMaterialReference	m_SunShaftBlendMat;
};
#endif