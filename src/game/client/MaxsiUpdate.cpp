/****************************************************************************

	COPYRIGHT(C) WWW.MAXSI.DK, MAXSI SOFTWARE, SORTIE 2008-2009
	ALL RIGHTS RESERVED

	Client.dll
	A client-side interface for a mod on the Source Engine

	MaxsiUpdate.cpp
	Functions that ensure this product is up to date.

	How to use:
	1) Include MaxsiUpdate.cpp and MaxsiUpdate.h in your project
		
		Include them in both Client.dll and Server.dll for optimal
		performance.

	2) Include "MaxsiUpdate.h" in cdll_client_int.cpp.
	   Add the following code in cdll_client_int.cpp to the function CHLClient::Init() just
	   before the function returns true.
	   
		if (!MaxsiDistribution::UpdateProduct() ||
			!MaxsiDistribution::ReportCrashes()  )
		{
			return false;
		}

	3) Do a small number of (currently undocumented) changes to your game source
	   to successfully utilize the Maxsi Distribution technologies such as Statistics,
	   Achievements, and more.

	4) Compile

	5) Make sure the SourceMods/Maxsi Distribution/ folder is distributed alongside your
	   mod, although this should be done automatically if you distribute using Maxsi
	   Distribution in the first place.

****************************************************************************/

#include "cbase.h"

/****************************************************************************
	PLEASE CUSTOMIZE THE FOLLOWING
****************************************************************************/

// Enter the name of the folder this mod is within, for instance,
// c:/program files/valve/steam/sourcemods/Altered Transmission/
// should be "Altered Transmission"

#define	MOD_FOLDER_NAME		"Altered Transmission"
#define	MOD_GLOBAL_NAME		"Altered Transmission - Demo 10"
#define	MOD_GLOBAL_VERSION	"Demo 10"
#define	MOD_SERVER_NAME		"Altered+Transmission+-+Demo+10"
#define	MOD_SERVER_VERSION	"AT10"

/****************************************************************************
	DON'T EDIT ANYTHING BELOW.
	(Unless you know what you are doing, of course)
****************************************************************************/

/****************************************************************************
	Defines
****************************************************************************/
#undef INVALID_HANDLE_VALUE
#undef GetCommandLine
#undef ReadConsoleInput
#undef RegCreateKey
#undef RegCreateKeyEx
#undef RegOpenKey
#undef RegOpenKeyEx
#undef RegQueryValue
#undef RegQueryValueEx
#undef RegSetValue
#undef RegSetValueEx

#include <windows.h>
#include <io.h>
#include <direct.h>
#include <iostream> 
#include <fcntl.h>
#include <SYS\Stat.h>
#include "MaxsiUpdate.h"

#define DECLARE_FUNCTION(return_type, name, parameters) typedef return_type (__cdecl* type_##name  ) parameters ; type_##name name;
#define IMPORT_FUNCTION(Executable,name)				name = reinterpret_cast<type_##name>(GetProcAddress(Executable, #name ));

#define	RESULT_DONT_RUN						0
#define	RESULT_RUN							1
#define	RESULT_UPDATE_UPDATING_SOFTWARE		2
#define	RESULT_UPDATE_UPDATED_SOFTWARE		3
#define	RESULT_UPDATE_UPDATING_FAILED		4
#define	RESULT_INVALID_PARAMETER_SIZE		5

// Should help players get the exact support information.
#define	SupportId(Id) "ErrorID: ME" #Id ", see www.maxsi.dk/support/ for more information."

namespace MaxsiDistribution
{

	//struct UpdateProductArguments_v1
	//{
	//	size_t Size; // = sizeof(UpdateProductArguments_v1);
	//	char* FolderName;
	//	char* WorkingDir;
	//	char* Username;
	//	char* Product;
	//	char* ProductVersion;
	//	char* DownloadName;
	//	char* DownloadVersion;
	//	char* GameFolder;
	//};


	//struct UpdateProductArguments 
	//{
	//	size_t Size; // = sizeof(UpdateProductArguments);
	//	char* FolderName;
	//	char* WorkingDir;
	//	char* Username;
	//	char* Product;
	//	char* ProductVersion;
	//	char* DownloadName;
	//	char* DownloadVersion;
	//	char* GameFolder;
	//	char* Parameter; // Added in Version 2.
	//	int	  Version;	 // = 2
	//};

	char* MDSeekpaths[] =
	{
		"../../Maxsi Distribution/",
		"",
	};
		
	#define	SEEK_PATHS								2

	char* Distributions[] =
	{
		"distribution.override", // Decides what distribution to use by loading the distribution.override file and using whatever is written in it.
		// Please do not remove distribution.override from here. People should be able to override any
		// settings regarding what distribution they want to use. However, if you prefer that people
		// use your custom distrubution of Maxsi Distribution, do add it beneath,

		"Maxsi/",				// Loads the official Maxsi build.
		"",						// Loads whatever is in the search path folder.
	};

	#define	DISTRIBUTIONS							3

	char* BinFolders[] =
	{
		"dev/",	// Loads whatever is in the dev/ folder. The dev/ folder should be be included in public releases.
		"bin/", // Loads the official release of the current distribution of Maxsi Distribution.
		"",		// Loads from the search path folder.
	};

	#define	BINARY_FOLDERS							3

	char* Binaries[] =
	{
		"MaxsiDistribution.dll",
		"MaxsiDistributionUpdate.dll",
		"MaxsiFeedback.exe",
		"MaxsiInstaller.exe",
	};

	#define	BINARY_MAXSIDISTRIBUTION_DLL			0
	#define	BINARY_MAXSIDISTRIBUTIONUPDATE_DLL		1
	#define	BINARY_MAXSIFEEDBACK_EXE				2
	#define	BINARY_MAXSIINSTALLER_EXE				3

/****************************************************************************
	Common functions from MaxsiEngine.dll
****************************************************************************/

	char* BuildString(unsigned int NumParameters, ...)
	{
		va_list param_pt;

		va_start(param_pt,NumParameters); // Call the setup macro
		
		// First calculate the string length

		size_t FinalString_Len = 0;
		char* TMPString = 0;

		for (unsigned int I = 0 ; I < NumParameters ; I++)
		{
			TMPString = va_arg(param_pt,char*);
			if ( TMPString )
			{
				FinalString_Len+=strlen(TMPString);
			}
		}
		FinalString_Len++;


		// Allocate the required string
		char* FinalString = new char[FinalString_Len+10];
		FinalString[0] = 0;

		va_end(param_pt); // Closing macro
		va_start(param_pt,NumParameters); // Call the setup macro

		for (unsigned int I = 0 ; I < NumParameters ; I++)
		{
			TMPString = va_arg(param_pt,char*);
			if ( TMPString )
			{
				strcat_s(FinalString,FinalString_Len,TMPString);
			}
		}
		
		return FinalString;
	}

	bool str_replace(char* input, char* find, char* replace)
	{
		if (!input||!find||!replace) { return false; }
		size_t input_Len = strlen(input);
		size_t find_Len = strlen(find);
		size_t replace_Len = strlen(replace);

		if (find_Len != replace_Len)
		{
			return false;
		}

		size_t X = 0, Y = 0;

		// Loop through X and check at each X if Y follows, if so return X
		for (X = 0; X < input_Len; X++)
		{
			for (Y = 0; Y < find_Len; Y++)
			{
				if ( input[X+Y] != find[Y] )
				{
					break; // Nope.
				}
			}
			if ( Y == find_Len )
			{
				memcpy(input+X,replace,find_Len);
			}
		}
		return true; // Done
	}

	char*	GetWorkingDir()
	{
		char*	CurrentDirectory		=	0;
		char*	CurrentDirectory_TMP	=	_getcwd(NULL,0);

		size_t CurrentDirectory_TMP_Len = strlen(CurrentDirectory_TMP);

		if (CurrentDirectory_TMP[CurrentDirectory_TMP_Len-1] == '\\' ||
			CurrentDirectory_TMP[CurrentDirectory_TMP_Len-1] == '/')
		{
			CurrentDirectory = new char[CurrentDirectory_TMP_Len+1];
			strcpy_s(CurrentDirectory,CurrentDirectory_TMP_Len+1,CurrentDirectory_TMP);
		}
		else
		{
			CurrentDirectory = new char[CurrentDirectory_TMP_Len+2];
			strcpy_s(CurrentDirectory,CurrentDirectory_TMP_Len+1,CurrentDirectory_TMP);
			memset(CurrentDirectory+CurrentDirectory_TMP_Len,'/',1);
			memset(CurrentDirectory+CurrentDirectory_TMP_Len+1,0,1);
		}

		free(CurrentDirectory_TMP);

		return CurrentDirectory;
	}
/****************************************************************************
	Purpose: An easy method to report any errors to the users.
****************************************************************************/
	bool	ReportError(const char* Error, bool DontShowNotepad)
	{
		IngameError(Error);
		int fh = 0;			
		char* WorkingDir = GetWorkingDir();
		size_t File_Len = strlen(WorkingDir) + strlen("MaxsiDistributionError.txt")+1;
		char* File = new char[File_Len];
		File[0] = 0;
		
		strcat_s(File,File_Len,WorkingDir);
		strcat_s(File,File_Len,"MaxsiDistributionError.txt");

		_sopen_s(&fh,File, _O_CREAT | O_TRUNC | O_BINARY | O_WRONLY, _SH_DENYNO, _S_IREAD | _S_IWRITE);

		if (fh == -1)
		{
			delete[] WorkingDir;
			delete[] File;
			return false;
		}
		else
		{
			char* Header = "An occured occured when linking with MaxsiDistribution.dll. Please contact the developers for more information, or contact Maxsi.dk.\r\n\r\n";
			_write(fh,Header,strlen(Header));
			_write(fh,Error,strlen(Error));
			_close(fh);		
		}
		
		size_t ReadMeCommandSize = strlen("notepad \"") + strlen(File) + 1;
		char* ReadMeCommand = new char[ReadMeCommandSize];
		ReadMeCommand[0] = 0;
		
		strcat_s(ReadMeCommand,ReadMeCommandSize,"notepad \"");
		strcat_s(ReadMeCommand,ReadMeCommandSize,File);

		if ( !DontShowNotepad )
		{
			WinExec(ReadMeCommand,SW_SHOW);
		}
		delete[] ReadMeCommand;
		delete[] WorkingDir;
		delete[] File;
		
		return true;
	}

	// Quick console error reporting
	bool IngameError(const char* Error)
	{
		Color ConsoleColor(100,255,100,255);
		ConColorMsg(ConsoleColor,"Maxsi Distribution Error: ");
		ConColorMsg(ConsoleColor,Error);
		ConColorMsg(ConsoleColor,"\n");
		return true;
	}

	// Quick console information
	bool IngameInfo(const char* Error)
	{
		Color ConsoleColor(100,255,100,255);
		ConColorMsg(ConsoleColor,"Maxsi Distribution Info: ");
		ConColorMsg(ConsoleColor,Error);
		ConColorMsg(ConsoleColor,"\n");
		return true;
	}

	// Returns true if the product can be run, false if an update-process has begun.
	bool	UpdateProduct( void )
	{
		return ExecuteMDDLL("UpdateProduct");
	}

	//Uploads all the crashes we need.
	bool	ReportCrashes( void )
	{
		return ExecuteMDDLL("ReportCrashes");
	}

	char*	GetSteamUsername()
	{
		char*	WorkingDir		=	GetWorkingDir();
		str_replace(WorkingDir,"\\","/");
		size_t	WorkingDirLen	=	strlen(WorkingDir);
		size_t	UsernameBegin	=	0;
		size_t	UsernameEnd		=	0;
		size_t	UsernameLen		=	0;
		char*	Username		=	0;
		int		NumSlashes		=	0;

		for (size_t N = WorkingDirLen; N > 0; N--)
		{
			if (WorkingDir[N] == '/'||
				WorkingDir[N] == '\\')
			{
				NumSlashes++;
				if ( NumSlashes == 1)
				{
				}
				else if ( NumSlashes == 2)
				{
					UsernameEnd				=	N-1;
				}
				else if ( NumSlashes == 3)
				{
					// Gather the username
					UsernameBegin			=	N+1;
					UsernameLen				=	UsernameEnd-UsernameBegin+1;
					Username				=	new char[UsernameLen+1];

					memcpy(Username,WorkingDir+UsernameBegin,UsernameLen);

					Username[UsernameLen]	=	0;
				}
			}
		}
		return Username;
	}


	// Links to the MD DLL and executes the given function
	bool	ExecuteMDDLL( char* Func, char* Parameter )
	{
		// Warning: bool	ExecuteMDDLL( char* Func, char* Parameter ) is disabled!
		return true;

		bool Result = false;
		char* GameDir = 0;

		if (engine)
		{
#ifdef GAME_DLL
			GameDir = new char[1024];
			engine->GetGameDir((char*)GameDir, 1024);
#else
			GameDir = (char*)engine->GetGameDirectory();
#endif
			if (!GameDir)
			{		
				ReportError("Maxsi Distribution Error: The in-game variable 'engine->GetGameDirectory()'  isn't set! " SupportId(1));
				return false;
			}
		}
		else
		{
			ReportError("Maxsi Distribution Error: The in-game variable 'engine' isn't set! " SupportId(2));
			return false;
		}

		char* BinFolder = BuildString(2,GameDir,"/bin/");
		char* WorkingDir = BuildString(1,GameDir);

		// Find the username!

		char*	Username	=	GetSteamUsername();
			
		HMODULE MaxsiDistributionLibrary = 0;

		int		iBinary			=	0;

		// Change the working directory
		if ( _chdir(BinFolder) != 0)
		{
			// Failure
			ReportError("Failed to change the working directory! " SupportId(3));
		}

		int		WhatTodo		=	0;
		bool	Relink			=	true;

		while (Relink)
		{	
			if ( WhatTodo == RESULT_UPDATE_UPDATING_SOFTWARE )
			{
				// The updating software needs to be updated!
				iBinary			=	BINARY_MAXSIDISTRIBUTION_DLL;		
			}
			else
			{
				iBinary			=	BINARY_MAXSIDISTRIBUTION_DLL;
			}

			// Go through all the search paths in order and find the best binary.
			for ( size_t I = 0; I < SEEK_PATHS; I++ )
			{
				for ( size_t J = 0; J < DISTRIBUTIONS; J++ )
				{
					// Todo!
					if ( _stricmp(Distributions[J],"distribution.override") == 0 ) { J++; }

					for ( size_t K = 0; K < BINARY_FOLDERS; K++ )
					{
						char*	Path				=	BuildString(5,MDSeekpaths[I],Distributions[J],BinFolders[K],Binaries[iBinary]);
						MaxsiDistributionLibrary	=	LoadLibraryA(Path);
						delete[] Path;

						if ( MaxsiDistributionLibrary ) { break; }
					}
				}
			}		

			if ( MaxsiDistributionLibrary )
			{
				typedef int (__cdecl* type_MDFunc  ) (UpdateProductArguments) ; type_MDFunc MDFunc;
				MDFunc = reinterpret_cast<type_MDFunc>(GetProcAddress(MaxsiDistributionLibrary, Func ));

				if ( MDFunc )
				{
					UpdateProductArguments Arg;

					Arg.DownloadName		=	MOD_SERVER_NAME;
					Arg.DownloadVersion		=	MOD_SERVER_VERSION;
					Arg.FolderName			=	(char*)GameDir;
					Arg.Product				=	MOD_GLOBAL_NAME;
					Arg.ProductVersion		=	MOD_GLOBAL_VERSION;
					Arg.Username			=	Username;
					Arg.WorkingDir			=	BinFolder;
					Arg.GameFolder			=	WorkingDir;
					Arg.Parameter			=	Parameter;
					Arg.Size				=	sizeof(Arg);

					WhatTodo = MDFunc(Arg);

					// Do what the library told us to do!

					if ( WhatTodo == RESULT_DONT_RUN )
					{
						Result = false;
						Relink = false;
					}
					else if ( WhatTodo == RESULT_RUN )
					{
						Result = true;
						Relink = false;
					}
					else if ( WhatTodo == RESULT_UPDATE_UPDATING_SOFTWARE )
					{
						Relink = true;
					}
					else if ( WhatTodo == RESULT_UPDATE_UPDATED_SOFTWARE )
					{
						Relink = true;
					}
					else if ( WhatTodo == RESULT_UPDATE_UPDATING_FAILED )
					{
						Relink = false;
						Result = false;
						ReportError("Maxsi Distribution Error:\r\n\r\nMaxsi Distribution failed to update itself for various reasons. " SupportId(4));	
					}
					else if ( WhatTodo == RESULT_INVALID_PARAMETER_SIZE )
					{
						Relink = false;
						Result = false;
						ReportError("Maxsi Distribution Error:\r\n\r\nMaxsiDistribution returned RESULT_INVALID_PARAMETER_SIZE " SupportId(38));	
					}
				}
				else
				{
					if ( WhatTodo == RESULT_UPDATE_UPDATING_SOFTWARE )
					{
						char*		Error		=		BuildString(3,"Maxsi Distribution Error:\r\n\r\nCouldn't find function ",Func," in MaxsiDistributionUpdate.dll. " SupportId(5));
						ReportError(Error);
						delete[]	Error;
					}
					else
					{						
						char*		Error		=		BuildString(3,"Maxsi Distribution Error:\r\n\r\nCouldn't find function ",Func," in MaxsiDistribution.dll. " SupportId(6));
						ReportError(Error);
						delete[]	Error;
					}
				}

				FreeLibrary(MaxsiDistributionLibrary);
				
				Relink = false;
			}
			else
			{
				LPTSTR pszMessage;
				DWORD dwLastError = GetLastError(); 

				FormatMessageA(
					FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					dwLastError,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR)&pszMessage,
					0, NULL );

				// Display the error message and exit the process

				ReportError(pszMessage);		

				LocalFree(pszMessage);
				Relink = false;
			}
		}

		// Restore the working directory
		_chdir(WorkingDir);

		delete[] BinFolder;
		//delete[] LibraryDLL;
		delete[] WorkingDir;
		delete[] Username;
#ifdef GAME_DLL
		delete[] GameDir;
#endif

		// If we cannot link, or the library requires the project to reboot in order to update, close the project.
		if (!Result)
		{
			PostQuitMessage(0);
		}
		return Result;
	}

	// Recieves pingbacks from the pingback function
	DECLARE_FUNCTION(bool,PingBackEngine,(char*));

	bool __cdecl PingBackEngineFunc(char* Message)
	{
		return IngameInfo(Message);
	}

	// Pings back async
	bool	PingBack( char* Function, char* Parameter )
	{		
		struct	PingBackArg
		{
			char*	Function;
			char*	Parameter;
			type_PingBackEngine PingBack;
		};

		PingBackArg	TheArg;

		TheArg.Function		=	Function;
		TheArg.Parameter	=	Parameter;
		TheArg.PingBack		=	PingBackEngineFunc;

		bool	Result		=	 ExecuteMDDLL("PingBack",(char*)&TheArg);

		return	Result;
	}


#ifdef CLIENT_DLL

	static bool AlreadyDefined = false;

	class LaunchFeedbackApplication
	{
	private:
		bool Should;

	public:
		LaunchFeedbackApplication()
		{
			Should			= !AlreadyDefined;
			AlreadyDefined	= true;
		}

		~LaunchFeedbackApplication()
		{		
			if (!Should)
			{
				return;
			}

			LPSTARTUPINFOA si			=	new _STARTUPINFOA;
			LPPROCESS_INFORMATION pi	=	new _PROCESS_INFORMATION;

			ZeroMemory( si, sizeof(_STARTUPINFOA) );
			si->cb = sizeof(si);
			ZeroMemory( pi, sizeof(_PROCESS_INFORMATION) );

#ifdef GAME_DLL
			char* GameDir = new char[1024];
			engine->GetGameDir((char*)GameDir, 1024);
#else
			char* GameDir = (char*)engine->GetGameDirectory();
#endif

			bool	Success		=	false;

			for ( size_t I = 0; I < BINARY_FOLDERS; I++ )
			{
				char* TheCommand = BuildString(8,"\"",GameDir,"/bin/",BinFolders[I],Binaries[BINARY_MAXSIFEEDBACK_EXE],"\" \"",GameDir,"\"");

				if( CreateProcessA( NULL,   // No module name (use command line)
					TheCommand,        // Command line
					NULL,           // Process handle not inheritable
					NULL,           // Thread handle not inheritable
					FALSE,          // Set handle inheritance to FALSE
					0,              // No creation flags
					NULL,           // Use parent's environment block
					NULL,           // Use parent's starting directory 
					si,            // Pointer to STARTUPINFO structure
					pi )           // Pointer to PROCESS_INFORMATION structure
				)
				{ 
					Success		=	true;

					CloseHandle( pi->hProcess );
					CloseHandle( pi->hThread );
					delete[] TheCommand;
					break;
				}

				delete[] TheCommand;
			}

			if (!Success)
			{
				char* Error = BuildString(1,"LaunchFeedbackApplication::~LaunchFeedbackApplication(), CreateProcessA() Failed! " SupportId(37));
				ReportError(Error);
				delete[] Error;
			}

#ifdef GAME_DLL
			delete[] GameDir;
#endif
			delete pi;
			delete si;
			return;
		}
	};

	// Launch the MaxsiFeedback.exe when the mod closes
	LaunchFeedbackApplication LaunchTheMaxsiFeedbackDotExeWhenTheDestructorCalls;

#endif
}