#ifdef ALTERED_TRANSMISSION

#include "ScreenSpaceEffects.h"
 
class CUnsharpEffect : public IScreenSpaceEffect
{
private:
        CTextureReference       m_UnsharpBlurFB;
        CMaterialReference      m_UnsharpBlur;
        CMaterialReference      m_Unsharp;

private:
	bool m_bEnabled;

public:
        CUnsharpEffect( void ) { };
 
        virtual void Init( void );
        virtual void Shutdown( void );
        virtual void SetParameters( KeyValues *params ) {};
 
        virtual void Render( int x, int y, int w, int h );

		virtual void Enable( bool bEnable ) { m_bEnabled = bEnable; }
		virtual bool IsEnabled( ) { return m_bEnabled; }
 
};

class CRecordingEffect : public IScreenSpaceEffect
{
private:
	int		iLastHealth;
	int		iTearFrames;
	float fNextTearTime;
	float fPrevTearMean;

private:
	CMaterialReference	m_ChromaticDisp;
	CMaterialReference	m_Noise;

private:
	bool m_bEnabled;

public:
	CRecordingEffect( void ) { };
 
	virtual void Init( void );
	virtual void Shutdown( void );
	virtual void SetParameters( KeyValues *params ) {};

	virtual void Render( int x, int y, int w, int h );

	virtual void Enable( bool bEnable ) { m_bEnabled = bEnable; }
	virtual bool IsEnabled( ) { return m_bEnabled; } 

private:
	void RecalculateTearTime();

};

ADD_SCREENSPACE_EFFECT( CRecordingEffect, at_recording );
ADD_SCREENSPACE_EFFECT( CUnsharpEffect, at_unsharp );

#endif