// Nate Nichols, InfoLab Northwestern University, August 2006.
#include "cbase.h"
#include "avi/ibik.h"
#include "materialsystem/IMaterialVar.h"
#include "materialsystem/ITexture.h"
// client

//This is the client side of the entity.  The corresponding server entity is C_Env_Bink.
class C_Env_Bink : public C_BaseEntity
{
public:
	DECLARE_CLIENTCLASS();
	DECLARE_CLASS( C_Env_Bink, C_BaseEntity );
	C_Env_Bink();
	~C_Env_Bink();

			void	OnDataChanged(DataUpdateType_t updateType);
	virtual	void	ClientThink( );
			bool	LoadVideo( void );
			bool	PlayVideo( void );

private:
	int m_iPlay;
	int m_iLastPlay;
	bool m_bLoop;
	int m_iAdvanceFrame;
	int m_iLastAdvanceFrame;
	float m_fBegun;
	char m_iszTextureName[256];	//These arrays may leak, but I don't think so.
	char m_iszMovieName[256];
	char m_iszLastMovieName[256];


	BIKMaterial_t	m_BIKHandle;
	IMaterial		*m_pMaterial;
	int				m_nPlaybackHeight;			// Calculated to address ratio changes
	int				m_nPlaybackWidth;
	char			m_szExitCommand[MAX_PATH];	// This call is fired at the engine when the video finishes or is interrupted

	float			m_flU;	// U,V ranges for video on its sheet
	float			m_flV;
};


IMPLEMENT_CLIENTCLASS_DT(C_Env_Bink, DT_Env_Bink, CEnv_Bink)
	RecvPropInt(RECVINFO(m_iPlay)),
	RecvPropInt(RECVINFO(m_bLoop)),
	RecvPropInt(RECVINFO(m_iAdvanceFrame)),
	RecvPropString(RECVINFO(m_iszTextureName)),
	RecvPropString(RECVINFO(m_iszMovieName)),
	RecvPropFloat(RECVINFO(m_fBegun)),
END_RECV_TABLE()

C_Env_Bink::C_Env_Bink()
{
	m_BIKHandle				=	BIKHANDLE_INVALID;
	m_iAdvanceFrame			=	0;
	m_iLastAdvanceFrame		=	0;
	m_iPlay					=	0;
	m_iLastPlay				=	0;
}
C_Env_Bink::~C_Env_Bink()
{
	// Shut down this video
	if ( m_BIKHandle != BIKHANDLE_INVALID )
	{
		bik->DestroyMaterial( m_BIKHandle );
		m_BIKHandle = BIKHANDLE_INVALID;
	}
}

void C_Env_Bink::ClientThink(  )
{
	if ( m_BIKHandle == BIKHANDLE_INVALID )
	{
		return;
	}

	float Time	=	gpGlobals->curtime-m_fBegun;
	if ( Time < 0.0f ) { Time = 0.0f; }

	// Just to make sure!
	// TODO: Is this what makes it LAG seriously? If so, don't uncomment!
	//bik->SetFrame(m_BIKHandle,Time);

	if ( bik->Update( m_BIKHandle ) == false )
	{
		
		// The video is done!

		SetNextClientThink( CLIENT_THINK_NEVER );
		Msg("Binkplayback is over!\n");

		// ...
	}
	else
	{
		SetNextClientThink( CLIENT_THINK_ALWAYS );
	}
}

bool OutputIMaterialVar(IMaterialVar* BIKVar)
{
	char*	MatTypeStr[9];
		MatTypeStr[0] = "MATERIAL_VAR_TYPE_FLOAT";
		MatTypeStr[1] = "MATERIAL_VAR_TYPE_STRING";
		MatTypeStr[2] = "MATERIAL_VAR_TYPE_VECTOR";
		MatTypeStr[3] = "MATERIAL_VAR_TYPE_TEXTURE";
		MatTypeStr[4] = "MATERIAL_VAR_TYPE_INT";
		MatTypeStr[5] = "MATERIAL_VAR_TYPE_FOURCC";
		MatTypeStr[6] = "MATERIAL_VAR_TYPE_UNDEFINED";
		MatTypeStr[7] = "MATERIAL_VAR_TYPE_MATRIX";
		MatTypeStr[8] = "MATERIAL_VAR_TYPE_MATERIAL";

	Msg("	%s	=	",BIKVar->GetName());
	if ( BIKVar->GetType() == MATERIAL_VAR_TYPE_FLOAT )
	{
		Msg("%f",BIKVar->GetFloatValue());
	}
	else if ( BIKVar->GetType() == MATERIAL_VAR_TYPE_STRING )
	{
		Msg("%s",BIKVar->GetStringValue());
	}
	else if ( BIKVar->GetType() == MATERIAL_VAR_TYPE_INT )
	{
		Msg("%d",BIKVar->GetIntValue());
	}
	else if ( BIKVar->GetType() == MATERIAL_VAR_TYPE_TEXTURE )
	{
		Msg("%s",BIKVar->GetTextureValue()->GetName());
	}
	else if ( BIKVar->GetType() == MATERIAL_VAR_TYPE_MATRIX )
	{
		// "$basetexturetransform" "center .25 .25 scale 1 1 rotate 0 translate 0 0"
		VMatrix M = BIKVar->GetMatrixValue();
		Msg("[%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ]",
			M.m[0][0],M.m[0][1],M.m[0][2],M.m[0][3],
			M.m[1][0],M.m[1][1],M.m[1][2],M.m[1][3],
			M.m[2][0],M.m[2][1],M.m[2][2],M.m[2][3],
			M.m[3][0],M.m[3][1],M.m[3][2],M.m[3][3]);

		// hmm.
	}
	else if ( BIKVar->GetType() == MATERIAL_VAR_TYPE_VECTOR )
	{
		Vector V;
		BIKVar->GetVecValue(V.Base(),3);
		Msg("[%f %f %f]",V.x,V.y,V.z);
	}
	else
	{
		Msg(MatTypeStr[BIKVar->GetType()]);
	}
	Msg("\n");
	return true;
}

bool	CopyMatVarFromMatToMat(const char* Var,IMaterial* Source, IMaterial* Dest)
{
	if (!Var||!Source||!Dest) { return false; }
	bool found;

	Dest->FindVar(Var, &found, false)->CopyFrom(Source->FindVar(Var, &found, false));
	return true;
}

bool C_Env_Bink::PlayVideo( void )
{	
	Msg(" PlayVideo ");
	bool		found			=	false;

	if ( m_BIKHandle == BIKHANDLE_INVALID )
	{
		LoadVideo();
	}

	// Should have been initialized by now!
	if ( m_BIKHandle == BIKHANDLE_INVALID )
	{		
		Msg(" BIKHANDLE_INVALID ");
		return false;
	}
	int nWidth, nHeight;
	bik->GetFrameSize( m_BIKHandle, &nWidth, &nHeight );
	bik->GetTexCoordRange( m_BIKHandle, &m_flU, &m_flV );
	m_pMaterial = bik->GetMaterial( m_BIKHandle );

	IMaterial*		BIK			=	m_pMaterial;

	Msg(" BIK Debug: Name: %s, Shader: %s, Proxy %d",
		BIK->GetName(),
		BIK->GetShaderName(),
		BIK->HasProxy());

	// Find the actual texture value!
	//IMaterialVar*	BIKVar		=	m_pMaterial->FindVar("$basetexture", &found, false);  // Get a reference to our base texture variable
	
	//if (!found) { Msg(" !BIKVar "); return false; }
	
	// Debug Output all the vars

	Msg("\nDebug Information for %s:\n",m_pMaterial->GetName());

	for (int N = 0; N < m_pMaterial->ShaderParamCount(); N++)
	{
		OutputIMaterialVar(m_pMaterial->GetShaderParams()[N]);
	}

	////ITexture*		BIKTex		=	BIKVar->GetTextureValue();  // Now grab a ref to the actual texture

	////if (!BIKTex) { Msg(" !BIKTex "); return false; }

	//// Now link to the material system!

	IMaterial*		Surface		=	g_pMaterialSystem->FindMaterial(m_iszTextureName,NULL);

	if (!Surface) { Msg(" !Surface "); return false; }

	Msg("Debug Information for %s:\n",Surface->GetName());

	for (int N = 0; N < Surface->ShaderParamCount(); N++)
	{
		OutputIMaterialVar(Surface->GetShaderParams()[N]);
	}

	////CopyMatVarFromMatToMat("$flags",BIK,Surface);
	////CopyMatVarFromMatToMat("$flags_defined",BIK,Surface);
	////CopyMatVarFromMatToMat("$flags2",BIK,Surface);
	//////CopyMatVarFromMatToMat("$flags_defined2",BIK,Surface);
	//CopyMatVarFromMatToMat("$color",BIK,Surface);
	//CopyMatVarFromMatToMat("$alpha",BIK,Surface);
	//CopyMatVarFromMatToMat("$basetexture",BIK,Surface);
	//CopyMatVarFromMatToMat("$frame",BIK,Surface);
	////CopyMatVarFromMatToMat("$basetexturetransform",BIK,Surface);
	////CopyMatVarFromMatToMat("$flashlighttexture",BIK,Surface);
	////CopyMatVarFromMatToMat("$flashlighttextureframe",BIK,Surface);
	//CopyMatVarFromMatToMat("$color2",BIK,Surface);
	//CopyMatVarFromMatToMat("$srgbtint",BIK,Surface);
	//CopyMatVarFromMatToMat("$ytexture",BIK,Surface);
	////CopyMatVarFromMatToMat("$crtexture",BIK,Surface);
	////CopyMatVarFromMatToMat("$cbtexture",BIK,Surface);
	//CopyMatVarFromMatToMat("$nolod",BIK,Surface);
	//CopyMatVarFromMatToMat("$nomip",BIK,Surface);

	Surface->FindVar("$basetexture", &found, false)->CopyFrom(BIK->FindVar("$ytexture", &found, false));
	
	Msg("Debug Information for %s after copy:\n",Surface->GetName());

	for (int N = 0; N < Surface->ShaderParamCount(); N++)
	{
		OutputIMaterialVar(Surface->GetShaderParams()[N]);
	}


	//IMaterialVar*	SurfaceVar	=	Surface->FindVar("$basetexture", &found, false);  // Get a reference to our base texture variable
	//
	//if (!found) { Msg(" !SurfaceVar "); return false; }

	// Replace the texture with the BIK Material.
	//SurfaceVar->SetTextureValue(BIKTex);
	//SurfaceVar->CopyFrom(BIKVar);

	Msg(" Success!!");

	return true;
}

bool C_Env_Bink::LoadVideo( void )
{
	Msg(" LoadVideo ");
	// Destroy any previously allocated video
	if ( m_BIKHandle != BIKHANDLE_INVALID )
	{
		bik->DestroyMaterial( m_BIKHandle );
		m_BIKHandle = BIKHANDLE_INVALID;
	}

	// Todo: Replace "VideoBIKMaterial" with something smarter!

	// Load and create our BINK video
	m_BIKHandle = bik->CreateMaterial( "VideoBIKMaterial", m_iszMovieName, "GAME" );
	if ( m_BIKHandle == BIKHANDLE_INVALID )
	{		
		Msg(" BIKHANDLE_INVALID ");
		return false;
	}
	Msg(" BIKHANDLE_VALID ");
	return true;
}

void C_Env_Bink::OnDataChanged( DataUpdateType_t updateType )
{
	Msg("C_Env_Bink::OnDataChanged(");
    BaseClass::OnDataChanged( updateType );
    if ( updateType == DATA_UPDATE_CREATED )
    {
		LoadVideo();
	}
	//m_pProxy->SetLooping(m_bLoop);
	if (m_iPlay > m_iLastPlay)
	{
		PlayVideo();
		SetNextClientThink( CLIENT_THINK_ALWAYS );
	}
	else if (m_iPlay < m_iLastPlay)
	{
		Msg("StopVideo();\n");
		if ( m_BIKHandle != BIKHANDLE_INVALID )
		{
			bik->DestroyMaterial( m_BIKHandle );
			m_BIKHandle = BIKHANDLE_INVALID;
		}
		//m_pProxy->Pause();
	}
	if (strcmp(m_iszMovieName, m_iszLastMovieName))
	{
		//m_pProxy->SetMovie(m_iszMovieName);
	}

	if (m_iAdvanceFrame != m_iLastAdvanceFrame)
	{
		//m_pProxy->AdvanceFrame();
	}

	strcpy(m_iszLastMovieName, m_iszMovieName);
	m_iLastAdvanceFrame = m_iAdvanceFrame;
	m_iLastPlay = m_iPlay;
	Msg(");\n");
}