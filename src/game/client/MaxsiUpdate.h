/****************************************************************************

	COPYRIGHT(C) WWW.MAXSI.DK, MAXSI SOFTWARE, SORTIE 2008-2009
	ALL RIGHTS RESERVED

	Client.dll
	A client-side interface for a mod on the Source Engine

	MaxsiUpdate.h
	Functions that ensure this product is up to date.

****************************************************************************/

#ifndef MaxsiUpdate_H
#define MaxsiUpdate_H

namespace MaxsiDistribution
{
	// Helper functions from MaxsiEngine.dll
	char*	BuildString				(unsigned int NumParameters, ...);
	bool	str_replace				(char* input, char* find, char* replace);
	char*	GetWorkingDir			(void);

	// Returns true if the product can be run, false if an update-process has begun.
	bool	UpdateProduct			(void);
	bool	ReportCrashes			(void);
	bool	ReportCrashesThreaded	(void);
	bool	ExecuteMDDLL			(char* Func, char* Parameter = NULL);
	bool	ReportError				(const char* Error, bool DontShowNotepad = false);
	bool	IngameError				(const char* Error);
	bool	IngameInfo				(const char* Error);
	bool	PingBack				(char* Function, char* Parameter);
}

#endif