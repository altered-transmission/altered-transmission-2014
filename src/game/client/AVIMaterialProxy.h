// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was taken from the SDK wiki page on procedural materials.
#ifndef AVI_MATERIAL_PROXY_H
#define AVI_MATERIAL_PROXY_H

#include "ProxyEntity.h"
#include "materialsystem/IMaterialVar.h"
#include "materialsystem/ITexture.h"
#include "bitmap/TGALoader.h"
#include "view.h"
#include "datacache/idatacache.h"
#include "materialsystem/IMaterial.h"
#include "materialsystem/IMaterialProxy.h"
#include "AVIMaterialRegen.h"

#define MAX_AVIS 10

class CAviMaterialProxy : public IMaterialProxy
{
public:
	CAviMaterialProxy();
	~CAviMaterialProxy();
	bool Init( IMaterial *pMaterial, KeyValues *pKeyValues );
	bool SetMovie(const char* filename);
	void OnBind(void *pC_BaseEntity );
	void Release();
	void Play() {m_pTextureRegen->Play();}
	void Pause() {m_pTextureRegen->Pause();}
	void Stop() {m_pTextureRegen->Stop();}
	void AdvanceFrame() {m_pTextureRegen->AdvanceFrame();}
	void SetLooping(bool bLoop) {m_pTextureRegen->SetLooping(bLoop);}
	static CAviMaterialProxy* LookupByTextureName(const char* textureName);
	static CAviMaterialProxy** MakeNullArray();
	virtual IMaterial *	GetMaterial();

private:
	IMaterialVar		*m_pTextureVar;   // The material variable
	ITexture			*m_pTexture;      // The texture
	CAviTextureRegen *m_pTextureRegen; // The regenerator
	static CAviMaterialProxy** g_pAviProxies;
	static int g_iNextProxy;

};

EXPOSE_INTERFACE( CAviMaterialProxy, IMaterialProxy, "AviRenderer" IMATERIAL_PROXY_INTERFACE_VERSION );

#endif //AVI_MATERIAL_PROXY_H