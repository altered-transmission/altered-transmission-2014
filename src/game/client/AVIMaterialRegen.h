// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was provided by Valve.  Thanks.

#ifndef AVI_MATERIAL_REGEN_H
#define AVI_MATERIAL_REGEN_H

#include "ProxyEntity.h"
#include "materialsystem/IMaterialVar.h"
#include "materialsystem/ITexture.h"
#include "bitmap/TGALoader.h"
#include "view.h"
#include "datacache/idatacache.h"
#include "materialsystem/IMaterial.h"
#include "materialsystem/IMaterialProxy.h"

#pragma warning( disable : 4201 )
#pragma warning( disable : 4005 )

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <vfw.h>

#pragma warning( default : 4201 )
#pragma warning( default : 4005 )


class CAviTextureRegen : public ITextureRegenerator
{
public:
	CAviTextureRegen();
	bool SetMovie(const char* filename);
	void RegenerateTextureBits( ITexture *pTexture, IVTFTexture *pVTFTexture, Rect_t *pSubRect );
	void Release();
	void CreateVideoStream( );
	void DestroyVideoStream();
	void SetTime( float flTime );
	void Play();
	void Pause();
	void Stop();
	void SetLooping(bool bLoop);
	void AdvanceFrame();
	void ResetPlayingTime() {m_fRunTime = 0.0f;}

private:
	bool m_bPlay;
	bool m_bLoop;
	bool m_bPaused;
	CMaterialReference m_Material;
	CTextureReference m_Texture;
	IAVIFile *m_pAVIFile;
	IAVIStream *m_pAVIStream;
	IGetFrame *m_pGetFrame;
	int m_nAVIWidth;
	int m_nAVIHeight;
	int m_nFrameRate;
	int m_nFrameCount;
	int m_nCurrentSample;
	int LastError;
	const char* m_pMovieFilename;
	HDC				m_memdc;
	HBITMAP			m_DIBSection;
	BITMAPINFO		m_bi;
	BITMAPINFOHEADER *m_bih;
	float m_fRunTime;
	float m_fLastRedraw;
}; 

#endif //AVI_MATERIAL_REGEN_H