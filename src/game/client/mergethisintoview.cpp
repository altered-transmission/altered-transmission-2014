#include "cbase.h"
#if 0
CViewRender::LevelInit:
	Move your ->EnableScreenSpaceEffects calls to here. (Out of clientmode_shared) Better location. be sure the enable calls are under InitScreenSpaceEffects( )
	g_pScreenSpaceEffects->EnableScreenSpaceEffect( "c17_healthfx" );
	g_pScreenSpaceEffects->EnableScreenSpaceEffect( "c17_waterfx" );
	g_pScreenSpaceEffects->EnableScreenSpaceEffect( "c17_unsharp" );
	g_pScreenSpaceEffects->EnableScreenSpaceEffect( "c17_sunshaft" );

	//City 17: Set up the effect call for DrawSunShaftsBlack
	pShaftFX = (CSunShaftEffect*)g_pScreenSpaceEffects->GetScreenSpaceEffect( "c17_sunshaft" );
	
//-----------------------------------------------------------------------------
// Purpose: Called once per level change
//-----------------------------------------------------------------------------
void CViewRender::LevelShutdown( void )
{
	//City 17: Disable our ScreenSpaceEffects,
	g_pScreenSpaceEffects->DisableScreenSpaceEffect( "c17_healthfx" );
	g_pScreenSpaceEffects->DisableScreenSpaceEffect( "c17_waterfx" );
	g_pScreenSpaceEffects->DisableScreenSpaceEffect( "c17_unsharp" );
	g_pScreenSpaceEffects->DisableScreenSpaceEffect( "c17_sunshaft" );

	//City 17: Remove our call for DrawSunShaftsBlack
	pShaftFX = NULL;

	// Shutdown all IScreenSpaceEffects
	//...
#endif