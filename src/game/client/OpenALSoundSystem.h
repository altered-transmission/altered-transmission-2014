// OpenALSoundSystem.h
#if defined(ALTERED_TRANSMISSION) && defined(OPENAL_SOUND)
#ifndef OpenALSoundSystem_H
#define OpenALSoundSystem_H

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

class IOpenALSound;
class COpenALSoundSystem;


class COpenALSoundSystem : public CAutoGameSystemPerFrame
{
public:
	typedef CAutoGameSystemPerFrame BaseClass;

private:
	bool m_IsPaused;

private:
	IOpenALSound* m_First;

public:
	Vector m_ListenerPosition;
	Vector m_ListenerForward;

public:
	COpenALSoundSystem();
	virtual ~COpenALSoundSystem();

public:
	// Init, shutdown
	// return true on success. false to abort DLL init!
	virtual bool Init();
	virtual void PostInit();
	virtual void Shutdown();

	// Level init, shutdown
	virtual void LevelInitPreEntity();
	virtual void LevelInitPostEntity();
	virtual void LevelShutdownPreEntity();
	virtual void LevelShutdownPostEntity();

	virtual void OnSave();
	virtual void OnRestore();
	virtual void SafeRemoveIfDesired();

	// Called before rendering
	virtual void PreRender();

	// Gets called each frame
	virtual void Update(float frametime);

	// Called after rendering
	virtual void PostRender();

public:
	virtual bool IsPaused() { return m_IsPaused; }

private:
	virtual void OnGamePaused();
	virtual void OnGameUnpaused();

public:
	virtual bool HandleSound(IOpenALSound* Sound);
	virtual void RemoveSound(IOpenALSound* Sound);

};

class IOpenALSound
{
	friend class COpenALSoundSystem;

protected:
	COpenALSoundSystem* m_SoundSystem;

private:
	IOpenALSound* m_NextSound;
	IOpenALSound* m_PrevSound;

public:
	virtual void OnGamePaused() = 0;
	virtual void OnGameUnpaused() = 0;
	virtual void OnGameFrame(float DeltaTime) = 0;

};

#define SOUNDSTREAM_NUM_BUFFERS 3

class COpenALSoundStream : public IOpenALSound, public ILibAVCodecAudioDest
{
public:
	typedef IOpenALSound BaseClass;

private:
	bool m_3DSound;
	Vector m_Location;
	CBaseEntity* m_Parent;
	int m_ParentAttachment;

private:
	bool m_bPauseOnGamePause;

private:
	ALuint m_Source;

private:
	ALuint* m_Buffers;
	ALsizei m_BufferNum;
	ALsizei m_BufferSize;
	ALsizei m_BuffersQueued;
	ALenum m_Format;
	int m_Rate;
	int m_Channels;
	int m_Bits;
	int m_BaseTime;

private:
	ALsizei m_BuffersInitialized;

public:
	COpenALSoundStream();
	virtual ~COpenALSoundStream();

public:
	virtual void SetLocation(Vector &Vec);
	virtual void SetLocation(CBaseEntity* Parent);
	virtual void SetLocation(CBaseEntity* Parent, int ParentAttachment);

public:
	virtual void OnGamePaused();
	virtual void OnGameUnpaused();
	virtual void OnGameFrame(float DeltaTime);

private:
	void virtual UpdateSound();

public:
	virtual bool AllocateBuffers(ALuint Num = 5, ALsizei BufferSize = 19200); 

public:
	virtual size_t AvailableAudioBufferSpace();
	virtual void ClearAudioBuffers();
	virtual bool OnAudioInformation(int Rate, int Channels, int Bits);
	virtual bool OnAudioData(void* Data, size_t DataSize);
	virtual void OnAudioStreamEnd();

};

extern COpenALSoundSystem* g_pOpenALSoundSystem;

#endif
#endif
