// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was provided by Valve.  Thanks.

#include "cbase.h"

#ifdef ENABLE_FFMPEG_IN_SOURCE

extern "C"
{

#include <avcodec.h>
#include <avformat.h>
#include <swscale.h>
#include <opt.h>

}

#include "vtf/vtf.h"
#include "env_ffmpeg.h"
#include "ffmpegMaterialRegen.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

void CffmpegTextureRegen::Release()
{

}

CffmpegTextureRegen::CffmpegTextureRegen()
{
	m_VideoEntity	=	NULL;

	//m_pAVIFile = NULL;
	//m_pAVIStream = NULL;
	//m_pGetFrame = NULL;
	//m_bih = NULL;
	//m_memdc = NULL;
	//m_DIBSection = NULL;
	//m_bPlay = false;
	//m_bPaused = false;
	//m_fLastRedraw = -1.0f;
	//m_bLoop = true;
}

/*void CffmpegTextureRegen::SetTime( float flTime )
{
	if ( m_pAVIStream )
	{
		m_nCurrentSample = (int)( flTime * m_nFrameRate + 0.5f );
		if (m_nCurrentSample >= m_nFrameCount)
		{
			m_fRunTime = 0.0f;
			m_nCurrentSample = m_nFrameCount - 1;
			if (!m_bLoop)
			{
				Stop();
			}
		}
	}
}

void CffmpegTextureRegen::Play()
{
	if (m_fLastRedraw == -1.0f)
	{
		m_fLastRedraw = gpGlobals->curtime;
	}
	m_bPlay = true;
	m_bPaused = false;
}

void CffmpegTextureRegen::Pause()
{
	m_bPaused = true;
}

void CffmpegTextureRegen::Stop()
{
	m_bPaused = false;
	m_bPlay = false;
	m_fLastRedraw = -1.0f;
}


void CffmpegTextureRegen::SetLooping(bool bLoop)
{
	m_bLoop = bLoop;
}*/

void CffmpegTextureRegen::RegenerateTextureBits( ITexture *pTexture, IVTFTexture *pVTFTexture, Rect_t *pRect )
{
	if ( !m_VideoEntity ) { return; }
	
	if ( pVTFTexture->Format() != IMAGE_FORMAT_RGBA8888 )
	{
		// Todo: does this succeed?
		//pVTFTexture->ConvertImageFormat(IMAGE_FORMAT_RGBA8888,false);
	}

	m_VideoEntity->Render((int*)pVTFTexture->ImageData(0, 0, 0),pVTFTexture->Width(),pVTFTexture->Height());

	return;
}

#endif
