#include "cbase.h"
#if 0
ConVar r_post_sunshaft_density( "r_post_sunshaft_density", "1.1", FCVAR_CHEAT );
ConVar r_post_sunshaft_decay( "r_post_sunshaft_decay", "0.91", FCVAR_CHEAT );
ConVar r_post_sunshaft_weight( "r_post_sunshaft_weight", "0.7", FCVAR_CHEAT );
ConVar r_post_sunshaft_exposure( "r_post_sunshaft_exposure", "0.15", FCVAR_CHEAT );
ConVar r_post_sunshaft_blur( "r_post_sunshaft_blur", "1", FCVAR_ARCHIVE );
ConVar r_post_sunshaft_blur_amount( "r_post_sunshaft_blur_amount", "1", FCVAR_CHEAT );
void CSunShaftEffect::Init( void )
{
	bMapHasSun = false;
	bSunIsBehind = false;
	fAlpha = 1.0f;
	fAlphaLerpTo = 1.0f;

	fDensity = r_post_sunshaft_density.GetFloat();
	fDecay = r_post_sunshaft_decay.GetFloat();
	fWeight = r_post_sunshaft_weight.GetFloat();
	fExposure = r_post_sunshaft_exposure.GetFloat();
	fBlur = r_post_sunshaft_blur_amount.GetFloat();
	fBlurAddition = 0.0f;
	fBlurAdditionLerpTo = 0.0f;

	m_SunShaftBlend.InitRenderTarget( 1024, 1024, RT_SIZE_HDR, IMAGE_FORMAT_RGBA8888, MATERIAL_RT_DEPTH_SEPARATE, false, "_rt_SunShafts" );
	m_SunShaftBlend_Blur.InitRenderTarget( 1024, 1024, RT_SIZE_HDR, IMAGE_FORMAT_RGBA8888, MATERIAL_RT_DEPTH_SEPARATE, false, "_rt_SunShafts_Blur" );

	m_SunShaft.Init( materials->FindMaterial("effects/shaders/sunshaft_prepass", TEXTURE_GROUP_PIXEL_SHADERS, true) );
	m_SunDebug.Init( materials->FindMaterial("effects/shaders/sun_debug", TEXTURE_GROUP_OTHER, true ) );

	m_SunShaft_BlurX.Init( materials->FindMaterial("effects/shaders/sunshaft_blurx", TEXTURE_GROUP_PIXEL_SHADERS, true) );
	m_SunShaft_BlurY.Init( materials->FindMaterial("effects/shaders/sunshaft_blury", TEXTURE_GROUP_PIXEL_SHADERS, true) );

	m_SunShaftBlendMat.Init( materials->FindMaterial("effects/shaders/sunshaft_final", TEXTURE_GROUP_CLIENT_EFFECTS, true) );
}

void CSunShaftEffect::Shutdown( void )
{
	m_SunShaftBlend.Shutdown();
	m_SunShaftBlend_Blur.Shutdown();

	m_SunShaft.Shutdown();
	m_SunDebug.Shutdown();
	m_SunShaft_BlurX.Shutdown();
	m_SunShaft_BlurY.Shutdown();

	m_SunShaftBlendMat.Shutdown();
}

ConVar r_post_sunshaft_debug( "r_post_sunshaft_debug", "0", FCVAR_CHEAT, "0 = off, 1 = debug RT, 2 = debug shader, 3 = debug values only." );
//------------------------------------------------------------------------------
// Purpose: Pick up changes in our parameters
//------------------------------------------------------------------------------
void CSunShaftEffect::SetParameters( KeyValues *params )
{
	if( params->FindKey( "sunposx" ) )
	{
		sunPos.x = params->GetFloat( "sunposx" );
	}
	if( params->FindKey( "sunposy" ) )
	{
		sunPos.y = params->GetFloat( "sunposy" );
	}
	if( params->FindKey( "sunbehind" ) )
	{
		bSunIsBehind = ( params->GetInt( "sunbehind" ) == 1 );
	}
	if( params->FindKey( "density" ) )
	{
		fDensity = params->GetFloat( "density" );
	}
	if( params->FindKey( "decay" ) )
	{
		fDecay = params->GetFloat( "decay" );
	}
	if( params->FindKey( "weight" ) )
	{
		fWeight = params->GetFloat( "weight" );
	}
	if( params->FindKey( "exposure" ) )
	{
		fExposure = params->GetFloat( "exposure" );
	}
	if( params->FindKey( "blur" ) )
	{
		fBlur = params->GetFloat( "blur" );
	}
	if( params->FindKey( "map_has_sun" ) )
	{
		bMapHasSun = ( params->GetInt( "map_has_sun" ) == 1 );
	}
}

ConVar r_post_sunshaft( "r_post_sunshaft", "1", FCVAR_ARCHIVE );
ConVar r_post_sunshaft_highquality( "r_post_sunshaft_highquality", "1", FCVAR_ARCHIVE );
ConVar r_post_sunshaft_useconvars( "r_post_sunshaft_useconvars", "0", FCVAR_CHEAT, "If true, use the values of the r_post_sunshaft convars instead of the mapper chosen amounts." );
ConVar r_post_sunshaft_startfade( "r_post_sunshaft_startfade", "2.2", FCVAR_CHEAT );
ConVar r_post_sunshaft_bluraddition( "r_post_sunshaft_bluraddition", "3.0", FCVAR_CHEAT );
ConVar r_post_sunshaft_blurcontrol_x( "r_post_sunshaft_blurcontrol_x", "1.9", FCVAR_CHEAT );
ConVar r_post_sunshaft_blurcontrol_y( "r_post_sunshaft_blurcontrol_y", "1.9", FCVAR_CHEAT );

bool CSunShaftEffect::ShaftsRendering( void )
{
	return ( r_post_sunshaft.GetBool() && engine->IsSkyboxVisibleFromPoint( CurrentViewOrigin() ) && bMapHasSun && IsEnabled() && (fAlpha > 0.01) );
}

void CSunShaftEffect::Render( int x, int y, int w, int h )
{
	VPROF( "CSunShaftEffect::Render" );

	if( !r_post_sunshaft.GetBool() || !engine->IsSkyboxVisibleFromPoint( CurrentViewOrigin() ) || !bMapHasSun || ( IsEnabled() == false ) )
		return;

	// Grab the render context
	CMatRenderContextPtr pRenderContext( materials );

	if( r_post_sunshaft_debug.GetInt() == 1 )
	{
		pRenderContext->DrawScreenSpaceQuad( m_SunDebug );
		return;
	}

	//Set up the sunshaft shader's paramaters.
	IMaterialVar *var;
	var = m_SunShaft->FindVar( "$lightvector", NULL );
	var->SetVecValue( sunPos.x, sunPos.y );
	if( !r_post_sunshaft_useconvars.GetBool() )
	{
		var = m_SunShaft->FindVar( "$density", NULL );
		var->SetFloatValue( fDensity );
		var = m_SunShaft->FindVar( "$decay", NULL );
		var->SetFloatValue( fDecay );
		var = m_SunShaft->FindVar( "$weight", NULL );
		var->SetFloatValue( fWeight );
		var = m_SunShaft->FindVar( "$exposure", NULL );
		var->SetFloatValue( fExposure );
		if( r_post_sunshaft_blur.GetBool() )
		{
			if( sunPos.x > r_post_sunshaft_blurcontrol_x.GetFloat() || sunPos.x < (-r_post_sunshaft_blurcontrol_x.GetFloat() + 0.5) || sunPos.y > r_post_sunshaft_blurcontrol_y.GetFloat() || sunPos.y < (-r_post_sunshaft_blurcontrol_y.GetFloat() + 0.5) )
			{
				fBlurAdditionLerpTo = r_post_sunshaft_bluraddition.GetFloat();
			}
			else
			{
				fBlurAdditionLerpTo = 0.0f;
			}

			if( fBlurAddition != fBlurAdditionLerpTo )
				fBlurAddition = FLerp( fBlurAddition, fBlurAdditionLerpTo, 0.2f );

			var = m_SunShaft_BlurX->FindVar( "$blursize", NULL );
			var->SetIntValue( fBlur + fBlurAddition );
			var = m_SunShaft_BlurY->FindVar( "$blursize", NULL );
			var->SetIntValue( fBlur + fBlurAddition );
		}
	}
	else
	{
		var = m_SunShaft->FindVar( "$density", NULL );
		var->SetFloatValue( r_post_sunshaft_density.GetFloat() );
		var = m_SunShaft->FindVar( "$decay", NULL );
		var->SetFloatValue( r_post_sunshaft_decay.GetFloat() );
		var = m_SunShaft->FindVar( "$weight", NULL );
		var->SetFloatValue( r_post_sunshaft_weight.GetFloat() );
		var = m_SunShaft->FindVar( "$exposure", NULL );
		var->SetFloatValue( r_post_sunshaft_exposure.GetFloat() );
		if( r_post_sunshaft_blur.GetBool() )
		{
			var = m_SunShaft_BlurX->FindVar( "$blursize", NULL );
			var->SetIntValue( r_post_sunshaft_blur_amount.GetFloat() );
			var = m_SunShaft_BlurY->FindVar( "$blursize", NULL );
			var->SetIntValue( r_post_sunshaft_blur_amount.GetFloat() );
		}
	}
	var = m_SunShaft->FindVar( "$highquality", NULL );
	var->SetIntValue( r_post_sunshaft_highquality.GetInt() );

	if( r_post_sunshaft_debug.GetInt() == 2 )
	{
		pRenderContext->DrawScreenSpaceQuad( m_SunShaft );
		return;
	}

	//At this point, our sun origin begins to break down. Lerp the alpha of our screeneffect down to 0 so it disappears smoothly.
	if( bSunIsBehind && sunPos.x < r_post_sunshaft_startfade.GetFloat() && sunPos.x > (-r_post_sunshaft_startfade.GetFloat() + 0.5) )
	{
		fAlphaLerpTo = 0.0f;
	}
	else if( sunPos.x > r_post_sunshaft_startfade.GetFloat() || sunPos.x < (-r_post_sunshaft_startfade.GetFloat() + 0.5) )
	{
		fAlphaLerpTo = 0.0f;
	}
	else
	{
		fAlphaLerpTo = 1.0f;
	}

	if( fAlpha != fAlphaLerpTo )
		fAlpha = FLerp( fAlpha, fAlphaLerpTo, 0.2f );

	//This happens for any setting of r_post_sunshaft_debug.
	if( r_post_sunshaft_debug.GetBool() )
	{
		DevMsg( "Sun X: %.2f | Sun Y: %.2f\n", sunPos.x, sunPos.y );
		DevMsg( "Blur Amount: %.2f | Blur Addition: %.2f | Blur Total: %.2f\n", fBlur, fBlurAddition, fBlur + fBlurAddition );
		DevMsg( "Alpha: %.2f\n", fAlpha );
		DevMsg( "Behind: %i\n", bSunIsBehind );
	}

	 //Don't draw to the screen if our alpha isn't great enough.
	if( fAlpha <= 0.0f )
		return;

	var = m_SunShaftBlendMat->FindVar( "$alpha", NULL );
	var->SetFloatValue( fAlpha );

	// Set to the proper rendering mode.
	pRenderContext->MatrixMode( MATERIAL_VIEW );
	pRenderContext->PushMatrix();
	pRenderContext->LoadIdentity();
	pRenderContext->MatrixMode( MATERIAL_PROJECTION );
	pRenderContext->PushMatrix();
	pRenderContext->LoadIdentity();

	//First, place the shafts into an RT so we can pass it off to UnlitGeneric with the shafts intact!
	pRenderContext->PushRenderTargetAndViewport( m_SunShaftBlend );
		pRenderContext->DrawScreenSpaceQuad( m_SunShaft );
	pRenderContext->PopRenderTargetAndViewport();

	//Render the gaussian blur pass over our shafts.
	if( r_post_sunshaft_blur.GetBool() )
	{
		pRenderContext->PushRenderTargetAndViewport( m_SunShaftBlend_Blur );
			pRenderContext->DrawScreenSpaceQuad( m_SunShaft_BlurX );
		pRenderContext->PopRenderTargetAndViewport();

		pRenderContext->PushRenderTargetAndViewport( m_SunShaftBlend );
			pRenderContext->DrawScreenSpaceQuad( m_SunShaft_BlurY );
		pRenderContext->PopRenderTargetAndViewport();
	}

	//Restore our state
	pRenderContext->MatrixMode( MATERIAL_VIEW );
	pRenderContext->PopMatrix();
	pRenderContext->MatrixMode( MATERIAL_PROJECTION );
	pRenderContext->PopMatrix();

	//Render our sun to the screen additively.
	pRenderContext->DrawScreenSpaceQuad( m_SunShaftBlendMat );
}
#endif