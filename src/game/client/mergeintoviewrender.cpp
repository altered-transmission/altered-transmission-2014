#include "cbase.h"
#if 0
CViewRender::RenderView
//BELOW DRAWMONITORS
		if(g_pMaterialSystemHardwareConfig->SupportsShaderModel_3_0() )
		{
			DrawSunShaftBlack( view );	
		}
//END
		
		
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CViewRender::DrawSunShaftBlack( const CViewSetup &viewSet )
{
	VPROF( "CViewRender::DrawSunShaftBlack" );

	if( pShaftFX == NULL || !pShaftFX->ShaftsRendering() )
		return;

	//Needed to change fog values.
	C_BasePlayer *localPlayer = C_BasePlayer::GetLocalPlayer();
	if(!localPlayer)
		return;

	//Copy our current View.
	CViewSetup sunshaftView = viewSet;

	//Get our camera render target.
	ITexture *pRenderTarget = GetSunShaftBlackTexture();

	if( pRenderTarget == NULL )
		return;

	if( !pRenderTarget->IsRenderTarget() )
		DevMsg("DrawSunShaftBlack: Not a render target.\n");

	// Save our old fog data.
	fogparams_t oldFogParams;
	fogparams_t *pFogParams = localPlayer->GetFogParams();
	oldFogParams = *pFogParams;

	// Set up our lovely black fog.
	pFogParams->enable = true;
	pFogParams->blend = false;
	pFogParams->start = 0.0f;
	pFogParams->end = 0.0f;
	pFogParams->maxdensity = 1.0f;
	pFogParams->farz = 4096;
	pFogParams->colorPrimary.SetR( 0 );
	pFogParams->colorPrimary.SetG( 0 );
	pFogParams->colorPrimary.SetB( 0 );
	pFogParams->colorSecondary.SetR( 0 );
	pFogParams->colorSecondary.SetG( 0 );
	pFogParams->colorSecondary.SetB( 0 );

	// Set up view information, so we get the same render settings as the main screen.
	sunshaftView.width = pRenderTarget->GetActualWidth();
	sunshaftView.height = pRenderTarget->GetActualHeight();
	sunshaftView.x = 0;
	sunshaftView.y = 0;
	sunshaftView.zFar = 4096;
	sunshaftView.m_bOrtho = false;
	sunshaftView.m_flAspectRatio = engine->GetScreenAspectRatio();
	sunshaftView.m_bDoBloomAndToneMapping = false;

	modelrender->SuppressEngineLighting( true );

	// Render the world and 3D Skybox.
	render->Push3DView( sunshaftView, VIEW_CLEAR_DEPTH | VIEW_CLEAR_COLOR, pRenderTarget, GetFrustum() );

	SkyboxVisibility_t nSkyboxVisible = SKYBOX_NOT_VISIBLE;
	int ClearFlags = 0;
	CSkyboxView *pSkyView = new CSkyboxView( this );
	if( pSkyView->Setup( sunshaftView, &ClearFlags, &nSkyboxVisible ) != false )
		AddViewToScene( pSkyView );
	SafeRelease( pSkyView );

	ViewDrawScene( false, SKYBOX_3DSKYBOX_VISIBLE, sunshaftView, VIEW_CLEAR_DEPTH, VIEW_SHADOW_DEPTH_TEXTURE );
	
	// Now actually draw the viewmodel
	modelrender->ForcedMaterialOverride( materials->FindMaterial( "tools/toolsblack", TEXTURE_GROUP_CLIENT_EFFECTS ) );
	DrawViewModels( sunshaftView, true );
	modelrender->ForcedMaterialOverride(0);

	modelrender->SuppressEngineLighting( false );
	render->PopView( m_Frustum );

	// Reset the fog back to it's original values.
	*pFogParams = oldFogParams;
}

//ADD TO HEADER AFTER DRAWMONITORS
CSunShaftEffect *pShaftFX;
void			DrawSunShaftBlack( const CViewSetup &cameraView );

//ALSO ADD INCLUDES NESSESCARY TO VIEW AND VIEWRENDER (YOUR SCREENSPACEEFFECT FILES)
#endif
