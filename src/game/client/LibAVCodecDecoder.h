// LibAVCodecDecoder.h
#ifdef ALTERED_TRANSMISSION
#ifndef LibAVCodecDecoder_H
#define LibAVCodecDecoder_H

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavcodec/opt.h>
}

unsigned int LibAVCodecDecoderThread(void* pParam);

class CLibAVCodecSystem;
class CLibAVCodecThread;
class ILibAVCodecAudioDest;
class ILibAVCodecVideoDest;
class CLibAVCodecVideoFrame;
class ILibAVCodecPlayer;
class CLibAVCodecPlayer;

class CLibAVCodecThread
{
	friend class CLibAVCodecSystem;

public:
	CLibAVCodecThread();
	~CLibAVCodecThread();

private:
	bool m_ShouldQuit;
	bool m_SignalGamePaused;
	bool m_SignalGameUnpaused;

private:
	CThreadEvent m_MessageEvent;
	CThreadMutex m_MessageLock;
	ILibAVCodecPlayer* m_MessagedPlayer;
	ILibAVCodecPlayer* m_MessagedPlayerLast;
	ILibAVCodecPlayer* m_First;

public:
	unsigned int Run();

};

class CLibAVCodecSystem : public CAutoGameSystemPerFrame
{
public:
	typedef CAutoGameSystemPerFrame BaseClass;

private:
	ThreadHandle_t m_ThreadHandle;

private:
	CLibAVCodecThread m_ThreadData;

private:
	bool m_IsPaused;

public:
	CLibAVCodecSystem();
	virtual ~CLibAVCodecSystem();

public:
	// Init, shutdown
	// return true on success. false to abort DLL init!
	virtual bool Init();
	virtual void Shutdown();

	// Gets called each frame
	virtual void Update(float frametime);

public:
	bool AddPlayer(ILibAVCodecPlayer* New);

public:
	virtual bool IsPaused() { return m_IsPaused; }

private:
	virtual void OnGamePaused();
	virtual void OnGameUnpaused();

};

class ILibAVCodecAudioDest
{
public:
	virtual size_t AvailableAudioBufferSpace() = 0;
	virtual void ClearAudioBuffers() = 0;
	virtual bool OnAudioInformation(int Rate, int Channels, int Bits) = 0;
	virtual bool OnAudioData(void* Data, size_t DataSize) = 0;
	virtual void OnAudioStreamEnd() = 0;

};

class ILibAVCodecVideoDest
{
public:
	virtual CLibAVCodecVideoFrame* AvailableVideoFrame();
	virtual void ClearVideoBuffers() = 0;
	virtual bool OnVideoInformation(int Width, int Height) = 0;
	virtual void OnVideoData(CLibAVCodecVideoFrame* Frame) = 0;
	virtual void OnVideoStreamEnd() = 0;

};

class CLibAVCodecVideoFrame
{
public:
	CLibAVCodecVideoFrame(int Width, int Height);
	~CLibAVCodecVideoFrame();

private:
	uint8_t* Img;
	int ImgWidth;
	int ImgHeight;

public:
	uint8_t* GetImg() { return Img; }
	int GetWidth() { return ImgWidth; }
	int GetHeight() { return ImgHeight; }

};

// NOTE: Player as in Media Player, not Game Player.
class ILibAVCodecPlayer
{
	friend class CLibAVCodecSystem;
	friend class CLibAVCodecThread;

// Owned by main thread, ownership transferred to decoder thread when player is sent to decoder thread.
private:
	ILibAVCodecPlayer* Prev;
	ILibAVCodecPlayer* Next;

// Owned by decoder thread.
public:
	virtual int DoIteration() = 0;
	virtual void OnGamePaused() = 0;
	virtual void OnGameUnpaused() = 0;

};

class CLibAVCodecPlayer : public ILibAVCodecPlayer
{
// Only if thread owns m_Lock.
private:
	const char* m_NewFilePath;

// Owned by decoder thread.
private:
	const char* m_FilePath;

// Owned by decoder thread.
private:
	AVFormatContext* m_FormatContext;
	AVCodecContext* m_AudioCodecContext;
	AVCodecContext* m_VideoCodecContext;
	int m_AudioStreamId;
	int m_VideoStreamId;
	AVPacket m_Packet;
	bool m_HasPacket;
	uint8_t* m_AudioPacketData;
	size_t m_AudioPacketDataUsed;
	size_t m_AudioPacketDataSize;
	int16_t* m_AudioReady;
	size_t m_AudioReadyUsed;
	size_t m_AudioReadySize;

// Owned by main thread, ownership transferred to decoder thread when player is sent to decoder thread.
private:
	ILibAVCodecAudioDest* m_AudioDest;
	ILibAVCodecVideoDest* m_VideoDest;

private:
	CThreadMutex m_Lock;
	
public:
	CLibAVCodecPlayer();
	~CLibAVCodecPlayer();

// Owned by main thread.
public:
	virtual bool LoadFile(const char* FilePath);
	virtual void Seek(float Position);

// May only be called before the player is sent to decoder thread.
public:
	virtual void SetAudioDest(ILibAVCodecAudioDest* Dest);
	virtual void SetVideoDest(ILibAVCodecVideoDest* Dest);

// Owned by decoder thread.
public:
	virtual int DoIteration();
	virtual void OnGamePaused();
	virtual void OnGameUnpaused();

// Owned by decoder thread.
private:
	virtual bool DoLoadFile();
	virtual void DoCloseFile();
	virtual bool DoFindStreams();
	virtual bool DoFindCodecs();
	virtual int DoDecoding();
	virtual int DoAudioDecoding();
	virtual int DoVideoDecoding();

};

extern CLibAVCodecSystem* g_pLibAVCodecSystem;

#endif
#endif
