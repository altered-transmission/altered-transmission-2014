#include "cbase.h"
#include "materialsystem/IMaterialVar.h"
#include "materialsystem/ITexture.h"

#ifdef ENABLE_FFMPEG_IN_SOURCE

#define ENABLE_FFMPEG_EXPERIMENTAL_SEEKING

extern "C"
{
	#include <avcodec.h>
	#include <avformat.h>
	#include <swscale.h>
	#include <opt.h>
}

#include "env_ffmpeg.h"
#include "ffmpeg_proxy.h"
#include "ffmpegMaterialRegen.h"

// client

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

static int sws_flags = SWS_BICUBIC;

ConVar ffmpeg_frame2surface_method(
	"ffmpeg_frame2surface_method", "0", 0, "Controls how the ffmpeg class converts its in-memory buffer to an ingame surface. 0 = Default, other values are experimental.");

class	RegisterFFMpeg
{
public:
	RegisterFFMpeg()
	{
		// Register all formats and codecs
		av_register_all();
	}    
};

RegisterFFMpeg AutoRegisterFFMpeg;

IMPLEMENT_CLIENTCLASS_DT(C_Env_ffmpeg, DT_Env_ffmpeg, CEnv_ffmpeg)
	RecvPropInt(RECVINFO(m_iPlay)),
	RecvPropInt(RECVINFO(m_bLoop)),
	RecvPropInt(RECVINFO(m_iAdvanceFrame)),
	RecvPropString(RECVINFO(m_iszTextureName)),
	RecvPropString(RECVINFO(m_iszMovieName)),
	RecvPropFloat(RECVINFO(m_fBegun)),
	RecvPropFloat(RECVINFO(m_fFrameRateHack)),
END_RECV_TABLE()

C_Env_ffmpeg::C_Env_ffmpeg()
{
	m_iPlay					=	0;
	m_iLastPlay				=	0;	
	ShouldPlayback			=	false;
	CurrentFrame			=	0;
	sws_opts				=	0;
	img_convert_ctx			=	0;
	m_fFrameRateHack		=	0.0;
	pFrameSize				=	0;
	pFrameRGBBiffer			=	NULL;
	for ( size_t I = 0; I < FFMPEG_FRAMES_IN_ADVANCE; I++ )
	{
		pFrameRGBBackBuffer[I]	=	NULL;
	}

	sws_opts = sws_getContext(16,16,PIX_FMT_YUV420P, 16,16,PIX_FMT_YUV420P, sws_flags, NULL,NULL,NULL);

	//sws_opts = sws_getContext(16,16,0, 16,16,0, sws_flags, NULL,NULL,NULL);
}
C_Env_ffmpeg::~C_Env_ffmpeg()
{
    av_free(sws_opts);

	if ( pFrameRGBBiffer ) { delete[] pFrameRGBBiffer; }
	for ( size_t I = 0; I < FFMPEG_FRAMES_IN_ADVANCE; I++ )
	{
		if ( pFrameRGBBackBuffer[I] ) { delete[] pFrameRGBBackBuffer[I]; }
	}
}

bool C_Env_ffmpeg::SyncNextFrame()
{
	//2 1 3 4
	//\ / | |
	// X  | |
 //   / \ | |
	//A B C D
	//Y Y Y N

	// Lock the frame state.

		// Ask if frame A is ready.

		// If frame A is not ready
			// Ask async thread to work on frame A
			// and continue working on B, C, and D when done.

	// Unlock the frame state.

	// If frame A was not ready
		// Listen to async thread to signal frame A is done.

	// Lock the frame state.

		// Set internal backbuffer's point to what A points to.
		// Set A to point to what B points to.
		// Set B to point to what C points to.
		// Set C to point to what D points to.
		// Set D to point to our old internal backbuffer.

		// Set D is not ready.

	// Unlock the frame state.

	// Display the internal backbuffer on the screen!

	return true;
}

void C_Env_ffmpeg::ClientThink()
{
	//if ( m_BIKHandle == BIKHANDLE_INVALID )
	//{
	//	return;
	//}

	//float Time	=	gpGlobals->curtime-m_fBegun;
	//if ( Time < 0.0f ) { Time = 0.0f; }

	//// Just to make sure!
	//// TODO: Is this what makes it LAG seriously? If so, don't uncomment!
	////bik->SetFrame(m_BIKHandle,Time);

	//if ( bik->Update( m_BIKHandle ) == false )
	//{
	//	
	//	// The video is done!

	//	SetNextClientThink( CLIENT_THINK_NEVER );
	//	Msg("ffmpegplayback is over!\n");

	//	// ...
	//}
	//else
	//{
	//	SetNextClientThink( CLIENT_THINK_ALWAYS );
	//}
}

//bool	CopyMatVarFromMatToMat(const char* Var,IMaterial* Source, IMaterial* Dest)
//{
//	if (!Var||!Source||!Dest) { return false; }
//	bool found;
//
//	Dest->FindVar(Var, &found, false)->CopyFrom(Source->FindVar(Var, &found, false));
//	return true;
//}

//bool C_Env_ffmpeg::PlayVideo( void )
//{	
//	Msg(" PlayVideo ");
//	bool		found			=	false;
//
//	if ( m_BIKHandle == BIKHANDLE_INVALID )
//	{
//		LoadVideo();
//	}
//
//	// Should have been initialized by now!
//	if ( m_BIKHandle == BIKHANDLE_INVALID )
//	{		
//		Msg(" BIKHANDLE_INVALID ");
//		return false;
//	}
//	int nWidth, nHeight;
//	bik->GetFrameSize( m_BIKHandle, &nWidth, &nHeight );
//	bik->GetTexCoordRange( m_BIKHandle, &m_flU, &m_flV );
//	m_pMaterial = bik->GetMaterial( m_BIKHandle );
//
//	IMaterial*		BIK			=	m_pMaterial;
//
//	Msg(" BIK Debug: Name: %s, Shader: %s, Proxy %d",
//		BIK->GetName(),
//		BIK->GetShaderName(),
//		BIK->HasProxy());
//
//	// Find the actual texture value!
//	//IMaterialVar*	BIKVar		=	m_pMaterial->FindVar("$basetexture", &found, false);  // Get a reference to our base texture variable
//	
//	//if (!found) { Msg(" !BIKVar "); return false; }
//	
//	// Debug Output all the vars
//
//	Msg("\nDebug Information for %s:\n",m_pMaterial->GetName());
//
//	for (int N = 0; N < m_pMaterial->ShaderParamCount(); N++)
//	{
//		OutputIMaterialVar(m_pMaterial->GetShaderParams()[N]);
//	}
//
//	////ITexture*		BIKTex		=	BIKVar->GetTextureValue();  // Now grab a ref to the actual texture
//
//	////if (!BIKTex) { Msg(" !BIKTex "); return false; }
//
//	//// Now link to the material system!
//
//	IMaterial*		Surface		=	g_pMaterialSystem->FindMaterial(m_iszTextureName,NULL);
//
//	if (!Surface) { Msg(" !Surface "); return false; }
//
//	Msg("Debug Information for %s:\n",Surface->GetName());
//
//	for (int N = 0; N < Surface->ShaderParamCount(); N++)
//	{
//		OutputIMaterialVar(Surface->GetShaderParams()[N]);
//	}
//
//	////CopyMatVarFromMatToMat("$flags",BIK,Surface);
//	////CopyMatVarFromMatToMat("$flags_defined",BIK,Surface);
//	////CopyMatVarFromMatToMat("$flags2",BIK,Surface);
//	//////CopyMatVarFromMatToMat("$flags_defined2",BIK,Surface);
//	//CopyMatVarFromMatToMat("$color",BIK,Surface);
//	//CopyMatVarFromMatToMat("$alpha",BIK,Surface);
//	//CopyMatVarFromMatToMat("$basetexture",BIK,Surface);
//	//CopyMatVarFromMatToMat("$frame",BIK,Surface);
//	////CopyMatVarFromMatToMat("$basetexturetransform",BIK,Surface);
//	////CopyMatVarFromMatToMat("$flashlighttexture",BIK,Surface);
//	////CopyMatVarFromMatToMat("$flashlighttextureframe",BIK,Surface);
//	//CopyMatVarFromMatToMat("$color2",BIK,Surface);
//	//CopyMatVarFromMatToMat("$srgbtint",BIK,Surface);
//	//CopyMatVarFromMatToMat("$ytexture",BIK,Surface);
//	////CopyMatVarFromMatToMat("$crtexture",BIK,Surface);
//	////CopyMatVarFromMatToMat("$cbtexture",BIK,Surface);
//	//CopyMatVarFromMatToMat("$nolod",BIK,Surface);
//	//CopyMatVarFromMatToMat("$nomip",BIK,Surface);
//
//	Surface->FindVar("$basetexture", &found, false)->CopyFrom(BIK->FindVar("$ytexture", &found, false));
//	
//	Msg("Debug Information for %s after copy:\n",Surface->GetName());
//
//	for (int N = 0; N < Surface->ShaderParamCount(); N++)
//	{
//		OutputIMaterialVar(Surface->GetShaderParams()[N]);
//	}
//
//
//	//IMaterialVar*	SurfaceVar	=	Surface->FindVar("$basetexture", &found, false);  // Get a reference to our base texture variable
//	//
//	//if (!found) { Msg(" !SurfaceVar "); return false; }
//
//	// Replace the texture with the BIK Material.
//	//SurfaceVar->SetTextureValue(BIKTex);
//	//SurfaceVar->CopyFrom(BIKVar);
//
//	Msg(" Success!!");
//
//	return true;
//}

bool C_Env_ffmpeg::LoadVideo( void )
{
	m_pProxy = LookupffmpegByTextureName(m_iszTextureName);

	char* GameDir = (char*)(engine->GetGameDirectory());

	if ( m_iszMovieName == NULL )
	{
		Warning("Env_ffmpeg didn't have MovieName set!\n");
		return false;
	}
	if ( GameDir == NULL )
	{
		Warning("Env_ffmpeg couldn't access the location of the game folder!\n");
		return false;
	}

	//size_t	FileNameLen	=	strlen(GameDir)+1+strlen(m_iszMovieName);
	//char*	FileName	=	new char[FileNameLen+1];

	//FileName[0] = 0;

	//strcat_s(FileName,FileNameLen+1,GameDir);
	//strcat_s(FileName,FileNameLen+1,"/");
	//strcat_s(FileName,FileNameLen+1,m_iszMovieName);

	char* FileName = "C:\\Users\\Sortie\\Videos\\oot2.mkv";

    // Open video file
    if(av_open_input_file(&pFormatCtx, FileName, NULL, 0, NULL)!=0)
	{
		Warning("Env_ffmpeg could not load the video file '%s'!\n",FileName);
		//delete[] FileName;
		return false;
	}
	
	//delete[] FileName;

	// Find the first video stream
    videoStream=-1;
    for(unsigned int i=0; i<pFormatCtx->nb_streams; i++)
	{
        if(pFormatCtx->streams[i]->codec->codec_type==CODEC_TYPE_VIDEO)
        {
            videoStream=i;
            break;
        }
	}
    if(videoStream==-1)
	{
		Warning("ffmpeg could not find a video stream in video file '%s'\n", m_iszMovieName);
	
		// Close the video file
		av_close_input_file(pFormatCtx);

        return false; // Didn't find a video stream
	}

	// Get a pointer to the codec context for the video stream
    pCodecCtx=pFormatCtx->streams[videoStream]->codec;

    // Find the decoder for the video stream
    pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
    
	if(pCodec==NULL)
	{		
		Warning("ffmpeg could not find a codec for the video stream in video file '%s'\n", m_iszMovieName);
	
		// Close the video file
		av_close_input_file(pFormatCtx);

        return false; // Codec not found
	}
	
    // Open codec
    if(avcodec_open(pCodecCtx, pCodec)<0)
	{		
		Warning("ffmpeg could not open the codec for the video stream in video file '%s'\n", m_iszMovieName);
	
		// Close the video file
		av_close_input_file(pFormatCtx);

        return false; // Could not open codec
	}
	
    // Allocate video frame
    pFrame=avcodec_alloc_frame();

	if(pFrame==NULL)
	{		
		Warning("ffmpeg could not open allocate the YUV frame for video file '%s'\n", m_iszMovieName);
	
		// Close the codec
		avcodec_close(pCodecCtx);

		// Close the video file
		av_close_input_file(pFormatCtx);

        return false; // Could not open codec
	}

    // Allocate an AVFrame structure
    pFrameRGB=avcodec_alloc_frame();

    if(pFrameRGB==NULL)
	{		
		Warning("ffmpeg could not open allocate the RGB frame for video file '%s'\n", m_iszMovieName);
	
		// Free the YUV frame
		av_free(pFrame);

		// Close the codec
		avcodec_close(pCodecCtx);

		// Close the video file
		av_close_input_file(pFormatCtx);

        return false; // Could not open codec
	}
	
    sws_flags = av_get_int(sws_opts, "sws_flags", NULL);

    // Determine required buffer size and allocate buffer
    int	numBytes=avpicture_get_size(PIX_FMT_BGRA, pCodecCtx->width, pCodecCtx->height);

	if ( pFrameSize != (size_t)numBytes )
	{
		if ( pFrameRGBBiffer ) { delete[] pFrameRGBBiffer; }
		for ( size_t I = 0; I < FFMPEG_FRAMES_IN_ADVANCE; I++ )
		{
			if ( pFrameRGBBackBuffer[I] ) { delete[] pFrameRGBBackBuffer[I]; }
		}
	}

    pFrameRGBBiffer	= new uint8_t[numBytes];
	for ( size_t I = 0; I < FFMPEG_FRAMES_IN_ADVANCE; I++ )
	{
		pFrameRGBBackBuffer[I]	= new uint8_t[numBytes];
	}

	pFrameSize	=	(size_t)numBytes;

    // Assign appropriate parts of buffer to image planes in pFrameRGB
    avpicture_fill((AVPicture *)pFrameRGB, pFrameRGBBiffer, PIX_FMT_BGRA,
        pCodecCtx->width, pCodecCtx->height);

	NumFramesInAdvanced		=	0;

	return true;
}

bool C_Env_ffmpeg::GetVideoFrame()
{
	bool	Result			=	true;
	bool	FrameFinished	=	false;	
	int		ReadResult		=	0;

	while((ReadResult = av_read_frame(pFormatCtx, &packet))>=0 && FrameFinished == false )
	{
		// Is this a packet from the video stream?
		if(packet.stream_index==videoStream)
		{
			NumFramesInAdvanced++;

			// Decode video frame
			avcodec_decode_video(pCodecCtx, pFrame, &frameFinished, 
				packet.data, packet.size);

			// Did we get a video frame?
			if(frameFinished)
			{
				FrameFinished	=	true;

				img_convert_ctx = sws_getCachedContext(img_convert_ctx,
				pCodecCtx->width, pCodecCtx->height,
				pCodecCtx->pix_fmt,
				pCodecCtx->width, pCodecCtx->height,
				PIX_FMT_BGRA, sws_flags, NULL, NULL, NULL);

				if (img_convert_ctx == NULL)
				{
					Warning("Env_ffmpeg had img_convert_ctx == NULL!\n");
					return false;
				}

				// Convert the image from its native format to RGB

				sws_scale(
					img_convert_ctx,		//	struct SwsContext *context
					pFrame->data,			//	uint8_t* src[]
					pFrame->linesize,		//	int srcStride[]
					0,						//	int srcSliceY
					pCodecCtx->height,		//	int srcSliceH
					pFrameRGB->data,		//	uint8_t* dst[]
					pFrameRGB->linesize);	//	int dstStride[]
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}
	if ( ReadResult > 0 )
	{
		// EOF
		Result	=	false;
	}

	return Result;
}

// Copy to the real out buffer!
bool C_Env_ffmpeg::PutVideoFrame(int* Buffer, int W, int H)
{
	NumFramesInAdvanced--;

	if ( ffmpeg_frame2surface_method.GetInt() == 1 )
	{
		// Write pixel data
		int y = 0;

		for(y=0; y<pCodecCtx->height; y++)
		{
			Assert( y*pFrameRGB->linesize[0] < W*H*3 );

			memcpy(Buffer+y*3*W,pFrameRGB->data[0]+y*pFrameRGB->linesize[0],pFrameRGB->linesize[0]);

		}
	}
	else if ( ffmpeg_frame2surface_method.GetInt() == 2 )
	{						
		// Write pixel data
		int		y		=	0;
		int		x		=	0;
		int		xmax	=	W/2;
		int*	WriteMe	=	0;
		int*	ReadMe	=	0;

		Assert( pFrameRGB->linesize[0] % 4 == 0 );
		Assert( W % 4 == 0 );

		for(y=0; y<pCodecCtx->height; y++)
		{
			Assert( y*pFrameRGB->linesize[0] < W*H*4 );

			WriteMe		=	(int*)((char*)Buffer+y*4*W);
			ReadMe		=	(int*)((char*)pFrameRGB->data[0]+y*pFrameRGB->linesize[0]);
			
			for ( x = 0; x < xmax; x++ )
			{
				*(int*)(WriteMe+x)	=	*(int*)(ReadMe+x);
			}
		}
	}
	else if ( ffmpeg_frame2surface_method.GetInt() == 0 )
	{						
		// Write pixel data
		int		y		=	0;
		int		x		=	0;
		int		xmax	=	pCodecCtx->width;
		int*	WriteMe	=	0;
		int*	ReadMe	=	0;

		Assert( pFrameRGB->linesize[0] % 4 == 0 );
		Assert( W % 4 == 0 );
		
		// Draw a black border around the actual image
		y = H; WriteMe = Buffer+y*W; // Above
		for ( x=0; x<W; x++ ) { *(int*)(WriteMe+x) = 0; }
		y = pCodecCtx->height; WriteMe = Buffer+y*W; // Below
		for ( x=0; x<W; x++ ) { *(int*)(WriteMe+x) = 0; }
		x = W-1; WriteMe = Buffer+x; // To the left
		for ( y=0; y<H; y++ ) { *WriteMe = 0; WriteMe += W; }
		x = pCodecCtx->width; WriteMe = Buffer+x; // To the right
		for ( y=0; y<H; y++ ) { *WriteMe = 0; WriteMe += W; }

		// Draw the actual image
		for(y=0; y<pCodecCtx->height; y++)
		{
			Assert( y*pFrameRGB->linesize[0] < W*H*4 );

			WriteMe		=	Buffer+y*W;
			ReadMe		=	(int*)((char*)pFrameRGB->data[0]+y*pFrameRGB->linesize[0]);

			for ( x = 0; x < xmax; x++ )
			{
				*(int*)(WriteMe+x)	=	*(int*)(ReadMe+x);
			}
		}
	}

	return	true;
}

bool C_Env_ffmpeg::Render(int* Buffer, int W, int H)
{
	if ( !ShouldPlayback ) { return false; } // Don't render!

	if ( CurrentFrame == -1 )
	{
		if ( LoadVideo() )
		{
			CurrentFrame	=	0;
		}
		else
		{
			ShouldPlayback	=	false;
			return false;
		}
	}

	// Check to see what frame should be at now - and if it's not the current frame, simply 
	// read the next frame and increment the framenumber.

	// Todo: How does this work if the ingame frame rate is lower than the video's framerate?

	// Todo: Find a reliable method to detect the real framerate of the movie instead of
	// providing it via Hammer.

	if ( m_fFrameRateHack == 0.0 )
	{
		m_fFrameRateHack = 30.0;
	}

	int64_t		DesiredFrame	=	(int64_t)((gpGlobals->curtime-m_fBegun)*m_fFrameRateHack);

	if ( CurrentFrame > DesiredFrame ) { return false; }

	//avpicture_layout((AVPicture *)pFrameRGB,W,pCodecCtx->width,pCodecCtx->height,(unsigned char*)Buffer,3*H*W);
	
	int64_t		Iterations		=	0;


// Todo: Fix the FFmpeg frame loop by introducing seeking:
	/*
	> Let's say I want to seek to a frame that is 14.25 seconds into a stream.
	> Would someone please post actual code showing (1) how to calculate the
	> value for 14.25 seconds to pass to av_seek_frame to seek to the nearest
	> key frame (Ian's question), and (2) a recommended way to actually seek
	> to the precise frame for 14.25 seconds, regardless of whether it is a
	> keyframe (my question).

	My example code from earlier in this thread, with the correction suggested by 
	Michael...

	// normal frame loop av_read_frame() done above here
	if( seekRequested ) {
		TARGET_PTS = 14.25 * AV_TIME_BASE;
		av_seek_frame( fmtCtx, -1, TARGET_PTS, AVSEEK_FLAG_BACKWARD );
		CodecCtx->hurry_up = 1;
		do {
			av_read_frame( fmtCtx, &Packet );
			// should really be checking that this is a video packet
			MyPts = av_rescale( Packet.pts,
				AV_TIME_BASE * (int64_t) Stream->time_base.num,
				Stream->time_base.den );
			// Once we pass the target point, break from the loop
			if( MyPts >= TARGET_PTS )
				break;
			avcodec_decode_video( CodecCtx, pFrame, &gotFrame, Packet.data,
				Packet.size );
			av_free_packet( &Packet );
		} while(1);
		CodecCtx->hurry_up = 0;
	}
	// Continue with normal frame loop (i.e. decode, show and free)

	The documentation for av_seek_frame() (and the code) says that if the stream 
	is not given (i.e. -1) then AV_TIME_BASE units are used, otherwise the time 
	base for the particular stream is used - in which case you would need to 
	rescale TARGET_PTS to stream units before calling it.  This would be done as 
	the inverse of the MyPts calculation above.  Looking at the code again, it 
	would also be better to use the stream time base throughout, which would then 
	save the repeated call to av_rescale() every loop.

	I've only recently started working with the libav* libraries myself, but have 
	found that everything I want is there; it's finding the thing that's the 
	trick :-)
	*/
// Above is from http://lists.mplayerhq.hu/pipermail/ffmpeg-devel/2005-July/002359.html

#ifdef ENABLE_FFMPEG_EXPERIMENTAL_SEEKING
	// This is a bit buggy - While it does appear to do some sort of seeking, it tends
	// to seek to the wrong time. It looks like the time it seeks to is somewhere in the past
	// compared to what we want. This time rescale might not be working as intended.
	if ( DesiredFrame-CurrentFrame > 10 )
	{
		// Reseek if we are more than 10 frames behind!
		int			stream_index	=	videoStream;
		int64_t		seek_target		=	DesiredFrame;
		int			seek_flags		=	0;
		AVRational	Rational;

		Rational.num				=	1;
		Rational.den				=	AV_TIME_BASE;

		// Rescale the time requested!
		//seek_target		=	av_rescale_q(seek_target, Rational, pFormatCtx->streams[stream_index]->time_base);

		// Do the actual seeking!
		if( av_seek_frame(pFormatCtx, stream_index, seek_target, seek_flags) < 0)
		{
			// Error!
			Warning("FFmpeg failed to seek to frame %d!\n",DesiredFrame);
		}
		else
		{
			Msg("FFmpeg searched to %d\n",DesiredFrame);
		}

		//avcodec_flush_buffers(pFormatCtx->streams[stream_index]->codec);

		CurrentFrame	=	DesiredFrame;
	}
#endif

	while ( CurrentFrame <= DesiredFrame )
	{
		Iterations++;
		if ( Iterations > 3 ) { break; }
		//{
		//	Warning("ffmpeg is %d behind!\n",DesiredFrame-CurrentFrame+3);
		//	// If we're behind, update a bit more often, but don't over-do it
		//	// in case we get far too behind, it'll be quite messy to do it this way.
		//
		//	break;
		//}

		if ( CurrentFrame % 30 == 0 )
		{
			Msg("FFmpeg is at frame %d\n",CurrentFrame);
		}

		CurrentFrame++;

		// Try and get ahead of the real outputted buffer, and if we are,
		// try and remain ahead.

		if ( NumFramesInAdvanced )
		{
			PutVideoFrame(Buffer,W,H);
			GetVideoFrame();
		}
		else
		{
			GetVideoFrame();
			PutVideoFrame(Buffer,W,H);
			GetVideoFrame();
		}		
	}

	return	true;
}


void C_Env_ffmpeg::OnDataChanged( DataUpdateType_t updateType )
{
    BaseClass::OnDataChanged( updateType );

    if ( updateType == DATA_UPDATE_CREATED )
    {
		m_pProxy = LookupffmpegByTextureName(m_iszTextureName);
		if ( m_pProxy )
		{
			m_pProxy->m_VideoEntity	=	this;
			if ( m_pProxy->m_pTextureRegen )
			{
				m_pProxy->m_pTextureRegen->m_VideoEntity	=	this;
			}
		}
	}
	//m_pProxy->SetLooping(m_bLoop);
	if (m_iPlay > m_iLastPlay)
	{
		CurrentFrame	=	-1;
		ShouldPlayback	=	true;

		//m_pProxy->Play();
	}
	else if (m_iPlay < m_iLastPlay)
	{
		//m_pProxy->Pause();
	}
	if (strcmp(m_iszMovieName, m_iszLastMovieName))
	{
		//m_pProxy->SetMovie(m_iszMovieName);
	}

	if (m_iAdvanceFrame != m_iLastAdvanceFrame)
	{
		//m_pProxy->AdvanceFrame();
	}

	strcpy(m_iszLastMovieName, m_iszMovieName);
	m_iLastAdvanceFrame = m_iAdvanceFrame;
	m_iLastPlay = m_iPlay;
}

#endif