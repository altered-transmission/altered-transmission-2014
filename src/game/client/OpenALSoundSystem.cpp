// OpenALSoundSystem.cpp
#include "cbase.h"
#include "igamesystem.h"
#include "LibAVCodecDecoder.h"
#include "OpenALSoundSystem.h"

#define FAKE3DSOUND

#if defined(ALTERED_TRANSMISSION) && defined(OPENAL_SOUND)

COpenALSoundSystem g_OpenALSoundSystem;
COpenALSoundSystem* g_pOpenALSoundSystem = &g_OpenALSoundSystem;

//=============================================================================
// OpenAL Sound System
//=============================================================================

COpenALSoundSystem::COpenALSoundSystem() :
	BaseClass( "COpenALSoundSystem" ),
	m_IsPaused(true),
	m_First(NULL)
{
	
}

COpenALSoundSystem::~COpenALSoundSystem()
{
	
}

bool COpenALSoundSystem::Init()
{
	if ( alutInit(NULL, NULL) == AL_FALSE )
	{
		Warning("Could not initialize OpenAL!\n");
		return false;
	}

	// Set the speed of sound for the Source Engine.
	alSpeedOfSound(13397.2441f);
#ifndef FAKE3DSOUND
	alDistanceModel(AL_INVERSE_DISTANCE);
#endif
	
	return true;
}

void COpenALSoundSystem::PostInit()
{
}

void COpenALSoundSystem::Shutdown()
{
	alutExit();
}

// Level init, shutdown
void COpenALSoundSystem::LevelInitPreEntity()
{
}

void COpenALSoundSystem::LevelInitPostEntity()
{
}

void COpenALSoundSystem::LevelShutdownPreEntity()
{
}

void COpenALSoundSystem::LevelShutdownPostEntity()
{
}

void COpenALSoundSystem::OnSave()
{
}

void COpenALSoundSystem::OnRestore()
{
}

void COpenALSoundSystem::SafeRemoveIfDesired()
{
}

// Called before rendering
void COpenALSoundSystem::PreRender()
{
}

// Gets called each frame
void COpenALSoundSystem::Update(float frametime)
{
	if ( frametime == 0.0f ) { if ( !m_IsPaused ) { OnGamePaused(); } return; }
	if ( m_IsPaused ) { OnGameUnpaused(); }

	CBasePlayer* Player = CBasePlayer::GetLocalPlayer();

	if ( Player != NULL )
	{
		alListenerfv(AL_POSITION, &Player->GetAbsOrigin().x);

		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting listener position\n"); }

		alListenerfv(AL_VELOCITY, &Player->GetAbsVelocity().x);

		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting listener velocity\n"); }

		Vector Forward;
		Vector Up;

		AngleVectors(Player->GetAbsAngles(), &Forward, NULL, &Up);

		float Orientation[6] = { Forward.x, Forward.y, Forward.z, Up.x, Up.y, Up.z };
					
		alListenerfv(AL_ORIENTATION, Orientation);

		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting listener orientation\n"); }

		m_ListenerPosition = Player->GetAbsOrigin();
		m_ListenerForward = Forward;
	}

	for ( IOpenALSound* TMP = m_First; TMP != NULL; TMP = TMP->m_NextSound )
	{
		TMP->OnGameFrame(frametime);
	}
}

// Called after rendering
void COpenALSoundSystem::PostRender()
{
}

void COpenALSoundSystem::OnGamePaused()
{
	if ( m_IsPaused ) { return; }

	m_IsPaused = true;
}

void COpenALSoundSystem::OnGameUnpaused()
{
	if ( !m_IsPaused ) { return; }

	m_IsPaused = false;
}

bool COpenALSoundSystem::HandleSound(IOpenALSound* Sound)
{
	Sound->m_NextSound = m_First;
	Sound->m_PrevSound = NULL;
	if ( m_First ) { m_First->m_PrevSound = Sound; }
	m_First = Sound;

	Sound->m_SoundSystem = this;

	return true;
}

void COpenALSoundSystem::RemoveSound(IOpenALSound* Sound)
{
	if ( Sound->m_NextSound ) { Sound->m_NextSound->m_PrevSound = Sound->m_PrevSound; }
	if ( Sound->m_PrevSound ) { Sound->m_PrevSound->m_NextSound = Sound->m_NextSound; }
	if ( m_First == Sound ) { m_First = Sound->m_NextSound; }
}

//=============================================================================
// OpenAL Sound Stream
//=============================================================================

COpenALSoundStream::COpenALSoundStream()
{
	m_bPauseOnGamePause = true;
	m_Buffers = NULL;
}

COpenALSoundStream::~COpenALSoundStream()
{
	if ( m_Buffers )
	{
		alDeleteSources(1, &m_Source);
		alDeleteBuffers(m_BufferNum, m_Buffers);
	}
}

void COpenALSoundStream::SetLocation(Vector &Vec)
{
	// TODO: Set proper parameters if not already 3D sound and buffers are allocated.
	m_3DSound = true;
	m_Location = Vec;
}

void COpenALSoundStream::SetLocation(CBaseEntity* Parent)
{
	// TODO: Set proper parameters if not already 3D sound and buffers are allocated.
	m_3DSound = true;
	m_Parent = Parent;
	m_ParentAttachment = 0;
}

void COpenALSoundStream::SetLocation(CBaseEntity* Parent, int ParentAttachment)
{
	// TODO: Set proper parameters if not already 3D sound and buffers are allocated.
	m_3DSound = true;	
	m_Parent = Parent;
	m_ParentAttachment = ParentAttachment;
}

void COpenALSoundStream::OnGamePaused()
{
	if ( !m_Buffers || m_bPauseOnGamePause ) { return; }

	alSourcePause(m_Source);
}

void COpenALSoundStream::OnGameUnpaused()
{
	if ( !m_Buffers || m_bPauseOnGamePause ) { return; }

	alSourcePlay(m_Source);
}

void COpenALSoundStream::OnGameFrame(float DeltaTime)
{
	UpdateSound();
}

void COpenALSoundStream::UpdateSound()
{
	if ( m_3DSound )
	{
		Vector PlayLocation;
		Vector PlayVelocity;

		if ( m_Parent != NULL )
		{
			QAngle Angles;
			Quaternion AnglesVelocity;

			if	( m_ParentAttachment == 0
				|| !m_Parent->GetAttachment(m_ParentAttachment, PlayLocation, Angles)
				|| !m_Parent->GetAttachmentVelocity(m_ParentAttachment, PlayVelocity, AnglesVelocity) )
			{
				PlayLocation = m_Parent->GetAbsOrigin();
				PlayVelocity = m_Parent->GetAbsVelocity();
			}
		}
		else
		{
			PlayLocation = m_Location;
			PlayVelocity = Vector(0.0f, 0.0f, 0.0f);
		}

		// Set the sound position.
		alSourcefv(m_Source, AL_POSITION, &PlayLocation.x);

		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting source position\n"); }

		alSourcefv(m_Source, AL_VELOCITY, &PlayVelocity.x);

		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting source velocity\n"); }

#ifdef FAKE3DSOUND
		// Calculate a fake 3D sound volume!
		vec_t DistanceSquare = PlayLocation.DistToSqr(m_SoundSystem->m_ListenerPosition);

		float Intensity = 1.0f  / (4.0f * M_PI * DistanceSquare);

		float NormalDist = 384;
		float NormalIntensity = 1.0f  / (4.0f * M_PI * NormalDist*NormalDist);

		float Ratio = min(Intensity / NormalIntensity, 1.0f);

		// Simulate loundness when looking directly at sound.
		float Additional = abs( m_SoundSystem->m_ListenerForward.Dot(PlayLocation - m_SoundSystem->m_ListenerPosition) / sqrt(DistanceSquare) );

		// Now calculate the actual loudness.
		float Gain = Ratio * (0.5f + Additional / 2.0f);

		alSourcef(m_Source, AL_GAIN, Gain);
#endif
	}
}

bool COpenALSoundStream::AllocateBuffers(ALuint Num, ALsizei BufferSize)
{
	if ( Num < 1 || BufferSize == 0 ) { return false; }

	m_Buffers = new ALuint[Num];

	// Generate the buffers.
	alGenBuffers(Num, m_Buffers);

	if ( alGetError() != AL_NO_ERROR )
	{
		Warning("COpenALSoundStream::AllocateBuffers could not generate %u buffers!\n", Num);
		delete[] m_Buffers;
		return false;
	}

	// Generate the sources.
	alGenSources(1, &m_Source);

	if ( alGetError() != AL_NO_ERROR )
	{
		Warning("COpenALSoundStream::AllocateBuffers could not generate a source!\n");
		alDeleteBuffers(Num, m_Buffers);
		delete[] m_Buffers;
		return false;
	}

	if ( m_3DSound )
	{
		alSourcef(m_Source, AL_ROLLOFF_FACTOR, 0.5f);
		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting rolloff factor for source\n"); }
		alSourcei(m_Source, AL_SOURCE_RELATIVE, AL_TRUE);
		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting source relative for source\n"); }
		alSourcef(m_Source, AL_PITCH, 1.0f);
		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting pitch for source\n"); }
		alSourcef(m_Source, AL_GAIN, 1.0f);
		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting gain for source\n"); }
		alSourcef(m_Source, AL_MAX_DISTANCE , 512.0f);
		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error setting max distance for source\n"); }
	}
	else
	{
		Warning("Allocating an, uh, 2D sound.");
		// Set parameters so mono sources won't distance attenuate.
		alSourcei(m_Source, AL_SOURCE_RELATIVE, AL_TRUE);
		alSourcei(m_Source, AL_ROLLOFF_FACTOR, 0);
	}

	if ( alGetError() != AL_NO_ERROR )
	{
		Warning("COpenALSoundStream::AllocateBuffers could not set parameters for source!\n");
		alDeleteSources(1, &m_Source);
		alDeleteBuffers(Num, m_Buffers);
		delete[] m_Buffers;
		return false;
	}

	m_BufferNum = Num;
	m_BufferSize = BufferSize;
	m_BuffersInitialized = 0;
	m_BuffersQueued = 0;

	// TODO: This might not be needed.
	//alSourceRewind(m_Source);
	//alSourcei(m_Source, AL_BUFFER, 0);

	return true;
}

size_t COpenALSoundStream::AvailableAudioBufferSpace()
{
	// If all buffers hasn't been filled up yet, go ahead and fill them up!
	if ( m_BuffersInitialized < m_BufferNum ) { return m_BufferSize; }

	ALint Processed = 0;

	alGetSourcei(m_Source, AL_BUFFERS_PROCESSED, &Processed);

	if ( Processed == 0 )
	{
		// Possibly restart the sound here, if not meant to be paused. See alffmpeg.c.

		// All buffers are full. Check if the source is still playing. If not, restart it.
		ALint State;

		alGetSourcei(m_Source, AL_SOURCE_STATE, &State);

		// TODO: This error does not halt the attempted playback.
		if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error checking source state\n"); }

		if ( State != AL_PLAYING )
		{
			alSourcePlay(m_Source);

			if ( alGetError() != AL_NO_ERROR ) { Warning("COpenALSoundStream: error checking restarting playback\n"); }
		}

		// None of our buffers are unused yet, so just say no buffer space is available.
		return 0;
	}

	// Alright, we have Processed free. Just return the size of a buffer, and we will
	// receive a bufferful of data soon. Then this function will be called again.
	return m_BufferSize;
}

void COpenALSoundStream::ClearAudioBuffers()
{
	// Rewind the source and unqueue all buffers for this source.
	alSourceRewind(m_Source);
	alSourcei(m_Source, AL_BUFFER, 0);
}

bool COpenALSoundStream::OnAudioInformation(int Rate, int Channels, int Bits)
{
	m_Rate = Rate;
	m_Channels = Channels;
	m_Bits = Bits;

	m_Format = 0;

	if ( m_Bits == 8 )
	{
		if ( m_Channels == 1 ) { m_Format = AL_FORMAT_MONO8; }
		if ( m_Channels == 2 ) { m_Format = AL_FORMAT_STEREO8; }
		if ( alIsExtensionPresent("AL_EXT_MCFORMATS") )
		{
			if (m_Channels == 4) { m_Format = alGetEnumValue("AL_FORMAT_QUAD8"); }
			if (m_Channels == 6) { m_Format = alGetEnumValue("AL_FORMAT_51CHN8"); }
		}
	}
	else if ( m_Bits == 16 )
	{
		if ( m_Channels == 1 ) { m_Format = AL_FORMAT_MONO16; }
		if ( m_Channels == 2 ) { m_Format = AL_FORMAT_STEREO16; }
		if ( alIsExtensionPresent("AL_EXT_MCFORMATS") )
		{
			if ( m_Channels == 4 ) { m_Format = alGetEnumValue("AL_FORMAT_QUAD16"); }
			if ( m_Channels == 6 ) { m_Format = alGetEnumValue("AL_FORMAT_51CHN16"); }
		}
	}

	if ( m_Format == 0 )
	{
		Warning("Unhandled format in COpenALSoundStream::OnAudioInformation! (%i channels, %i bits)\n", m_Channels, m_Bits);
		return false;
	}

	return true;
}

bool COpenALSoundStream::OnAudioData(void* Data, size_t DataSize)
{
	ALuint Buffer = 0;

	if ( m_BuffersInitialized < m_BufferNum )
	{
		Buffer = m_Buffers[m_BuffersInitialized];
		m_BuffersInitialized++;

		if ( m_BuffersInitialized == m_BufferNum )
		{
			alSourcePlay(m_Source);

			if ( alGetError() != AL_NO_ERROR )
			{
				Warning("Error starting playback in COpenALSoundStream::OnAudioData!\n");
				return false;
			}
		}
	}
	else
	{
		alSourceUnqueueBuffers(m_Source, 1, &Buffer);
	}

	if ( Buffer != 0 )
	{
		alBufferData(Buffer, m_Format, Data, (ALsizei) DataSize, m_Rate);
		alSourceQueueBuffers(m_Source, 1, &Buffer);

		m_BuffersQueued++;

		// For each successfully unqueued buffer, increment the base time. The
		// retrieved sample offset for timing is relative to the start of the
		// buffer queue, so for every buffer that gets unqueued we need to
		// increment the base time to keep the reported time accurate and not
		// fall backwards
		m_BaseTime++;
	}

	ALenum ErrorCode;
	if ( ( ErrorCode = alGetError()) != AL_NO_ERROR )
	{
		Warning("Error %i buffering data in COpenALSoundStream::OnAudioData!\n", ErrorCode);
		return false;
	}

	return true;
}

void COpenALSoundStream::OnAudioStreamEnd()
{
	// TODO: Clean up here!
}

//=============================================================================
// OpenAL Hello World Code
//=============================================================================

// Warning this is horrible test code I used to test the system.
// AFAIK, it might be very, very crashy.

void al_helloworld_f(const CCommand& args)
{
	// Attempt to find a prop_physics!

	C_AllBaseEntityIterator Iter;

	CBaseEntity* Entity = NULL;

	for ( CBaseEntity* TMP = Iter.Next(); TMP != NULL; TMP = Iter.Next() )
	{
		Vector Vec = TMP->GetAbsOrigin();

		if ( Vec.x > 256-64 && Vec.x < 256+64 && Vec.y > -64 && Vec.y < 64 && Vec.z > -64 && Vec.z < 64 ) { Entity = TMP; break; }
	}

	char File[MAX_PATH + 1];
	File[0] = 0;
	Q_strcat(File, engine->GetGameDirectory(), MAX_PATH);
	Q_strcat(File, "\\media\\OpenAL-Test.mp3", MAX_PATH);

	// Create a new OpenAL Sound, which streams its audio from somewhere else.
	COpenALSoundStream* Sound = new COpenALSoundStream;

	// Set up the 3D world position.
	if ( Entity ) { Sound->SetLocation(Entity); } else { Warning("Could not find a suitable entity!\n"); }

	// Allocate and set up the buffers for this sound stream.
	Sound->AllocateBuffers();

	// Tell the OpenAL sound system to take care of this sound. This will handle
	// 3D sound simulation (updating the location of the sound in regards to the
	// players position, setting proper velocities, applying the doppler effect),
	// handle save/loads and so on.
	g_pOpenALSoundSystem->HandleSound(Sound);

	// Create a media player to decode audio for our sound.
	CLibAVCodecPlayer* Player = new CLibAVCodecPlayer;

	// Load a file containing an audio stream into our player. Of course, the file
	// won't be loaded until the player is initialized in the libavcodec main loop.
	Player->LoadFile(File);

	// Link the player's audio output to our sound.
	Player->SetAudioDest(Sound);

	// Add the player to the LibAVCodec thread's mainloop, and our sound will start
	// playing soon. This will also handle when the game is paused.
	g_pLibAVCodecSystem->AddPlayer(Player);
}

ConCommand al_helloworld( "al_helloworld", al_helloworld_f, "al_helloworld", 0);

#endif
