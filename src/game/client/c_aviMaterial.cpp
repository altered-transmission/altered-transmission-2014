
// Nate Nichols, InfoLab Northwestern University, August 2006.
#include "cbase.h"

// OBSOLETE OLD STUFF
#if 0
#include "c_aviMaterial.h"


// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

C_AVIMaterial::C_AVIMaterial()
{
	m_iAdvanceFrame = 0;
	m_iLastAdvanceFrame = 0;
	m_iPlay = 0;
	m_iLastPlay = 0;
}

void C_AVIMaterial::OnDataChanged( DataUpdateType_t updateType )
{
    BaseClass::OnDataChanged( updateType );
    if ( updateType == DATA_UPDATE_CREATED )
    {
		m_pProxy = CAviMaterialProxy::LookupByTextureName(m_iszTextureName);
	}
	m_pProxy->SetLooping(m_bLoop);
	if (m_iPlay > m_iLastPlay)
	{
		m_pProxy->Play();
	}
	else if (m_iPlay < m_iLastPlay)
	{
		m_pProxy->Pause();
	}
	if (strcmp(m_iszMovieName, m_iszLastMovieName))
	{
		m_pProxy->SetMovie(m_iszMovieName);
	}

	if (m_iAdvanceFrame != m_iLastAdvanceFrame)
	{
		m_pProxy->AdvanceFrame();
	}

	strcpy(m_iszLastMovieName, m_iszMovieName);
	m_iLastAdvanceFrame = m_iAdvanceFrame;
	m_iLastPlay = m_iPlay;
}
#endif