#include "cbase.h"
#if 0
//=============================================================================
// Sun Shaft Black Texture
//=============================================================================
static CTextureReference s_pSunShaftBlackTexture;
ITexture *GetSunShaftBlackTexture( void )
{ 
	if ( !s_pSunShaftBlackTexture )
	{
		s_pSunShaftBlackTexture.Init( materials->FindTexture( "_rt_SunShaftBlack", TEXTURE_GROUP_RENDER_TARGET ) );
		Assert( !IsErrorTexture( s_pSunShaftBlackTexture ) );
		AddReleaseFunc();
	}
	return s_pSunShaftBlackTexture;
}


// also in the shutdown:
s_pSunShaftBlackTexture.Shutdown();

// Don't forget to add that to the header as well
#endif