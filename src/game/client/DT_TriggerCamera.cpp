// Header here please - by Sortie - 2009!
#include "cbase.h"
//#include "util.h"
// client

#ifdef ALTERED_TRANSMISSION

//This is the client side of the entity.  The corresponding server entity is C_Env_Bink.
class C_TriggerCamera : public C_BaseEntity
{
public:
	DECLARE_CLIENTCLASS();
	DECLARE_CLASS( C_TriggerCamera, C_BaseEntity );

	void	OnDataChanged(DataUpdateType_t updateType);

private:
	QAngle	PlayerAngle;
	bool	ShouldUpdate;
};

IMPLEMENT_CLIENTCLASS_DT(C_TriggerCamera, DT_TriggerCamera, CTriggerCamera)
	RecvPropQAngles(RECVINFO(PlayerAngle)),
	RecvPropInt(RECVINFO(ShouldUpdate)),
END_RECV_TABLE()

void C_TriggerCamera::OnDataChanged( DataUpdateType_t updateType )
{
	Msg("C_TriggerCamera::OnDataChanged(");
    BaseClass::OnDataChanged( updateType );
    if ( updateType == DATA_UPDATE_CREATED )
    {
	}
	if ( ShouldUpdate )
	{
		Msg("Updating Angles...");
		engine->SetViewAngles	(PlayerAngle);

		//C_BasePlayer*	pPlayer	=	UTIL_GetLocalPlayer();
		//pPlayer->SetAbsAngles	(PlayerAngle);		
		//pPlayer->SetViewAngles	(PlayerAngle);
	}
	Msg(");\n");
}

#endif