// Nate Nichols, InfoLab Northwestern University, August 2006.  Much of this was provided by Valve.  Thanks.

#include "cbase.h"

// OBSOLETE OLD STUFF
#if 0

#include "vtf/vtf.h"
#include "pixelwriter.h"
#include "AVIMaterialRegen.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"



CAviTextureRegen::CAviTextureRegen()
{
	m_pAVIFile = NULL;
	m_pAVIStream = NULL;
	m_pGetFrame = NULL;
	m_bih = NULL;
	m_memdc = NULL;
	m_DIBSection = NULL;
	m_bPlay = false;
	m_bPaused = false;
	m_fLastRedraw = -1.0f;
	m_bLoop = true;
	LastError = -1;
}

bool CAviTextureRegen::SetMovie(const char* filename)
{
	m_pMovieFilename = filename;
	DestroyVideoStream();
	HRESULT hr = AVIFileOpen(&m_pAVIFile, filename, OF_READ, NULL);

	if ( hr != AVIERR_OK )
	{
		Warning("Failed to load movie: \"%s\"\n",filename);
		return false;
	}
	// Get AVI size
	AVIFILEINFO info;
	AVIFileInfo( m_pAVIFile, &info, sizeof(info) );
	m_nAVIWidth = info.dwWidth;
	m_nAVIHeight = info.dwHeight;
	m_nFrameRate = (int)( (float)info.dwRate / (float)info.dwScale + 0.5f );
	m_nFrameCount = info.dwLength;
	CreateVideoStream();
	m_fLastRedraw = -1.0f;
	m_fRunTime = 0.0f;
	return true;
}

void CAviTextureRegen::Release()
{
	DestroyVideoStream();
}

void CAviTextureRegen::SetTime( float flTime )
{
	//DevMsg("CAviTextureRegen::SetTime(%d) {",flTime);

	if ( flTime < 0 )
	{
		flTime = 0.0f; // Ensure valid times!
	}

	if ( m_pAVIStream )
	{
		//DevMsg("m_nFrameRate = %i;",m_nFrameRate);
		m_nCurrentSample = (int)( flTime * m_nFrameRate + 0.5f );
		//DevMsg("m_nCurrentSample = %i;",m_nCurrentSample);
		if (m_nCurrentSample >= m_nFrameCount)
		{
			//DevMsg("End;");
			m_fRunTime = 0.0f;
			m_nCurrentSample = m_nFrameCount - 1;
			if (!m_bLoop)
			{
				//DevMsg("NoLoop;");
				Stop();
			}
			else
			{
				//DevMsg("Loop;");
			}
		}
	}
	else
	{
		//DevMsg("m_pAVIStream = 0;");
	}
	//DevMsg("}\n",flTime);
}

void CAviTextureRegen::Play()
{
	m_fLastRedraw = gpGlobals->curtime;
	m_bPlay = true;
	m_bPaused = false;
}

void CAviTextureRegen::Pause()
{
	m_bPaused = true;
}

void CAviTextureRegen::Stop()
{
	m_bPaused = false;
	m_bPlay = false;
	m_fLastRedraw = -1.0f;
}

void CAviTextureRegen::AdvanceFrame()
{
	if (m_pAVIStream)
		m_nCurrentSample++;
}

void CAviTextureRegen::SetLooping(bool bLoop)
{
	m_bLoop = bLoop;
}

void CAviTextureRegen::RegenerateTextureBits( ITexture *pTexture, IVTFTexture *pVTFTexture, Rect_t *pRect )
{
	int i, y, nIncY;
	// Set up the pixel writer to write into the VTF texture
	CPixelWriter pixelWriter;
	pixelWriter.SetPixelMemory( pVTFTexture->Format(), pVTFTexture->ImageData(0, 0, 0), pVTFTexture->RowSizeInBytes( 0 ) );

	int xmax = pRect->x + pRect->width;
	int ymax = pRect->y + pRect->height;
	int x;//, y;

	// Altered Transmission: 
	// Why is it clearing the buffer when it's about be
	// overridden anyways?

	/*for( y = pRect->y; y < ymax; ++y )
	{
		pixelWriter.Seek( pRect->x, y );

		for( x=pRect->x; x < xmax; ++x )
		{
			pixelWriter.WritePixel( 0, 0, 0, 0 );
		}
	}*/

	if ((!m_bPlay) || (!m_pAVIStream))
	{
		if (!m_pAVIStream)
		{			
			int nBytes = pVTFTexture->ComputeTotalSize();
			memset( pVTFTexture->ImageData(), 0xFF, nBytes );
			if ( LastError != 4 )
			{
				Msg("Bombing out because m_pAVIStream isn't setup yet!\n");
				LastError = 4;
			}
			return;
		}
		m_fLastRedraw = gpGlobals->curtime; //increment regardless
		
		// If no drawage, clean the buffer dude.

		for( y = pRect->y; y < ymax; ++y )
		{
			pixelWriter.Seek( pRect->x, y );

			for( x=pRect->x; x < xmax; ++x )
			{
				pixelWriter.WritePixel( 0, 0, 0, 0 );
			}
		}

		return;
	}

	// Ensure valid times
	if ( m_fLastRedraw < 0)
	{
		m_fLastRedraw = gpGlobals->curtime;
	}

	if (!m_bPaused)
	{
		m_fRunTime += (float)(gpGlobals->curtime - m_fLastRedraw);
		SetTime(m_fRunTime);
	}
	
	m_fLastRedraw = gpGlobals->curtime;

	LPBITMAPINFOHEADER lpbih;
	unsigned char *pData;

	// Well, it should be a valid frame, shouldn't it?

	if ( m_nCurrentSample < 0 )
	{
		m_nCurrentSample = 1; // Array starting with 0 or 1?
	}


	lpbih = (LPBITMAPINFOHEADER)AVIStreamGetFrame( m_pGetFrame, m_nCurrentSample );
	if ( !lpbih )
	{
		int nBytes = pVTFTexture->ComputeTotalSize();
		memset( pVTFTexture->ImageData(), 0xFF, nBytes );
		if ( LastError != 1 )
		{
			Msg("AVIMATERIAL ERROR: Frame %i not available (time %d)\n",m_nCurrentSample,m_fRunTime);
			LastError = -1; // Well, there's bacon and egg.
			// And then there's bacon, egg, spam, spam, sussage, spam, spam, avierror and spam
		}
		return;
	}
	
	int nWidth = pVTFTexture->Width();
	int nHeight = pVTFTexture->Height();
	int nBihHeight = abs( lpbih->biHeight );
	if ( lpbih->biWidth > nWidth || nBihHeight > nHeight )
	{
		int nBytes = pVTFTexture->ComputeTotalSize();
		memset( pVTFTexture->ImageData(), 0xFF, nBytes );
		if ( LastError != 2 )
		{
			Msg("AVIMATERIAL ERROR: Too small buffer; Width (%i vs %i) Height (%i vs %i)\n",lpbih->biWidth,nWidth,nBihHeight,nHeight);
			LastError = 2;
		}
		return;
	}

	pData = (unsigned char *)lpbih + lpbih->biSize;
	if ( lpbih->biBitCount == 8 )
	{
		// This is the palette
		pData += 256 * sizeof(RGBQUAD);
	}

	if ( (( lpbih->biBitCount == 16 ) || ( lpbih->biBitCount == 32 )) && ( lpbih->biCompression == BI_BITFIELDS ) )
	{
		pData += 3 * sizeof(DWORD);

		// MASKS NOT IMPLEMENTED YET
		int nBytes = pVTFTexture->ComputeTotalSize();
		memset( pVTFTexture->ImageData(), 0xFF, nBytes );
		if ( LastError != 3 )
		{
			Msg("AVIMATERIAL ERROR: Masks are not implemented yet\n");
			LastError = 3;
		}
		//return; // Non-Fetal?
	}

	int nStride = ( lpbih->biWidth * lpbih->biBitCount / 8 + 3 ) & 0xFFFFFFFC;
	if ( lpbih->biHeight > 0 )
	{
		y = nBihHeight - 1;
		nIncY = -1;
	}
	else
	{
		y = 0;
		nIncY = 1;
	}

	for ( i = 0; i < nBihHeight; ++i, pData += nStride, y += nIncY )
	{
		pixelWriter.Seek( 0, y );
		BGR888_t *pAVIPixel = (BGR888_t*)pData;
		for (int x = 0; x < lpbih->biWidth; ++x, ++pAVIPixel)
		{
			pixelWriter.WritePixel( pAVIPixel->r, pAVIPixel->g, pAVIPixel->b, 255 );
		}
	}

	
	return;

}

void CAviTextureRegen::CreateVideoStream( )
{
	HRESULT hr = AVIFileGetStream( m_pAVIFile, &m_pAVIStream, streamtypeVIDEO, 0 );
	if ( hr != AVIERR_OK )
	{ 
		Warning("CAviTextureRegen::CreateVideoStream(): Failure loading stream, error = %i",hr);
		return;
	}

	m_nCurrentSample = AVIStreamStart( m_pAVIStream );

	// Create a compatible DC
	HDC hdcscreen = GetDC( GetDesktopWindow() );
	m_memdc = CreateCompatibleDC( hdcscreen ); 
	ReleaseDC( GetDesktopWindow(), hdcscreen );

	// Set up a DIBSection for the screen
	m_bih					= &m_bi.bmiHeader;
	m_bih->biSize			= sizeof( *m_bih );
	m_bih->biWidth			= m_nAVIWidth;
	m_bih->biHeight			= m_nAVIHeight;
	m_bih->biPlanes			= 1;
	m_bih->biBitCount		= 24;
	m_bih->biCompression	= BI_RGB;
	m_bih->biSizeImage		= ( ( m_bih->biWidth * m_bih->biBitCount / 8 + 3 )& 0xFFFFFFFC ) * m_bih->biHeight;
	m_bih->biXPelsPerMeter	= 10000;
	m_bih->biYPelsPerMeter	= 10000;
	m_bih->biClrUsed		= 0;
	m_bih->biClrImportant	= 0;
	
	// Create the DIBSection
	void *bits;
	m_DIBSection = CreateDIBSection( m_memdc, ( BITMAPINFO *)m_bih, DIB_RGB_COLORS, &bits, NULL, NULL );

	// Get at the DIBSection object
    DIBSECTION dibs; 
	GetObject( m_DIBSection, sizeof( dibs ), &dibs );

	m_pGetFrame = AVIStreamGetFrameOpen( m_pAVIStream, &dibs.dsBmih );
}

void CAviTextureRegen::DestroyVideoStream( )
{
	if ( m_pGetFrame )
	{
		AVIStreamGetFrameClose( m_pGetFrame );
		m_pGetFrame = NULL;
	}

	if ( m_DIBSection != 0 )
	{
		DeleteObject( m_DIBSection );
		m_DIBSection = (HBITMAP)0;
	}

	if ( m_memdc != 0 )
	{
		// Release the compatible DC
		DeleteDC( m_memdc );
		m_memdc = (HDC)0;
	}

	if ( m_pAVIStream )
	{
		AVIStreamRelease( m_pAVIStream );
		m_pAVIStream = NULL;
	}
}
#endif