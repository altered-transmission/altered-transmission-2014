#ifdef ENABLE_FFMPEG_IN_SOURCE
#ifndef ENV_FFMPEG_H
#define ENV_FFMPEG_H


// How many frames should be try and be in advance compared to the actual frame rendered?
#define FFMPEG_FRAMES_IN_ADVANCE 3

class	CffmpegMaterialProxy;

//This is the client side of the entity.  The corresponding server entity is C_Env_ffmpeg.
class C_Env_ffmpeg : public C_BaseEntity
{
public:
	DECLARE_CLIENTCLASS();
	DECLARE_CLASS( C_Env_ffmpeg, C_BaseEntity );
	C_Env_ffmpeg();
	~C_Env_ffmpeg();

			void	OnDataChanged(DataUpdateType_t updateType);
	virtual	void	ClientThink( );
			bool	LoadVideo( void );
			bool	PlayVideo( void );
			bool	Render(int* Buffer, int W, int H); 
			bool	PutVideoFrame(int* Buffer, int W, int H);
			bool	GetVideoFrame();
			bool	SyncNextFrame();

	int m_iPlay;
	int m_iLastPlay;
	bool m_bLoop;
	int m_iAdvanceFrame;
	int m_iLastAdvanceFrame;
	float m_fBegun;
	float m_fFrameRateHack;
	char m_iszTextureName[256];	//These arrays may leak, but I don't think so.
	char m_iszMovieName[256];
	char m_iszLastMovieName[256];
	size_t		pFrameSize;
	uint8_t*	pFrameRGBBiffer;
	uint8_t*	pFrameRGBBackBuffer[FFMPEG_FRAMES_IN_ADVANCE];


	//BIKMaterial_t	m_BIKHandle;
	IMaterial		*m_pMaterial;
	int				m_nPlaybackHeight;			// Calculated to address ratio changes
	int				m_nPlaybackWidth;
	char			m_szExitCommand[MAX_PATH];	// This call is fired at the engine when the video finishes or is interrupted

    AVFormatContext *pFormatCtx;
    int             i, videoStream;
    AVCodecContext  *pCodecCtx;
    AVCodec         *pCodec;
    AVFrame         *pFrame; 
    AVFrame         *pFrameRGB;
    AVPacket        packet;
    int             frameFinished;
    int             numBytes;
	int				NumFramesInAdvanced;
	struct SwsContext *sws_opts;
	struct SwsContext *img_convert_ctx;

	CffmpegMaterialProxy	*m_pProxy;

	bool			ShouldPlayback;
	int64_t			CurrentFrame;

	float			m_flU;	// U,V ranges for video on its sheet
	float			m_flV;
};

#endif
#endif