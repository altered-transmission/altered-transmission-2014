#include "cbase.h"
#include "screenspaceeffects.h"
#include "rendertexture.h"
#include "model_types.h"
#include "materialsystem/imaterialsystemhardwareconfig.h"
#include "materialsystem/imaterialsystem.h"
#include "materialsystem/imaterialvar.h"
#include "cdll_client_int.h"
#include "materialsystem/itexture.h"
#include "keyvalues.h"
#include "ClientEffectPrecacheSystem.h"
#include "viewrender.h"
#include "view_scene.h"
#include "tier0/vprof.h"
#include "c_basehlplayer.h"
#include "AT_ScreenSpaceEffects.h"
 
// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#ifdef ALTERED_TRANSMISSION

//=============================================================================
// Unsharp Mask
//=============================================================================
 
ConVar r_post_unsharp( "r_post_unsharp", "1", FCVAR_ARCHIVE );
ConVar r_post_unsharp_debug( "r_post_unsharp_debug", "0", FCVAR_CHEAT );
ConVar r_post_unsharp_strength( "r_post_unsharp_strength", "0.5", FCVAR_ARCHIVE );
ConVar r_post_unsharp_blursize( "r_post_unsharp_blursize", "5.0", FCVAR_ARCHIVE );


void CUnsharpEffect::Init( void )
{
        m_UnsharpBlurFB.InitRenderTarget( ScreenWidth()/2, ScreenHeight()/2, RT_SIZE_DEFAULT, IMAGE_FORMAT_RGBA8888, MATERIAL_RT_DEPTH_SEPARATE, false, "_rt_UnsharpBlur" );
 
        m_UnsharpBlur.Init( materials->FindMaterial("effects/unsharp_blur", TEXTURE_GROUP_OTHER, true) );
        m_Unsharp.Init( materials->FindMaterial("effects/unsharp", TEXTURE_GROUP_OTHER, true) );
}
 
void CUnsharpEffect::Shutdown( void )
{
        m_UnsharpBlurFB.Shutdown();
        m_UnsharpBlur.Shutdown();
        m_Unsharp.Shutdown();
} 

void CUnsharpEffect::Render( int x, int y, int w, int h )
{
	if ( !m_bEnabled ) { return; }

        VPROF( "CUnsharpEffect::Render" );
 
        if( !r_post_unsharp.GetBool() )
                return;
 
        // Grab the render context
        CMatRenderContextPtr pRenderContext( materials );
 
        // Set to the proper rendering mode.
        pRenderContext->MatrixMode( MATERIAL_VIEW );
        pRenderContext->PushMatrix();
        pRenderContext->LoadIdentity();
        pRenderContext->MatrixMode( MATERIAL_PROJECTION );
        pRenderContext->PushMatrix();
        pRenderContext->LoadIdentity();
 
        IMaterialVar *var;
        var = m_UnsharpBlur->FindVar( "$blursize", NULL );
        var->SetFloatValue( r_post_unsharp_blursize.GetFloat() );
 
        if( r_post_unsharp_debug.GetBool() )
        {
                DrawScreenEffectMaterial( m_UnsharpBlur, x, y, w, h );
                return;
        }
 
		Rect_t actualRect;
		UpdateScreenEffectTexture( 0, x, y, w, h, false, &actualRect );

        pRenderContext->PushRenderTargetAndViewport( m_UnsharpBlurFB );
        pRenderContext->DrawScreenSpaceQuad( m_UnsharpBlur );
        pRenderContext->PopRenderTargetAndViewport();
 
        //Restore our state
        pRenderContext->MatrixMode( MATERIAL_VIEW );
        pRenderContext->PopMatrix();
        pRenderContext->MatrixMode( MATERIAL_PROJECTION );
        pRenderContext->PopMatrix();
 
        var = m_Unsharp->FindVar( "$fbblurtexture", NULL );
        var->SetTextureValue( m_UnsharpBlurFB );
        var = m_Unsharp->FindVar( "$unsharpstrength", NULL );
        var->SetFloatValue( r_post_unsharp_strength.GetFloat() );
        var = m_Unsharp->FindVar( "$blursize", NULL );
        var->SetFloatValue( r_post_unsharp_blursize.GetFloat() );
 
        DrawScreenEffectMaterial( m_Unsharp, x, y, w, h );
}

//=============================================================================
// Recording Effect used in Altered Transmission. This should help convey the
// interpretation that AT is a recording recovered by the Combine. This means
// that the image is slightly disoriented every once in a while, and that you
// see some interesting visual artifacts whenever the player is damaged.
// Hopefully that should make players remember AT is not your average mod.
//=============================================================================

ConVar at_is_recording( "at_is_recording", "1", FCVAR_CLIENTDLL );
ConVar r_post_tearing( "r_post_tearing", "1", FCVAR_ARCHIVE );
ConVar r_post_chromatic( "r_post_chromatic", "1", FCVAR_ARCHIVE );
ConVar r_post_scanline( "r_post_scanline", "1", FCVAR_ARCHIVE );

ConVar r_post_chromatic_osc_c( "r_post_chromatic_osc_c", "0.06", FCVAR_CLIENTDLL );
ConVar r_post_chromatic_offset( "r_post_chromatic_offset", "0.85", FCVAR_CLIENTDLL );
ConVar r_post_noise_intensity( "r_post_noise_intensity", "0.2", FCVAR_CLIENTDLL );
ConVar r_post_scanline_intensity( "r_post_scanline_intensity", "0.075", FCVAR_CLIENTDLL );
ConVar r_post_scanline_count( "r_post_scanline_count", "2048", FCVAR_CLIENTDLL );

ConVar r_post_tear_chromatic_offset( "r_post_tear_chromatic_offset", "15.0", FCVAR_CLIENTDLL );
ConVar r_post_tear_noise_intensity( "r_post_tear_noise_intensity", "0.3", FCVAR_CLIENTDLL );
ConVar r_post_tear_scanline_intensity( "r_post_tear_scanline_intensity", "0.3", FCVAR_CLIENTDLL );
ConVar r_post_tear_frames_min( "r_post_tear_frames_min", "5", FCVAR_CLIENTDLL );
ConVar r_post_tear_frames_max( "r_post_tear_frames_max", "25", FCVAR_CLIENTDLL );
ConVar r_post_tear_frames_force( "r_post_tear_frames_force", "0", FCVAR_CLIENTDLL );
ConVar r_post_tear_frames_mean_occurences( "r_post_tear_frames_mean_occurences", "30.0", FCVAR_CLIENTDLL );

void CRecordingEffect::Init( void )
{
	m_ChromaticDisp.Init( materials->FindMaterial("effects/chromaticDisp", TEXTURE_GROUP_OTHER, true) );
	m_Noise.Init( materials->FindMaterial("effects/noise", TEXTURE_GROUP_OTHER, true ) );

	iLastHealth = -1;
	iTearFrames = 0;
	RecalculateTearTime();
}

void CRecordingEffect::RecalculateTearTime()
{
	fPrevTearMean = r_post_tear_frames_mean_occurences.GetFloat();
	fNextTearTime = gpGlobals->curtime + fPrevTearMean * 2 * random->RandomFloat();
}

void CRecordingEffect::Shutdown( void )
{
	m_ChromaticDisp.Shutdown();
	m_Noise.Shutdown();
}

void CRecordingEffect::Render( int x, int y, int w, int h )
{
	// Only render this effect if enabled, or if we are rendering a recording.
	if ( !m_bEnabled || !at_is_recording.GetBool() ) { return; }

	if ( fPrevTearMean != r_post_tear_frames_mean_occurences.GetFloat() )
	{
		RecalculateTearTime();
	}

	IMaterialVar *var;

	// Get a pointer to our player.
	C_BaseHLPlayer *pPlayer = (C_BaseHLPlayer *)C_BasePlayer::GetLocalPlayer();	
	if( !pPlayer ) { return; }

	// Figure out the itensities of the various effects.
	float fDispC = gpGlobals->curtime * r_post_chromatic_osc_c.GetFloat();
	fDispC = cos(fDispC) * cos(fDispC) * cos(fDispC) * cos(fDispC) * cos(fDispC);
	float fDispersionAmount = r_post_chromatic_offset.GetFloat() * fDispC;
	float fNoiseAmount = r_post_noise_intensity.GetFloat() * (0.75f+fDispC/2.0f);
	
	// Reset the damage coefficient when the player is hit.
	if ( iLastHealth > pPlayer->GetHealth() && pPlayer->GetHealth() > 10 )
	{
		iTearFrames = random->RandomInt(r_post_tear_frames_min.GetInt(), r_post_tear_frames_max.GetInt());
	}

	iLastHealth = pPlayer->GetHealth();

	// Apply the chromatic disp effect.
	if ( r_post_chromatic.GetBool() )
	{
		var = m_ChromaticDisp->FindVar( "$FOCUSOFFSET", NULL );
		var->SetFloatValue( fDispersionAmount );
		DrawScreenEffectMaterial( m_ChromaticDisp, x, y, w, h );
	}

	// Apply the noise/scanline effects.
	if ( r_post_scanline.GetBool() )
	{
		var = m_Noise->FindVar( "$NOISE_INTENSITY", NULL );
		var->SetFloatValue( fNoiseAmount );
		var = m_Noise->FindVar( "$SCAN_INTENSITY", NULL );
		var->SetFloatValue( r_post_scanline_intensity.GetFloat() * (0.75+fDispC/2.0f) );
		var = m_Noise->FindVar( "$SCAN_AMOUNT", NULL );
		var->SetFloatValue( r_post_scanline_count.GetFloat() );
		DrawScreenEffectMaterial( m_Noise, x, y, w, h );
	}

	// Occasionally tear the frame anyway. 
	if ( fNextTearTime <= gpGlobals->curtime )
	{
		iTearFrames = random->RandomInt(r_post_tear_frames_min.GetInt(), r_post_tear_frames_max.GetInt());
		RecalculateTearTime();
	}

	// If requested, force tearing of frames.
	if ( r_post_tear_frames_force.GetBool() ) { iTearFrames = 1; }

	// If we have some tearing frames left to be drawn, draw them.
	if ( iTearFrames > 0 )
	{
		// But only if the effect is enabled.
		if ( r_post_tearing.GetBool() )
		{
			int iTearLength = random->RandomInt(h/8, h/3);
			int iTearOffset = random->RandomInt(0, h-1);

			// Set up the tearing effect parameters.
			var = m_Noise->FindVar( "$NOISE_INTENSITY", NULL );
			var->SetFloatValue( r_post_tear_noise_intensity.GetFloat() );
			var = m_Noise->FindVar( "$SCAN_INTENSITY", NULL );
			var->SetFloatValue( r_post_tear_scanline_intensity.GetFloat() );
			var = m_Noise->FindVar( "$SCAN_AMOUNT", NULL );
			var->SetFloatValue( r_post_scanline_count.GetFloat() / 1.5f );
			DrawScreenEffectMaterial( m_Noise, x, iTearOffset, w, iTearLength );

			fDispersionAmount = r_post_tear_chromatic_offset.GetFloat() * random->RandomFloat(-1, 1);
			if ( random->RandomInt(0, 10) == 0 ) { fDispersionAmount *= 100; }
			var = m_ChromaticDisp->FindVar( "$FOCUSOFFSET", NULL );
			var->SetFloatValue( fDispersionAmount );
			DrawScreenEffectMaterial( m_ChromaticDisp, x, iTearOffset, w, iTearLength );
		}

		iTearFrames--;
	}
}

#endif
