//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
#include "cbase.h"
#include "ivmodemanager.h"
#include "clientmode_hlnormal.h"
#include "panelmetaclassmgr.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// default FOV for HL2
ConVar default_fov( "default_fov", "75", FCVAR_CHEAT );

// The current client mode. Always ClientModeNormal in HL.
IClientMode *g_pClientMode = NULL;

#define SCREEN_FILE		"scripts/vgui_screens.txt"

class CHLModeManager : public IVModeManager
{
public:
				CHLModeManager( void );
	virtual		~CHLModeManager( void );

	virtual void	Init( void );
	virtual void	SwitchMode( bool commander, bool force );
	virtual void	OverrideView( CViewSetup *pSetup );
	virtual void	CreateMove( float flInputSampleTime, CUserCmd *cmd );
	virtual void	LevelInit( const char *newmap );
	virtual void	LevelShutdown( void );
};

CHLModeManager::CHLModeManager( void )
{
}

CHLModeManager::~CHLModeManager( void )
{
}

void CHLModeManager::Init( void )
{
	g_pClientMode = GetClientModeNormal();
	PanelMetaClassMgr()->LoadMetaClassDefinitionFile( SCREEN_FILE );
}

void CHLModeManager::SwitchMode( bool commander, bool force )
{
}

void CHLModeManager::OverrideView( CViewSetup *pSetup )
{
}

void CHLModeManager::CreateMove( float flInputSampleTime, CUserCmd *cmd )
{
}

#ifdef ALTERED_TRANSMISSION
// Altered Transmission Edit: Pingback on new map!
#include "../client/MaxsiDistributionSDK.h"

// Hack to record demos on level start.

char RecordingNameParameter[64] = "";

void recordonlevelstart_f ( const CCommand& args )
{
	if ( args.ArgC() < 2 ) { RecordingNameParameter[0] = 0; return; }
 
 	if ( Q_strlen(args[1]) > 63 )
	{
		Warning("Input to recordonlevelstart may not be longer than 63 characters, otherwise you have to up the buffer size in hl2_clientmode.cpp, sorry!\n");
		return;
	}

	Q_strcpy(RecordingNameParameter, args[1]);
}

ConCommand recordonlevelstart( "recordonlevelstart", recordonlevelstart_f, "recordonlevelstart", 0);

#endif

void CHLModeManager::LevelInit( const char *newmap )
{
#ifdef ALTERED_TRANSMISSION
	if ( g_MaxsiAchievements )
	{
		char* NewMap = (char*)newmap;
		g_MaxsiAchievements->UnlockAchievement(NewMap);
		//g_MaxsiDistribution->Statistic("Progress",NewMap);
	}

	if ( strlen(RecordingNameParameter) > 0 )
	{
		char Command[64 + 7] = "record ";
		Q_strcat(Command, RecordingNameParameter, 64+7);
		engine->ClientCmd(Command);
	}

#endif
	g_pClientMode->LevelInit( newmap );
}

void CHLModeManager::LevelShutdown( void )
{
	g_pClientMode->LevelShutdown();
}


static CHLModeManager g_HLModeManager;
IVModeManager *modemanager = &g_HLModeManager;

